package com.ibm.app;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;

import com.ibm.services.CatalogServiceImpl;

public class CatalogsApp extends Application{
	private Set<Object> singletons = new HashSet<Object>();

	public CatalogsApp() {
		singletons.add(new CatalogServiceImpl());
	} 

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
