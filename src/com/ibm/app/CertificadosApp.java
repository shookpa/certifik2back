package com.ibm.app;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import com.ibm.services.TitulosServiceImpl;
import com.ibm.services.CatalogServiceImpl;
import com.ibm.services.CertificadosServiceImpl;
import com.ibm.services.SecurityServiceImpl;

public class CertificadosApp extends Application{
	private Set<Object> singletons = new HashSet<Object>();

	public CertificadosApp() {
		singletons.add(new CertificadosServiceImpl());
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
