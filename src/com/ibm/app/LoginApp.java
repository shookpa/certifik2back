package com.ibm.app;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;


import com.ibm.services.SecurityServiceImpl;

public class LoginApp extends Application{
	private Set<Object> singletons = new HashSet<Object>();

	public LoginApp() {
		singletons.add(new SecurityServiceImpl());
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
