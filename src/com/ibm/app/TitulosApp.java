package com.ibm.app;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import com.ibm.services.TitulosServiceImpl;
import com.ibm.services.CatalogServiceImpl;
import com.ibm.services.SecurityServiceImpl;

public class TitulosApp extends Application{
	private Set<Object> singletons = new HashSet<Object>();

	public TitulosApp() {
		singletons.add(new TitulosServiceImpl());
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
