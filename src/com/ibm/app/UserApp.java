package com.ibm.app;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import com.ibm.services.UsersServiceImpl;



public class UserApp extends Application{
	private Set<Object> singletons = new HashSet<Object>();

	public UserApp() {
		singletons.add(new UsersServiceImpl());
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}