package com.ibm.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ibm.models.Asignatura;
import com.ibm.models.AutorizacionReconocimientoShort;
import com.ibm.models.Calificacion;
import com.ibm.models.CargosFirmantesShort;
import com.ibm.models.Carrera;
import com.ibm.models.Configurations;
import com.ibm.models.CreditosCarrera;
import com.ibm.models.EntidadFederativaShort;
import com.ibm.models.Firmante;
import com.ibm.models.FirmanteShort;
import com.ibm.models.FundamentoLegalServicioSocialShort;
import com.ibm.models.ModalidadTitulacionShort;
import com.ibm.models.School;
import com.ibm.models.Student;
import com.ibm.models.StudentInfoCalif;
import com.ibm.models.TipoEstudioAntecedenteShort;
import com.ibm.models.Titulo;
import com.ibm.util.CrudOperation;

public class CatalogsDao {

	public List<Object> getCatalogItems(String catalog) {
		CrudOperation crud = new CrudOperation();
		return crud.getList(catalog, "");
	}

	public Carrera getCarrera(int id) {
		CrudOperation crud = new CrudOperation();
		Carrera tit = (Carrera) crud.obtieneDatosPojo(new Carrera(), String.valueOf(id), "Integer");
		return tit;
	}

	public Firmante getFirmante(int id) {
		CrudOperation crud = new CrudOperation();
		Firmante firm = (Firmante) crud.obtieneDatosPojo(new Firmante(), String.valueOf(id), "Integer");
		return firm;
	}

	public Configurations getConfig(int id) {
		CrudOperation crud = new CrudOperation();
		Configurations tit = (Configurations) crud.obtieneDatosPojo(new Configurations(), String.valueOf(id),
				"Integer");
		return tit;
	}

	public void setCalificacion(Calificacion calif) {
		CrudOperation crud = new CrudOperation();
		crud.inserta(calif);

	}

	public void setCarrera(Carrera car) {
		CrudOperation crud = new CrudOperation();
		crud.inserta(car);

	}

	public void setSchool(School sch) {
		CrudOperation crud = new CrudOperation();
		crud.inserta(sch);

	}

	public void setAsignatura(Asignatura asig) {
		CrudOperation crud = new CrudOperation();
		crud.inserta(asig);

	}

	public void setStudent(Student stud) {
		CrudOperation crud = new CrudOperation();
		crud.inserta(stud);

	}

	public void setSigner(Firmante firmante) {
		CrudOperation crud = new CrudOperation();
		crud.inserta(firmante);

	}

	public List<Object> searchCatalog(Object pojito, Map<String, Object> filtro) {
		CrudOperation crud = new CrudOperation();
		List e = crud.search(pojito, filtro);
		// List em = null;
		// int i = 0;
		// for (Object object : e) {
		// em.add( e.get(i));
		// i++;
		// }
		return e;

	}
	public Object searchRow(Object pojito, Map<String, Object> filtro) {
		CrudOperation crud = new CrudOperation();
		
		return  crud.searchRow(pojito, filtro);

	}

	public Student searchStudent(Map<String, Object> filtro) {
		CrudOperation crud = new CrudOperation();
		List e = crud.search(new Student(), filtro);
		Student[] em = new Student[e.size()];
		int i = 0;
		for (Object object : e) {
			em[i] = (Student) e.get(i);
			i++;
		}
		if (i > 0)
			return em[0];
		else
			return null;

	}

	public StudentInfoCalif searchStudentInfo(Map<String, Object> filtro) {
		CrudOperation crud = new CrudOperation();
		List e = crud.search(new StudentInfoCalif(), filtro);
		StudentInfoCalif[] em = new StudentInfoCalif[e.size()];
		int i = 0;
		for (Object object : e) {
			em[i] = (StudentInfoCalif) e.get(i);
			i++;
		}
		CrudOperation crud2 = new CrudOperation();
		Map<String, Object> filtroCred = new HashMap<String, Object>();
		if (i > 0) {
			filtroCred.put("id_carrera", em[0].getId_carrera());
			CreditosCarrera cred = (CreditosCarrera) crud2.searchRow(new CreditosCarrera(), filtroCred);
//			em[0].setAsignadas(cred.getTotal_asignadas());
			em[0].setTotal_creditos(cred.getTotal_creditos());
			em[0].setEstatus_certif(
					em[0].getTotal_creditos() <= em[0].getCreditos_obtenidos() ? "Completo" : "Parcial");
			return em[0];
		} else
			return null;

	}

	public void deleteRecords(String tabla, String campo, String valor) {
		CrudOperation crud = new CrudOperation();
		crud.borra(tabla, campo, valor);
	}

	public void deleteRecord(Object obj) {
		CrudOperation crud = new CrudOperation();
		crud.borra(obj);
	}

	public void updateRecord(Object obj) {
		CrudOperation crud = new CrudOperation();
		crud.actualiza(obj);
	}
	public AutorizacionReconocimientoShort getAutorizacion(int id) {
		CrudOperation crud = new CrudOperation();
		AutorizacionReconocimientoShort reg = (AutorizacionReconocimientoShort) crud.obtieneDatosPojo(new AutorizacionReconocimientoShort(), String.valueOf(id), "Integer");
		return reg;
	}
	public FundamentoLegalServicioSocialShort getFundamentoLegalServicioSocialShort(int id) {
		CrudOperation crud = new CrudOperation();
		FundamentoLegalServicioSocialShort reg = (FundamentoLegalServicioSocialShort) crud.obtieneDatosPojo(new FundamentoLegalServicioSocialShort(), String.valueOf(id), "Integer");
		return reg;
	}
	public ModalidadTitulacionShort getModalidadTitulacionShort(int id) {
		CrudOperation crud = new CrudOperation();
		ModalidadTitulacionShort reg = (ModalidadTitulacionShort) crud.obtieneDatosPojo(new ModalidadTitulacionShort(), String.valueOf(id), "Integer");
		return reg;
	}
	public EntidadFederativaShort getEntidadFederativaShort(int id) {
		CrudOperation crud = new CrudOperation();
		EntidadFederativaShort reg = (EntidadFederativaShort) crud.obtieneDatosPojo(new EntidadFederativaShort(), String.valueOf(id), "Integer");
		return reg;
	}
	public TipoEstudioAntecedenteShort getTipoEstudioAntecedenteShort(int id) {
		CrudOperation crud = new CrudOperation();
		TipoEstudioAntecedenteShort reg = (TipoEstudioAntecedenteShort) crud.obtieneDatosPojo(new TipoEstudioAntecedenteShort(), String.valueOf(id), "Integer");
		return reg;
	}
	public CargosFirmantesShort getCargosFirmantesShort(int id) {
		CrudOperation crud = new CrudOperation();
		CargosFirmantesShort reg = (CargosFirmantesShort) crud.obtieneDatosPojo(new CargosFirmantesShort(), String.valueOf(id), "Integer");
		return reg;
	}
	
}
