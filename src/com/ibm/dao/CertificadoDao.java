package com.ibm.dao;

import java.util.List;
import java.util.Map;

import com.ibm.models.Certificado;


import com.ibm.util.CrudOperation;


public class CertificadoDao {
	public Certificado getCertificado(int id) {
		CrudOperation crud = new CrudOperation();
		Certificado reg = (Certificado) crud.obtieneDatosPojo(new Certificado(), String.valueOf(id), "Integer");
		return reg;
	}
	public void setCertificado(Certificado certif) {
		CrudOperation crud = new CrudOperation();
		crud.inserta(certif);

	}
	public List<Object> listCertificados(Object pojito, Map<String, Object> filtro, int limit, int offset) {
		CrudOperation crud = new CrudOperation();
		List e = crud.searchPaginated(pojito, filtro, limit,offset);
//		List em = null;
//		int i = 0;
//		for (Object object : e) {
//			em.add( e.get(i));
//			i++;
//		}
		return e;

	}
	public List<Object> listCertificados(Object pojito, Map<String, Object> filtro) {
		CrudOperation crud = new CrudOperation();
		List e = crud.search(pojito, filtro);
//		List em = null;
//		int i = 0;
//		for (Object object : e) {
//			em.add( e.get(i));
//			i++;
//		}
		return e;

	}
	
}
