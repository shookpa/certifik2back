package com.ibm.dao;

import java.util.List;
import java.util.Map;

import com.ibm.models.License;
import com.ibm.models.RolMenu;
import com.ibm.models.User;
import com.ibm.util.CrudOperation;


public class LicenseDao {
	public License[] getLicense(Map<String, Object> filtro) {
		CrudOperation crud = new CrudOperation();
		List e = crud.search(new License(), filtro);
		License[] em = new License[e.size()];
		int i = 0;
		for (Object object : e) {
			em[i] = (License) e.get(i);
			i++;
		}
		return em;
	}
	public License getLicense(int id) {
		CrudOperation crud = new CrudOperation();
		License lic = (License) crud.obtieneDatosPojo(new License(), String.valueOf(id), "Integer");
		return lic;
	}
	public void deleteLicense(Object obj) {
		CrudOperation crud = new CrudOperation();
		crud.borra(obj);
	}
	public void setLicense(License lic) {
		CrudOperation crud = new CrudOperation();
		crud.inserta(lic);
		
	}
	public void updateLicense(License lic) {
		CrudOperation crud = new CrudOperation();
		crud.actualiza(lic);
		

	}
	public License[] searchLicense(Map<String, Object> filtro) {
		CrudOperation crud = new CrudOperation();
		List e = crud.search(new License(), filtro);
		License[] em = new License[e.size()];
		int i = 0;
		for (Object object : e) {
			em[i] = (License) e.get(i);
			i++;
		}
		return em;

	}
	public License[] getAllLicenses() {
		CrudOperation crud = new CrudOperation();
		List e = crud.getList(License.class, "");
		License[] em = new License[e.size()];
		int i = 0;
		for (Object object : e) {
			em[i] = (License) e.get(i);
			i++;
		}
//		crud.cierra();
		return em;
	}
}
