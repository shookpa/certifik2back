package com.ibm.dao;


import java.util.List;
import java.util.Map;


import com.ibm.models.User;
import com.ibm.models.UserTwoFactor;
import com.ibm.util.CrudOperation;

public class LoginDao {
	
	
	public LoginDao(){}

	public User getUser(int id) {
		CrudOperation crud = new CrudOperation();
		User user = (User) crud.obtieneDatosPojo(new User(), String.valueOf(id), "Integer");
		return user;
	}
	public UserTwoFactor getUserTwoFactor(int id) {
		CrudOperation crud = new CrudOperation();
		UserTwoFactor user = (UserTwoFactor) crud.obtieneDatosPojo(new UserTwoFactor(), String.valueOf(id), "Integer");
		return user;
	}
	public void setUser(User user) {
		CrudOperation crud = new CrudOperation();
		crud.inserta(user);

	}	
	
	public User searchUser(Map<String, Object> filtro) {
		CrudOperation crud = new CrudOperation();
		List e = crud.search(new User(), filtro);
		User em = new User();
//		int i = 0;
		if (e.size()>0)
		{
			em = (User) e.get(0);
			return em;
		}
		else
			return null;
//		for (Object object : e) {
//			em = (User) e.get(i);
//			i++;
//		}
//		if (!em.getNombre().equalsIgnoreCase(""))
//			return em;
//		else
//			return null;

	}
	
	public User[] getAllUsers() {
		CrudOperation crud = new CrudOperation();
		List e = crud.getList(User.class, "");
		User[] em = new User[e.size()];
		int i = 0;
		for (Object object : e) {
			em[i] = (User) e.get(i);
			i++;
		}
		return em;
	}
	
}
