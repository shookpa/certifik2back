package com.ibm.dao;

import java.util.List;
import java.util.Map;

import com.ibm.models.Carrera;
import com.ibm.models.Permiso;
import com.ibm.models.Rol;
import com.ibm.models.User;
import com.ibm.util.CrudOperation;


public class RolDao {
	public Rol[] getAllRoles() {
		CrudOperation crud = new CrudOperation();
		List e = crud.getList(Rol.class, "");
		Rol[] em = new Rol[e.size()];
		int i = 0;
		for (Object object : e) {
			em[i] = (Rol) e.get(i);
			i++;
		}
//		crud.cierra();
		return em;
	}
	public Permiso[] getAllPermisos() {
		CrudOperation crud = new CrudOperation();
		List e = crud.getList(Permiso.class, "");
		Permiso[] em = new Permiso[e.size()];
		int i = 0;
		for (Object object : e) {
			em[i] = (Permiso) e.get(i);
			i++;
		}
//		crud.cierra();
		return em;
	}
	public void setRol(Rol rol) {
		CrudOperation crud = new CrudOperation();
		crud.inserta(rol);

	}
}
