package com.ibm.dao;

import java.util.List;
import java.util.Map;

import com.ibm.models.RolMenu;
import com.ibm.models.User;
import com.ibm.util.CrudOperation;

public class RolMenuDao {

	public RolMenu[] searchRolMenu(Map<String, Object> filtro) {
		CrudOperation crud = new CrudOperation();
		List e = crud.search(new RolMenu(), filtro);
		RolMenu[] em = new RolMenu[e.size()];
		int i = 0;
		for (Object object : e) {
			em[i] = (RolMenu) e.get(i);
			i++;
		}
		return em;
	}
	
	public RolMenu getUser(int id) {
		CrudOperation crud = new CrudOperation();
		RolMenu rolMenu = (RolMenu) crud.obtieneDatosPojo(new User(), String.valueOf(id), "Integer");
		return rolMenu;
	}
}
