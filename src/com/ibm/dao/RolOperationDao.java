package com.ibm.dao;

import java.util.List;
import java.util.Map;

import com.ibm.models.PermisosRol;
import com.ibm.models.RolMenu;
import com.ibm.models.RolOperation;
import com.ibm.models.User;
import com.ibm.util.CrudOperation;

public class RolOperationDao {
	public PermisosRol[] searchRolOperation(Map<String, Object> filtro) {
		CrudOperation crud = new CrudOperation();
		List e = crud.search(new PermisosRol(), filtro);
		PermisosRol[] em = new PermisosRol[e.size()];
		int i = 0;
		for (Object object : e) {
			em[i] = (PermisosRol) e.get(i);
			i++;
		}
		return em;
	}
	
	public RolOperation getUser(int id) {
		CrudOperation crud = new CrudOperation();
		RolOperation rolOp = (RolOperation) crud.obtieneDatosPojo(new User(), String.valueOf(id), "Integer");
		return rolOp;
	}
}
