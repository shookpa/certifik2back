package com.ibm.dao;

import java.util.List;
import java.util.Map;

import com.ibm.models.*;
import com.ibm.util.CrudOperation;


public class TituloDao {
	
	public Titulo getTitulo(int id) {
		CrudOperation crud = new CrudOperation();
		Titulo tit = (Titulo) crud.obtieneDatosPojo(new Titulo(), String.valueOf(id), "Integer");
		return tit;
	}
	
	
	
	public void setTitulo(Titulo titulo) {
		CrudOperation crud = new CrudOperation();
		crud.inserta(titulo);

	}
	
	public void setTituloFirmante(TituloFirmantes tituFirm) {
		CrudOperation crud = new CrudOperation();
		crud.inserta(tituFirm);
		
	}
	
	public List<Object> listTitulos(Object pojito, Map<String, Object> filtro, int limit, int offset) {
		CrudOperation crud = new CrudOperation();
		List e = crud.searchPaginated(pojito, filtro, limit,offset);
//		List em = null;
//		int i = 0;
//		for (Object object : e) {
//			em.add( e.get(i));
//			i++;
//		}
		return e;

	}
	public List<Object> listTitulos(Object pojito, Map<String, Object> filtro) {
		CrudOperation crud = new CrudOperation();
		List e = crud.search(pojito, filtro);
//		List em = null;
//		int i = 0;
//		for (Object object : e) {
//			em.add( e.get(i));
//			i++;
//		}
		return e;

	}
	
}
