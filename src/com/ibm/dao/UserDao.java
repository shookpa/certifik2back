package com.ibm.dao;

import java.util.List;
import java.util.Map;

import com.ibm.models.School;
import com.ibm.models.SchoolsUser;
import com.ibm.models.User;
import com.ibm.models.UserSchool;
import com.ibm.util.CrudOperation;


public class UserDao {
	public User[] getAllUsers() {
		CrudOperation crud = new CrudOperation();
		List e = crud.getList(User.class, "");
		User[] em = new User[e.size()];
		int i = 0;
		for (Object object : e) {
			em[i] = (User) e.get(i);
			i++;
		}
//		crud.cierra();
		return em;
	}
	public User getUser(int id) {
		CrudOperation crud = new CrudOperation();
		User emp = (User) crud.obtieneDatosPojo(new User(), String.valueOf(id), "Integer");
		return emp;
	}
	public School getSchool(int id) {
		CrudOperation crud = new CrudOperation();
		School ins = (School) crud.obtieneDatosPojo(new School(), String.valueOf(id), "Integer");
		return ins;
	}
	
	public void setUserSchool(UserSchool userSch) {
		CrudOperation crud = new CrudOperation();
		crud.inserta(userSch);
		
	}
	
	public void setUser(User user) {
		CrudOperation crud = new CrudOperation();
		crud.inserta(user);
		
	}
	
	public void updateUser(User user) {
		CrudOperation crud = new CrudOperation();
		crud.actualiza(user);
		

	}
	public User[] searchUser(Map<String, Object> filtro) {
		CrudOperation crud = new CrudOperation();
		List e = crud.search(new User(), filtro);
		User[] em = new User[e.size()];
		int i = 0;
		for (Object object : e) {
			em[i] = (User) e.get(i);
			i++;
		}
		return em;

	}
	public SchoolsUser[] getSchools(Map<String, Object> filtro) {
		CrudOperation crud = new CrudOperation();
		List e = crud.search(new SchoolsUser(), filtro);
		SchoolsUser[] em = new SchoolsUser[e.size()];
		int i = 0;
		for (Object object : e) {
			em[i] = (SchoolsUser) e.get(i);
			i++;
		}
		return em;

	}
	public void deleteUser(Object obj) {
		CrudOperation crud = new CrudOperation();
		crud.borra(obj);
	}
	public void deleteUserSchools(String obj, String campo, String valor) {
		CrudOperation crud = new CrudOperation();
		crud.borra(obj, campo, valor);
	}
}
