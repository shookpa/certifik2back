package com.ibm.models;




public class Asignatura {
	private Integer id_asignatura;
	private Integer id_asignatura_mec;
	private Integer id_institucion,  id_carrera, estatus;
	;	
	private Double creditos;
	public Integer getEstatus() {
		return estatus;
	}
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	private String clave_asignatura,desc_asignatura,tipo_asignatura;
	public Integer getId_asignatura() {
		return id_asignatura;
	}
	public void setId_asignatura(Integer id_asignatura) {
		this.id_asignatura = id_asignatura;
	}
	public Integer getId_institucion() {
		return id_institucion;
	}
	public void setId_institucion(Integer id_institucion) {
		this.id_institucion = id_institucion;
	}
	public Integer getId_carrera() {
		return id_carrera;
	}
	public void setId_carrera(Integer id_carrera) {
		this.id_carrera = id_carrera;
	}
	public Double getCreditos() {
		return creditos;
	}
	public void setCreditos(Double creditos) {
		this.creditos = creditos;
	}
	public String getClave_asignatura() {
		return clave_asignatura;
	}
	public void setClave_asignatura(String clave_asignatura) {
		this.clave_asignatura = clave_asignatura;
	}
	public String getDesc_asignatura() {
		return desc_asignatura;
	}
	public void setDesc_asignatura(String desc_asignatura) {
		this.desc_asignatura = desc_asignatura;
	}
	public String getTipo_asignatura() {
		return tipo_asignatura;
	}
	public void setTipo_asignatura(String tipo_asignatura) {
		this.tipo_asignatura = tipo_asignatura;
	}
	public Integer getId_asignatura_mec() {
		return id_asignatura_mec;
	}
	public void setId_asignatura_mec(Integer id_asignatura_mec) {
		this.id_asignatura_mec = id_asignatura_mec;
	}
	@Override
	public String toString() {
		return "Asignatura [id_asignatura=" + id_asignatura + ", id_asignatura_mec=" + id_asignatura_mec
				+ ", id_institucion=" + id_institucion + ", id_carrera=" + id_carrera + ", estatus=" + estatus
				+ ", creditos=" + creditos + ", clave_asignatura=" + clave_asignatura + ", desc_asignatura="
				+ desc_asignatura + ", tipo_asignatura=" + tipo_asignatura + "]";
	}
	
			
}
