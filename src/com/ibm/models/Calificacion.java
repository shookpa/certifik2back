package com.ibm.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;


public class Calificacion {
	
	private Integer id_calificacion;
	private Integer id_institucion, id_alumno, id_carrera, id_asignatura,
	id_observacion,periodo;	
	private String ciclo;
	private Double calificacion;
	public Integer getId_calificacion() {
		return id_calificacion;
	}
	public void setId_calificacion(Integer id_calificacion) {
		this.id_calificacion = id_calificacion;
	}
	public Integer getId_institucion() {
		return id_institucion;
	}
	public void setId_institucion(Integer id_institucion) {
		this.id_institucion = id_institucion;
	}
	public Integer getId_alumno() {
		return id_alumno;
	}
	public void setId_alumno(Integer id_alumno) {
		this.id_alumno = id_alumno;
	}
	public Integer getId_carrera() {
		return id_carrera;
	}
	public void setId_carrera(Integer id_carrera) {
		this.id_carrera = id_carrera;
	}
	public Integer getId_asignatura() {
		return id_asignatura;
	}
	public void setId_asignatura(Integer id_asignatura) {
		this.id_asignatura = id_asignatura;
	}
	public Integer getId_observacion() {
		return id_observacion;
	}
	public void setId_observacion(Integer id_observacion) {
		this.id_observacion = id_observacion;
	}
	public Integer getPeriodo() {
		return periodo;
	}
	public void setPeriodo(Integer periodo) {
		this.periodo = periodo;
	}
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	public Double getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(Double calificacion) {
		this.calificacion = calificacion;
	}
	@Override
	public String toString() {
		return "Calificacion [id_calificacion=" + id_calificacion + ", id_institucion=" + id_institucion
				+ ", id_alumno=" + id_alumno + ", id_carrera=" + id_carrera + ", id_asignatura=" + id_asignatura
				+ ", id_observacion=" + id_observacion + ", periodo=" + periodo + ", ciclo=" + ciclo + ", calificacion="
				+ calificacion + "]";
	}
		
}
