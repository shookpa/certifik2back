package com.ibm.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;


public class CalificacionInfo {
	
	private Integer id_calificacion;
	private Integer  id_alumno, id_carrera, id_asignatura,
	id_observacion,periodo;	
	private String ciclo, clave_asignatura, desc_asignatura, nombre_carrera, cve_carrera, tipo_periodo,desc_observacion;
	private Double calificacion;
	public Integer getId_calificacion() {
		return id_calificacion;
	}
	public void setId_calificacion(Integer id_calificacion) {
		this.id_calificacion = id_calificacion;
	}
	
	public Integer getId_alumno() {
		return id_alumno;
	}
	public void setId_alumno(Integer id_alumno) {
		this.id_alumno = id_alumno;
	}
	public Integer getId_carrera() {
		return id_carrera;
	}
	public void setId_carrera(Integer id_carrera) {
		this.id_carrera = id_carrera;
	}
	public Integer getId_asignatura() {
		return id_asignatura;
	}
	public void setId_asignatura(Integer id_asignatura) {
		this.id_asignatura = id_asignatura;
	}
	public Integer getId_observacion() {
		return id_observacion;
	}
	public void setId_observacion(Integer id_observacion) {
		this.id_observacion = id_observacion;
	}
	public Integer getPeriodo() {
		return periodo;
	}
	public void setPeriodo(Integer periodo) {
		this.periodo = periodo;
	}
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	public Double getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(Double calificacion) {
		this.calificacion = calificacion;
	}
	public String getNombre_carrera() {
		return nombre_carrera;
	}
	public void setNombre_carrera(String  nombre_carrera) {
		this.nombre_carrera = nombre_carrera;
	}
	public String  getCve_carrera() {
		return cve_carrera;
	}
	public void setCve_carrera(String  cve_carrera) {
		this.cve_carrera = cve_carrera;
	}
	public String  getTipo_periodo() {
		return tipo_periodo;
	}
	public void setTipo_periodo(String  tipo_periodo) {
		this.tipo_periodo = tipo_periodo;
	}
	public String  getDesc_observacion() {
		return desc_observacion;
	}
	public void setDesc_observacion(String  desc_observacion) {
		this.desc_observacion = desc_observacion;
	}
	public String getClave_asignatura() {
		return clave_asignatura;
	}
	public void setClave_asignatura(String clave_asignatura) {
		this.clave_asignatura = clave_asignatura;
	}
	public String getDesc_asignatura() {
		return desc_asignatura;
	}
	public void setDesc_asignatura(String desc_asignatura) {
		this.desc_asignatura = desc_asignatura;
	}
	@Override
	public String toString() {
		return "CalificacionInfo [id_calificacion=" + id_calificacion + ", id_alumno=" + id_alumno + ", id_carrera="
				+ id_carrera + ", id_asignatura=" + id_asignatura + ", nombre_carrera=" + nombre_carrera
				+ ", cve_carrera=" + cve_carrera + ", tipo_periodo=" + tipo_periodo + ", desc_observacion="
				+ desc_observacion + ", id_observacion=" + id_observacion + ", periodo=" + periodo + ", ciclo=" + ciclo
				+ ", clave_asignatura=" + clave_asignatura + ", desc_asignatura=" + desc_asignatura + ", calificacion="
				+ calificacion + "]";
	}
	
		
}
