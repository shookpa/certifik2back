package com.ibm.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;


public class Carrera {

	private Integer id_carrera, estatus;
	private Integer id_institucion;
	private Integer id_autorizacion_reconocimiento;
	private String cve_carrera	;
	private String nombre_carrera;
	private String nivel_educativo,clavePlan;
	private String modalidad;
	private String rvoe_dgp;
	private String tipo_periodo;
	private String idCarrera_mec;
	private String idTipoPeriodo;
	private String idNivelEstudios;
	private Date fecha_expedicion_rvoe;
	
	private Integer total_materias;
	private Double calif_minima;
	private Double calif_maxima;
	public Integer getEstatus() {
		return estatus;
	}
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	private Double calif_aprob;
	public Integer getId_carrera() {
		return id_carrera;
	}
	public void setId_carrera(Integer id_carrera) {
		this.id_carrera = id_carrera;
	}
	public Integer getId_institucion() {
		return id_institucion;
	}
	public void setId_institucion(Integer id_institucion) {
		this.id_institucion = id_institucion;
	}
	public String getCve_carrera() {
		return cve_carrera;
	}
	public void setCve_carrera(String cve_carrera) {
		this.cve_carrera = cve_carrera;
	}
	public String getNombre_carrera() {
		return nombre_carrera;
	}
	public void setNombre_carrera(String nombre_carrera) {
		this.nombre_carrera = nombre_carrera;
	}
	public String getNivel_educativo() {
		return nivel_educativo;
	}
	public void setNivel_educativo(String nivel_educativo) {
		this.nivel_educativo = nivel_educativo;
	}
	public String getModalidad() {
		return modalidad;
	}
	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}
	public String getRvoe_dgp() {
		return rvoe_dgp;
	}
	public void setRvoe_dgp(String rvoe_dgp) {
		this.rvoe_dgp = rvoe_dgp;
	}
	public String getTipo_periodo() {
		return tipo_periodo;
	}
	public void setTipo_periodo(String tipo_periodo) {
		this.tipo_periodo = tipo_periodo;
	}
	public Date getFecha_expedicion_rvoe() {
		return fecha_expedicion_rvoe;
	}
	public void setFecha_expedicion_rvoe(Date fecha_expedicion_rvoe) {
		this.fecha_expedicion_rvoe = fecha_expedicion_rvoe;
	}
	public Integer getTotal_materias() {
		return total_materias;
	}
	public void setTotal_materias(Integer total_materias) {
		this.total_materias = total_materias;
	}
	public Double getCalif_minima() {
		return calif_minima;
	}
	public void setCalif_minima(Double calif_minima) {
		this.calif_minima = calif_minima;
	}
	public Double getCalif_maxima() {
		return calif_maxima;
	}
	public void setCalif_maxima(Double calif_maxima) {
		this.calif_maxima = calif_maxima;
	}
	public Double getCalif_aprob() {
		return calif_aprob;
	}
	public void setCalif_aprob(Double calif_aprob) {
		this.calif_aprob = calif_aprob;
	}
	
	public Integer getId_autorizacion_reconocimiento() {
		return id_autorizacion_reconocimiento;
	}
	public void setId_autorizacion_reconocimiento(Integer id_autorizacion_reconocimiento) {
		this.id_autorizacion_reconocimiento = id_autorizacion_reconocimiento;
	}
	public String getClavePlan() {
		return clavePlan;
	}
	public void setClavePlan(String clavePlan) {
		this.clavePlan = clavePlan;
	}
	@Override
	public String toString() {
		return "Carrera [id_carrera=" + id_carrera + ", estatus=" + estatus + ", id_institucion=" + id_institucion
				+ ", id_autorizacion_reconocimiento=" + id_autorizacion_reconocimiento + ", cve_carrera=" + cve_carrera
				+ ", nombre_carrera=" + nombre_carrera + ", nivel_educativo=" + nivel_educativo + ", clavePlan="
				+ clavePlan + ", modalidad=" + modalidad + ", rvoe_dgp=" + rvoe_dgp + ", tipo_periodo=" + tipo_periodo
				+ ", fecha_expedicion_rvoe=" + fecha_expedicion_rvoe + ", total_materias=" + total_materias
				+ ", calif_minima=" + calif_minima + ", calif_maxima=" + calif_maxima + ", calif_aprob=" + calif_aprob
				+ "]";
	}
	public String getIdCarrera_mec() {
		return idCarrera_mec;
	}
	public void setIdCarrera_mec(String idCarrera_mec) {
		this.idCarrera_mec = idCarrera_mec;
	}
	public String getIdTipoPeriodo() {
		return idTipoPeriodo;
	}
	public void setIdTipoPeriodo(String idTipoPeriodo) {
		this.idTipoPeriodo = idTipoPeriodo;
	}
	public String getIdNivelEstudios() {
		return idNivelEstudios;
	}
	public void setIdNivelEstudios(String idNivelEstudios) {
		this.idNivelEstudios = idNivelEstudios;
	}
	
}
