package com.ibm.models;


public class CatalogSuperClass {
	
	private int key;
	
	
	@Override
	public String toString() {
		return "CatalogSuperClass [key=" + key + ", value=" + value + "]";
	}


	private String value;


	public int getKey() {
		return key;
	}


	public void setKey(int key) {
		this.key = key;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	
	
	

}
