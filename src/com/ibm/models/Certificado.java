package com.ibm.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;


public class Certificado {
	private Integer id_certificado;
	private Integer id_institucion, id_firmante, id_carrera, id_alumno, estatus_certificado,
	total_materias,total_asignadas,total_aprobadas, numero_ciclos;
	public Integer getNumero_ciclos() {
		return numero_ciclos;
	}
	public void setNumero_ciclos(Integer numero_ciclos) {
		this.numero_ciclos = numero_ciclos;
	}
	private Double promedio, total_creditos,creditos_obtenidos;
	private String matricula, folioControl;

	
	private String tipo_certif,num_certif;
	private Date fecha_expedicion,
			fecha_registro, fecha_modificacion;
	public Integer getId_certificado() {
		return id_certificado;
	}
	public void setId_certificado(Integer id_certificado) {
		this.id_certificado = id_certificado;
	}
	public Integer getId_institucion() {
		return id_institucion;
	}
	public void setId_institucion(Integer id_institucion) {
		this.id_institucion = id_institucion;
	}
	
	public Integer getId_carrera() {
		return id_carrera;
	}
	public void setId_carrera(Integer id_carrera) {
		this.id_carrera = id_carrera;
	}
	public Integer getId_alumno() {
		return id_alumno;
	}
	public void setId_alumno(Integer id_alumno) {
		this.id_alumno = id_alumno;
	}
	public Integer getEstatus_certificado() {
		return estatus_certificado;
	}
	public void setEstatus_certificado(Integer estatus_certificado) {
		this.estatus_certificado = estatus_certificado;
	}
	public Integer getTotal_materias() {
		return total_materias;
	}
	public void setTotal_materias(Integer total_materias) {
		this.total_materias = total_materias;
	}
	public Integer getTotal_asignadas() {
		return total_asignadas;
	}
	public void setTotal_asignadas(Integer total_asignadas) {
		this.total_asignadas = total_asignadas;
	}
	public Integer getTotal_aprobadas() {
		return total_aprobadas;
	}
	public void setTotal_aprobadas(Integer total_aprobadas) {
		this.total_aprobadas = total_aprobadas;
	}
	public Double getTotal_creditos() {
		return total_creditos;
	}
	public void setTotal_creditos(Double total_creditos) {
		this.total_creditos = total_creditos;
	}
	public Double getCreditos_obtenidos() {
		return creditos_obtenidos;
	}
	public void setCreditos_obtenidos(Double creditos_obtenidos) {
		this.creditos_obtenidos = creditos_obtenidos;
	}
	public Double getPromedio() {
		return promedio;
	}
	public void setPromedio(Double promedio) {
		this.promedio = promedio;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getTipo_certif() {
		return tipo_certif;
	}
	public void setTipo_certif(String tipo_certif) {
		this.tipo_certif = tipo_certif;
	}
	public String getNum_certif() {
		return num_certif;
	}
	public void setNum_certif(String num_certif) {
		this.num_certif = num_certif;
	}
	public Date getFecha_expedicion() {
		return fecha_expedicion;
	}
	public void setFecha_expedicion(Date fecha_expedicion) {
		this.fecha_expedicion = fecha_expedicion;
	}
	public Date getFecha_registro() {
		return fecha_registro;
	}
	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}
	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}
	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}
	
	public String getFolioControl() {
		return folioControl;
	}
	public void setFolioControl(String folioControl) {
		this.folioControl = folioControl;
	}
	public Integer getId_firmante() {
		return id_firmante;
	}
	public void setId_firmante(Integer id_firmante) {
		this.id_firmante = id_firmante;
	}
	@Override
	public String toString() {
		return "Certificado [id_certificado=" + id_certificado + ", id_institucion=" + id_institucion + ", id_firmante="
				+ id_firmante + ", id_carrera=" + id_carrera + ", id_alumno=" + id_alumno + ", estatus_certificado="
				+ estatus_certificado + ", total_materias=" + total_materias + ", total_asignadas=" + total_asignadas
				+ ", total_aprobadas=" + total_aprobadas + ", total_creditos=" + total_creditos
				+ ", creditos_obtenidos=" + creditos_obtenidos + ", promedio=" + promedio + ", matricula=" + matricula
				+ ", folioControl=" + folioControl + ", tipo_certif=" + tipo_certif + ", num_certif=" + num_certif
				+ ", fecha_expedicion=" + fecha_expedicion + ", fecha_registro=" + fecha_registro
				+ ", fecha_modificacion=" + fecha_modificacion + "]";
	}
	

}
