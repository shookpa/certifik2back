package com.ibm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

public class ClaveValor {

	private Object clave;
	
	public Object getClave() {
		return clave;
	}
	public void setClave(Object clave) {
		this.clave = clave;
	}
	public Object getValor() {
		return valor;
	}
	public void setValor(Object valor) {
		this.valor = valor;
	}
	private Object valor;
	
	@Override
	public String toString() {
		return "ClaveValor [getClave()=" + getClave() + ", getValor()=" + getValor() + "]";
	}

}
