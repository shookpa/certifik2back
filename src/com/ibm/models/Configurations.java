package com.ibm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;


public class Configurations {

	private Integer id_config;
	private Integer id_institucion;
	private Integer id_tipoAmbiente_tit;
	private String password_tit;
	private String usuario_tit;	
	private String url_tit;
	
	private Integer id_tipoAmbiente_cert;
	private String password_cert;
	private String usuario_cert;	
	private String url_cert;
	
	
	public Integer getId_tipoAmbiente_tit() {
		return id_tipoAmbiente_tit;
	}
	public void setId_tipoAmbiente_tit(Integer id_tipoAmbiente_tit) {
		this.id_tipoAmbiente_tit = id_tipoAmbiente_tit;
	}
	public String getPassword_tit() {
		return password_tit;
	}
	public void setPassword_tit(String password_tit) {
		this.password_tit = password_tit;
	}
	public String getUsuario_tit() {
		return usuario_tit;
	}
	public void setUsuario_tit(String usuario_tit) {
		this.usuario_tit = usuario_tit;
	}
	public String getUrl_tit() {
		return url_tit;
	}
	public void setUrl_tit(String url_tit) {
		this.url_tit = url_tit;
	}
	public Integer getId_tipoAmbiente_cert() {
		return id_tipoAmbiente_cert;
	}
	public void setId_tipoAmbiente_cert(Integer id_tipoAmbiente_cert) {
		this.id_tipoAmbiente_cert = id_tipoAmbiente_cert;
	}
	public String getPassword_cert() {
		return password_cert;
	}
	public void setPassword_cert(String password_cert) {
		this.password_cert = password_cert;
	}
	public String getUsuario_cert() {
		return usuario_cert;
	}
	public void setUsuario_cert(String usuario_cert) {
		this.usuario_cert = usuario_cert;
	}
	public String getUrl_cert() {
		return url_cert;
	}
	public void setUrl_cert(String url_cert) {
		this.url_cert = url_cert;
	}
	public Integer getId_config() {
		return id_config;
	}
	public void setId_config(Integer id_config) {
		this.id_config = id_config;
	}
	public Integer getId_institucion() {
		return id_institucion;
	}
	public void setId_institucion(Integer id_institucion) {
		this.id_institucion = id_institucion;
	}
	@Override
	public String toString() {
		return "SEPUser [id_config=" + id_config + ", id_institucion=" + id_institucion + ", id_tipoAmbiente_tit="
				+ id_tipoAmbiente_tit + ", password_tit=" + password_tit + ", usuario_tit=" + usuario_tit + ", url_tit="
				+ url_tit + ", id_tipoAmbiente_cert=" + id_tipoAmbiente_cert + ", password_cert=" + password_cert
				+ ", usuario_cert=" + usuario_cert + ", url_cert=" + url_cert + "]";
	}

	
	

	
}
