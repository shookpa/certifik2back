package com.ibm.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

public class CreditosCarrera {

	private Integer id_carrera;
	private String nombre_carrera;
	private Integer total_asignadas;
	private Double total_creditos;
	
	@Override
	public String toString() {
		return "CreditosCarrera [id_carrera=" + id_carrera + ", nombre_carrera=" + nombre_carrera + ", total_asignadas="
				+ total_asignadas + ", total_creditos=" + total_creditos + "]";
	}
	public Integer getId_carrera() {
		return id_carrera;
	}
	public void setId_carrera(Integer id_carrera) {
		this.id_carrera = id_carrera;
	}
	public String getNombre_carrera() {
		return nombre_carrera;
	}
	public void setNombre_carrera(String nombre_carrera) {
		this.nombre_carrera = nombre_carrera;
	}
	public Integer getTotal_asignadas() {
		return total_asignadas;
	}
	public void setTotal_asignadas(Integer total_asignadas) {
		this.total_asignadas = total_asignadas;
	}
	public Double getTotal_creditos() {
		return total_creditos;
	}
	public void setTotal_creditos(Double total_creditos) {
		this.total_creditos = total_creditos;
	}
	

}
