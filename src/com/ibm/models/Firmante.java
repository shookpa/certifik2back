package com.ibm.models;

public class Firmante {

	@Override
	public String toString() {
		return "Firmante [id_responsable=" + id_responsable + ", id_institucion=" + id_institucion + ", idCargo="
				+ idCargo + ", nombre=" + nombre + ", primerApellido=" + primerApellido + ", segundoApellido="
				+ segundoApellido + ", curp=" + curp + ", abrTitulo=" + abrTitulo + ", certificadoResponsable="
				+ certificadoResponsable + ", keyFile=" + keyFile
				+ ", pwdCertificado=" + pwdCertificado + ", noCertificadoResponsable=" + noCertificadoResponsable + "]";
	}

	private Integer id_responsable, id_institucion, idCargo,status;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	private String nombre, primerApellido, segundoApellido, curp, abrTitulo, certificadoResponsable, keyFile,
			 pwdCertificado, noCertificadoResponsable;

	public String getNoCertificadoResponsable() {
		return noCertificadoResponsable;
	}

	public void setNoCertificadoResponsable(String noCertificadoResponsable) {
		this.noCertificadoResponsable = noCertificadoResponsable;
	}

	public Integer getId_responsable() {
		return id_responsable;
	}

	public String getKeyFile() {
		return keyFile;
	}

	public void setKeyFile(String keyFile) {
		this.keyFile = keyFile;
	}

	public String getPwdCertificado() {
		return pwdCertificado;
	}

	public void setPwdCertificado(String pwdCertificado) {
		this.pwdCertificado = pwdCertificado;
	}

	public void setId_responsable(Integer id_responsable) {
		this.id_responsable = id_responsable;
	}

	public Integer getId_institucion() {
		return id_institucion;
	}

	public void setId_institucion(Integer id_institucion) {
		this.id_institucion = id_institucion;
	}

	public Integer getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(Integer idCargo) {
		this.idCargo = idCargo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getAbrTitulo() {
		return abrTitulo;
	}

	public void setAbrTitulo(String abrTitulo) {
		this.abrTitulo = abrTitulo;
	}

	public String getCertificadoResponsable() {
		return certificadoResponsable;
	}

	public void setCertificadoResponsable(String certificadoResponsable) {
		this.certificadoResponsable = certificadoResponsable;
	}

	

}
