package com.ibm.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@Entity(name = "license_information")
@XmlRootElement(name = "license_information")
public class License {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_license;
	private Date fecha_creacion;
	private Date fecha_expiracion;
	private String fecha_expiracionStr;
	private String codigo_licencia;
	public String getCodigo_licencia() {
		return codigo_licencia;
	}
	public void setCodigo_licencia(String codigo_licencia) {
		this.codigo_licencia = codigo_licencia;
	}
	private Integer timbres_usados , timbres_disponibles;	
	
	public int getId_license() {
		return id_license;
	}
	public void setId_license(int id_license) {
		this.id_license = id_license;
	}
	public Date getFecha_creacion() {
		return fecha_creacion;
	}
	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}
	public Date getFecha_expiracion() {
		return fecha_expiracion;
	}
	public void setFecha_expiracion(Date fecha_expiracion) {
		this.fecha_expiracion = fecha_expiracion;
	}
	public Integer getTimbres_usados() {
		return timbres_usados;
	}
	public void setTimbres_usados(Integer timbres_usados) {
		this.timbres_usados = timbres_usados;
	}
	public Integer getTimbres_disponibles() {
		return timbres_disponibles;
	}
	public void setTimbres_disponibles(Integer timbres_disponibles) {
		this.timbres_disponibles = timbres_disponibles;
	}
	
	@Override
	public String toString() {
		return "License [id_license=" + id_license + ", fecha_creacion=" + fecha_creacion + ", fecha_expiracion="
				+ fecha_expiracion + ", timbres_usados=" + timbres_usados + ", timbres_disponibles="
				+ timbres_disponibles + "]";
	}
	public String getFecha_expiracionStr() {
		return fecha_expiracionStr;
	}
	public void setFecha_expiracionStr(String fecha_expiracionStr) {
		this.fecha_expiracionStr = fecha_expiracionStr;
	}
	
	

	

	

}
