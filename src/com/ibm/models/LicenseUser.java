package com.ibm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@Entity(name = "license_information")
@XmlRootElement(name = "license_information")
public class LicenseUser {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_license;
	
	private String fecha_creacion;
	private String fecha_expiracion;
	private String timbres_usados,codigo_licencia;
	public String getCodigo_licencia() {
		return codigo_licencia;
	}
	public void setCodigo_licencia(String codigo_licencia) {
		this.codigo_licencia = codigo_licencia;
	}
	private String timbres_disponibles;	
	private int id_usuario;
	public int getId_license() {
		return id_license;
	}
	public void setId_license(int id_license) {
		this.id_license = id_license;
	}
	public String getFecha_creacion() {
		return fecha_creacion;
	}
	public void setFecha_creacion(String fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}
	public String getFecha_expiracion() {
		return fecha_expiracion;
	}
	public void setFecha_expiracion(String fecha_expiracion) {
		this.fecha_expiracion = fecha_expiracion;
	}
	public String getTimbres_usados() {
		return timbres_usados;
	}
	public void setTimbres_usados(String timbres_usados) {
		this.timbres_usados = timbres_usados;
	}
	public String getTimbres_disponibles() {
		return timbres_disponibles;
	}
	public void setTimbres_disponibles(String timbres_disponibles) {
		this.timbres_disponibles = timbres_disponibles;
	}
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	@Override
	public String toString() {
		return "License [id_license=" + id_license + ", fecha_creacion=" + fecha_creacion + ", fecha_expiracion="
				+ fecha_expiracion + ", timbres_usados=" + timbres_usados + ", timbres_disponibles="
				+ timbres_disponibles + ", id_usuario=" + id_usuario + "]";
	}
	
	

	

	

}
