package com.ibm.models;



public class MotivoCancelacionShort {

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	private String value, key;

	@Override
	public String toString() {
		return "MotivoCancelacionShort [value=" + value + ", key=" + key + "]";
	}
	
 
}
