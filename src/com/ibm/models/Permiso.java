package com.ibm.models;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;


public class Permiso {





private int id_permiso;
private String modulo, func1, func2, func3, func4, func5, func6, func7, func8, func9, func10, func11, func12, func13, func14, func15, func16, func17, func18;
public int getId_permiso() {
	return id_permiso;
}
public void setId_permiso(int id_permiso) {
	this.id_permiso = id_permiso;
}
public String getModulo() {
	return modulo;
}
public void setModulo(String modulo) {
	this.modulo = modulo;
}
public String getFunc1() {
	return func1;
}
public void setFunc1(String func1) {
	this.func1 = func1;
}
public String getFunc2() {
	return func2;
}
public void setFunc2(String func2) {
	this.func2 = func2;
}
public String getFunc3() {
	return func3;
}
public void setFunc3(String func3) {
	this.func3 = func3;
}
public String getFunc4() {
	return func4;
}
public void setFunc4(String func4) {
	this.func4 = func4;
}
public String getFunc5() {
	return func5;
}
public void setFunc5(String func5) {
	this.func5 = func5;
}
public String getFunc6() {
	return func6;
}
public void setFunc6(String func6) {
	this.func6 = func6;
}
public String getFunc7() {
	return func7;
}
public void setFunc7(String func7) {
	this.func7 = func7;
}
public String getFunc8() {
	return func8;
}
public void setFunc8(String func8) {
	this.func8 = func8;
}
public String getFunc9() {
	return func9;
}
public void setFunc9(String func9) {
	this.func9 = func9;
}
public String getFunc10() {
	return func10;
}
public void setFunc10(String func10) {
	this.func10 = func10;
}
public String getFunc11() {
	return func11;
}
public void setFunc11(String func11) {
	this.func11 = func11;
}
public String getFunc12() {
	return func12;
}
public void setFunc12(String func12) {
	this.func12 = func12;
}
public String getFunc13() {
	return func13;
}
public void setFunc13(String func13) {
	this.func13 = func13;
}
public String getFunc14() {
	return func14;
}
public void setFunc14(String func14) {
	this.func14 = func14;
}
public String getFunc15() {
	return func15;
}
public void setFunc15(String func15) {
	this.func15 = func15;
}
public String getFunc16() {
	return func16;
}
public void setFunc16(String func16) {
	this.func16 = func16;
}
public String getFunc17() {
	return func17;
}
public void setFunc17(String func17) {
	this.func17 = func17;
}
public String getFunc18() {
	return func18;
}
public void setFunc18(String func18) {
	this.func18 = func18;
}



}
