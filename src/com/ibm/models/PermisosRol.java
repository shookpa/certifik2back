package com.ibm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

public class PermisosRol {

	private int id, id_rol, id_permiso;
	private Integer per_modulo, per_func1, per_func2, per_func3, per_func4, per_func5, per_func6, per_func7, per_func8,
			per_func9, per_func10, per_func11, per_func12, per_func13, per_func14, per_func15, per_func16, per_func17,
			per_func18;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_rol() {
		return id_rol;
	}

	public void setId_rol(int id_rol) {
		this.id_rol = id_rol;
	}

	public int getId_permiso() {
		return id_permiso;
	}

	public void setId_permiso(int id_permiso) {
		this.id_permiso = id_permiso;
	}

	public Integer getPer_modulo() {
		return per_modulo;
	}

	public void setPer_modulo(Integer per_modulo) {
		this.per_modulo = per_modulo;
	}

	public Integer getPer_func1() {
		return per_func1;
	}

	public void setPer_func1(Integer per_func1) {
		this.per_func1 = per_func1;
	}

	public Integer getPer_func2() {
		return per_func2;
	}

	public void setPer_func2(Integer per_func2) {
		this.per_func2 = per_func2;
	}

	public Integer getPer_func3() {
		return per_func3;
	}

	public void setPer_func3(Integer per_func3) {
		this.per_func3 = per_func3;
	}

	public Integer getPer_func4() {
		return per_func4;
	}

	public void setPer_func4(Integer per_func4) {
		this.per_func4 = per_func4;
	}

	public Integer getPer_func5() {
		return per_func5;
	}

	public void setPer_func5(Integer per_func5) {
		this.per_func5 = per_func5;
	}

	public Integer getPer_func6() {
		return per_func6;
	}

	public void setPer_func6(Integer per_func6) {
		this.per_func6 = per_func6;
	}

	public Integer getPer_func7() {
		return per_func7;
	}

	public void setPer_func7(Integer per_func7) {
		this.per_func7 = per_func7;
	}

	public Integer getPer_func8() {
		return per_func8;
	}

	public void setPer_func8(Integer per_func8) {
		this.per_func8 = per_func8;
	}

	public Integer getPer_func9() {
		return per_func9;
	}

	public void setPer_func9(Integer per_func9) {
		this.per_func9 = per_func9;
	}

	public Integer getPer_func10() {
		return per_func10;
	}

	public void setPer_func10(Integer per_func10) {
		this.per_func10 = per_func10;
	}

	public Integer getPer_func11() {
		return per_func11;
	}

	public void setPer_func11(Integer per_func11) {
		this.per_func11 = per_func11;
	}

	public Integer getPer_func12() {
		return per_func12;
	}

	public void setPer_func12(Integer per_func12) {
		this.per_func12 = per_func12;
	}

	public Integer getPer_func13() {
		return per_func13;
	}

	public void setPer_func13(Integer per_func13) {
		this.per_func13 = per_func13;
	}

	public Integer getPer_func14() {
		return per_func14;
	}

	public void setPer_func14(Integer per_func14) {
		this.per_func14 = per_func14;
	}

	public Integer getPer_func15() {
		return per_func15;
	}

	public void setPer_func15(Integer per_func15) {
		this.per_func15 = per_func15;
	}

	public Integer getPer_func16() {
		return per_func16;
	}

	public void setPer_func16(Integer per_func16) {
		this.per_func16 = per_func16;
	}

	public Integer getPer_func17() {
		return per_func17;
	}

	public void setPer_func17(Integer per_func17) {
		this.per_func17 = per_func17;
	}

	public Integer getPer_func18() {
		return per_func18;
	}

	public void setPer_func18(Integer per_func18) {
		this.per_func18 = per_func18;
	}

	@Override
	public String toString() {
		return "PermisosRol [id=" + id + ", id_rol=" + id_rol + ", id_permiso=" + id_permiso + ", per_modulo="
				+ per_modulo + ", per_func1=" + per_func1 + ", per_func2=" + per_func2 + ", per_func3=" + per_func3
				+ ", per_func4=" + per_func4 + ", per_func5=" + per_func5 + ", per_func6=" + per_func6 + ", per_func7="
				+ per_func7 + ", per_func8=" + per_func8 + ", per_func9=" + per_func9 + ", per_func10=" + per_func10
				+ ", per_func11=" + per_func11 + ", per_func12=" + per_func12 + ", per_func13=" + per_func13
				+ ", per_func14=" + per_func14 + ", per_func15=" + per_func15 + ", per_func16=" + per_func16
				+ ", per_func17=" + per_func17 + ", per_func18=" + per_func18 + "]";
	}
}
