package com.ibm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

public class ProyectosPorRepositorio {

	private String pais;
	private String nombreRepo;	
	private Double plastic;
	private int total;
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getNombreRepo() {
		return nombreRepo;
	}
	public void setNombreRepo(String nombreRepo) {
		this.nombreRepo = nombreRepo;
	}
	
	public Double getPlastic() {
		return plastic;
	}
	public void setPlastic(Double plastic) {
		this.plastic = plastic;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	@Override
	public String toString() {
		return "TotalProyectos [getPais()=" + getPais() + ", getNombreRepo()=" + getNombreRepo() + ", getPlastic()=" + getPlastic() + ", getTotal()=" + getTotal() + "]";
	}
	
}
