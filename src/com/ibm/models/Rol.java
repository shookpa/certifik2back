package com.ibm.models;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;


public class Rol {


private String desc_rol;

@Id
@GeneratedValue(strategy=GenerationType.AUTO)
private int id_rol, status; 

public int getStatus() {
	return status;
}
public void setStatus(int status) {
	this.status = status;
}
public int getId_rol() {
	return id_rol;
}
public void setId_rol(int id_rol) {
	this.id_rol = id_rol;
}

public String getDesc_rol() {
	return desc_rol;
}
public void setDesc_rol(String desc_rol) {
	this.desc_rol = desc_rol;
}




}
