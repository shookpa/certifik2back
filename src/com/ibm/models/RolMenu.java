package com.ibm.models;

public class RolMenu {

	
	public int id_menu;
	public int id_rol;	
	public String nombre_menu;
	public String class_i;
	public String class_s;
	public String href;
	public int nivel;
	public Integer id_padre;
	public int status;
	
	
	
	public int getId_menu() {
		return id_menu;
	}
	public void setId_menu(int id_menu) {
		this.id_menu = id_menu;
	}
	public String getNombre_menu() {
		return nombre_menu;
	}
	public void setNombre_menu(String nombre_menu) {
		this.nombre_menu = nombre_menu;
	}
	
	public String getClass_i() {
		return class_i;
	}
	public void setClass_i(String class_i) {
		this.class_i = class_i;
	}
	public String getClass_s() {
		return class_s;
	}
	public void setClass_s(String class_s) {
		this.class_s = class_s;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public int getNivel() {
		return nivel;
	}
	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	public Integer getId_padre() {
		return id_padre;
	}
	public void setId_padre(Integer id_padre) {
		this.id_padre = id_padre;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getId_rol() {
		return id_rol;
	}
	public void setId_rol(int id_rol) {
		this.id_rol = id_rol;
	}
	@Override
	public String toString() {
		return "RolMenu [getId_menu()=" + getId_menu() + ", getNombre_menu()=" + getNombre_menu() + ", getClass_i()=" + getClass_i() + ", getClass_s()=" + getClass_s() + ", getHref()=" + getHref() + ", getNivel()=" + getNivel() + ", getId_padre()="
				+ getId_padre() + ", getStatus()=" + getStatus() + ", getId_rol()=" + getId_rol() + "]";
	}
	
	
}
