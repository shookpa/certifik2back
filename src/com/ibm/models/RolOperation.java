package com.ibm.models;

public class RolOperation {

	private int id_operation;
	public int getId_operation() {
		return id_operation;
	}

	public void setId_operation(int id_operation) {
		this.id_operation = id_operation;
	}

	private int id_rol;
	private String path_service;
	private String path_operation;
	
	
	public RolOperation(){}
	
	public RolOperation(int id_rol, String path_service, String path_operation) {
		super();
		this.id_rol = id_rol;
		this.path_service = path_service;
		this.path_operation = path_operation;
	}
	
	
	
	
	public int getId_rol() {
		return id_rol;
	}

	public void setId_rol(int id_rol) {
		this.id_rol = id_rol;
	}

	public String getPath_service() {
		return path_service;
	}
	public void setPath_service(String path_service) {
		this.path_service = path_service;
	}
	public String getPath_operation() {
		return path_operation;
	}
	public void setPath_operation(String path_operation) {
		this.path_operation = path_operation;
	}

	@Override
	public String toString() {
		return "RolOperation [id_operation=" + id_operation + ", id_rol=" + id_rol + ", path_service=" + path_service
				+ ", path_operation=" + path_operation + "]";
	}
	
	
	
	
}
