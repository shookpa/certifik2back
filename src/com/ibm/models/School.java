package com.ibm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;


public class School {
	private int id_institucion, id_entidad_federativa;
	
	private String cveInstitucion;
	private String nombreInstitucion;
	private String campus;
	private String idNombreInstitucion;
	private String rfc;
	private String razonSocial;	
	private int titulos_timbrados, estatus;
	private String localidad;	
	public int getId_institucion() {
		return id_institucion;
	}
	public void setId_institucion(int id_institucion) {
		this.id_institucion = id_institucion;
	}
	public String getCveInstitucion() {
		return cveInstitucion;
	}
	public void setCveInstitucion(String cveInstitucion) {
		this.cveInstitucion = cveInstitucion;
	}
	public String getNombreInstitucion() {
		return nombreInstitucion;
	}
	public void setNombreInstitucion(String nombreInstitucion) {
		this.nombreInstitucion = nombreInstitucion;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public int getTitulos_timbrados() {
		return titulos_timbrados;
	}
	public void setTitulos_timbrados(int titulos_timbrados) {
		this.titulos_timbrados = titulos_timbrados;
	}
	public String getLocalidad() {
		return localidad;
	}
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	
	@Override
	public String toString() {
		return "School [id_institucion=" + id_institucion + ", cveInstitucion="
				+ cveInstitucion + ", nombreInstitucion=" + nombreInstitucion + ", rfc=" + rfc + ", razonSocial="
				+ razonSocial + ", titulos_timbrados=" + titulos_timbrados + ", localidad=" + localidad + "]";
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	public int getId_entidad_federativa() {
		return id_entidad_federativa;
	}
	public void setId_entidad_federativa(int id_entidad_federativa) {
		this.id_entidad_federativa = id_entidad_federativa;
	}
	public String getIdNombreInstitucion() {
		return idNombreInstitucion;
	}
	public void setIdNombreInstitucion(String idNombreInstitucion) {
		this.idNombreInstitucion = idNombreInstitucion;
	}
	public String getCampus() {
		return campus;
	}
	public void setCampus(String campus) {
		this.campus = campus;
	}
	
		

}
