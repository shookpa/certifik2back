package com.ibm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;


public class SchoolsUser {
	
	private int id_institucion;
	private int id_usuario,rol;	
	private String cveInstitucion,user, nombre, desc_rol;
	private String nombreInstitucion;
	private String rfc;
	private String razonSocial;	
	private int titulos_timbrados,id_entidad_federativa;
	private String localidad;	
	public int getRol() {
		return rol;
	}
	public void setRol(int rol) {
		this.rol = rol;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDesc_rol() {
		return desc_rol;
	}
	public void setDesc_rol(String desc_rol) {
		this.desc_rol = desc_rol;
	}
	public int getId_institucion() {
		return id_institucion;
	}
	public void setId_institucion(int id_institucion) {
		this.id_institucion = id_institucion;
	}
	public String getCveInstitucion() {
		return cveInstitucion;
	}
	public void setCveInstitucion(String cveInstitucion) {
		this.cveInstitucion = cveInstitucion;
	}
	public String getNombreInstitucion() {
		return nombreInstitucion;
	}
	public void setNombreInstitucion(String nombreInstitucion) {
		this.nombreInstitucion = nombreInstitucion;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public int getTitulos_timbrados() {
		return titulos_timbrados;
	}
	public void setTitulos_timbrados(int titulos_timbrados) {
		this.titulos_timbrados = titulos_timbrados;
	}
	public String getLocalidad() {
		return localidad;
	}
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	public int getId_entidad_federativa() {
		return id_entidad_federativa;
	}
	public void setId_entidad_federativa(int id_entidad_federativa) {
		this.id_entidad_federativa = id_entidad_federativa;
	}
	@Override
	public String toString() {
		return "SchoolsUser [id_institucion=" + id_institucion + ", id_usuario=" + id_usuario + ", rol=" + rol
				+ ", cveInstitucion=" + cveInstitucion + ", user=" + user + ", nombre=" + nombre + ", desc_rol="
				+ desc_rol + ", nombreInstitucion=" + nombreInstitucion + ", rfc=" + rfc + ", razonSocial="
				+ razonSocial + ", titulos_timbrados=" + titulos_timbrados + ", id_entidad_federativa="
				+ id_entidad_federativa + ", localidad=" + localidad + "]";
	}
	
	
		

}
