package com.ibm.models;

import java.util.Date;

public class Student {
	private int id_alumno;
	private int id_institucion;
	private String matricula;
	private String curp;
	private String sexo;
	private String generacion;
	private int id_carrera, estatus;

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	private Date f_inicio_carrera, f_fin_carrera;
	private String nombre;
	private String primerApellido;
	private String segundoApellido;
	private String correoElectronico;

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getGeneracion() {
		return generacion;
	}

	public void setGeneracion(String generacion) {
		this.generacion = generacion;
	}

	public int getId_carrera() {
		return id_carrera;
	}

	public void setId_carrera(int id_carrera) {
		this.id_carrera = id_carrera;
	}

	public Date getF_inicio_carrera() {
		return f_inicio_carrera;
	}

	public void setF_inicio_carrera(Date f_inicio_carrera) {
		this.f_inicio_carrera = f_inicio_carrera;
	}

	public Date getF_fin_carrera() {
		return f_fin_carrera;
	}

	public void setF_fin_carrera(Date f_fin_carrera) {
		this.f_fin_carrera = f_fin_carrera;
	}

	public int getId_alumno() {
		return id_alumno;
	}

	public void setId_alumno(int id_alumno) {
		this.id_alumno = id_alumno;
	}

	public int getId_institucion() {
		return id_institucion;
	}

	public void setId_institucion(int id_institucion) {
		this.id_institucion = id_institucion;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	@Override
	public String toString() {
		return "Student [id_alumno=" + id_alumno + ", id_institucion=" + id_institucion + ", matricula=" + matricula
				+ ", curp=" + curp + ", sexo=" + sexo + ", generacion=" + generacion + ", id_carrera=" + id_carrera
				+ ", f_inicio_carrera=" + f_inicio_carrera + ", f_fin_carrera=" + f_fin_carrera + ", nombre=" + nombre
				+ ", primerApellido=" + primerApellido + ", segundoApellido=" + segundoApellido + ", correoElectronico="
				+ correoElectronico + "]";
	}

}
