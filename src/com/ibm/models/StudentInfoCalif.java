package com.ibm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@Entity(name = "license_information")
@XmlRootElement(name = "license_information")
public class StudentInfoCalif {
	
	private int id_institucion;
	private int id_carrera;
	
	private String matricula;	
	private String nombre;
	private String primerApellido;
	private String segundoApellido;
	private String nombre_carrera;
	private String estatus_certif;
	
	private Integer total_materias;
	private Integer asignadas;
	private Integer aprobadas;
	private Double promedio;
	private Double creditos_obtenidos;
	private Double total_creditos;
	public int getId_institucion() {
		return id_institucion;
	}
	public void setId_institucion(int id_institucion) {
		this.id_institucion = id_institucion;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPrimerApellido() {
		return primerApellido;
	}
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	public String getSegundoApellido() {
		return segundoApellido;
	}
	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}
	
	public String getNombre_carrera() {
		return nombre_carrera;
	}
	public void setNombre_carrera(String nombre_carrera) {
		this.nombre_carrera = nombre_carrera;
	}
	public Integer getTotal_materias() {
		return total_materias;
	}
	public void setTotal_materias(Integer total_materias) {
		this.total_materias = total_materias;
	}
	public Integer getAsignadas() {
		return asignadas;
	}
	public void setAsignadas(Integer asignadas) {
		this.asignadas = asignadas;
	}
	public Integer getAprobadas() {
		return aprobadas;
	}
	public void setAprobadas(Integer aprobadas) {
		this.aprobadas = aprobadas;
	}
	public Double getPromedio() {
		return promedio;
	}
	public void setPromedio(Double promedio) {
		this.promedio = promedio;
	}
	public Double getCreditos_obtenidos() {
		return creditos_obtenidos;
	}
	public void setCreditos_obtenidos(Double creditos_obtenidos) {
		this.creditos_obtenidos = creditos_obtenidos;
	}
	public Double getTotal_creditos() {
		return total_creditos;
	}
	public void setTotal_creditos(Double total_creditos) {
		this.total_creditos = total_creditos;
	}
	public int getId_carrera() {
		return id_carrera;
	}
	public void setId_carrera(int id_carrera) {
		this.id_carrera = id_carrera;
	}
	public String getEstatus_certif() {
		return estatus_certif;
	}
	public void setEstatus_certif(String estatus_certif) {
		this.estatus_certif = estatus_certif;
	}
}
