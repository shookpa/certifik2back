package com.ibm.models;

import java.math.BigInteger;
import java.util.Date;

public class Titulo {
	private Integer id_titulo;
	private BigInteger numeroLote;
	private Integer id_institucion, id_carrera, expedicion_idModalidadTitulacion,
			expedicion_idFundamentoLegalServicioSocial, expedicion_idEntidadFederativa,
			antecedente_idTipoEstudioAntecedente, antecedente_idEntidadFederativa, estatus_titulo;
	private Integer expedicion_cumplioServicioSocial;
	private String matricula, folioControl, descEstatusEnvio, motivoCancelacion;
	

	private String antecedente_institucionProcedencia,
			antecedente_noCedula;
	private Date expedicion_fechaExpedicion, expedicion_fechaExamenProfesional,
			expedicion_fechaExencionExamenProfesional, antecedente_fechaInicio, antecedente_fechaTerminacion,
			fecha_registro, fecha_modificacion, carrera_fechaTermino, carrera_fechaInicio, estudios_fechaTermino;
	public String getMotivoCancelacion() {
		return motivoCancelacion;
	}

	public void setMotivoCancelacion(String motivoCancelacion) {
		this.motivoCancelacion = motivoCancelacion;
	}
	public Date getEstudios_fechaTermino() {
		return estudios_fechaTermino;
	}

	public void setEstudios_fechaTermino(Date estudios_FechaTermino) {
		this.estudios_fechaTermino = estudios_FechaTermino;
	}

	public BigInteger getNumeroLote() {
		return numeroLote;
	}

	public void setNumeroLote(BigInteger numeroLote) {
		this.numeroLote = numeroLote;
	}

	public String getFolioControl() {
		return folioControl;
	}

	public void setFolioControl(String folioControl) {
		this.folioControl = folioControl;
	}

	public String getDescEstatusEnvio() {
		return descEstatusEnvio;
	}

	public void setDescEstatusEnvio(String descEstatusEnvio) {
		this.descEstatusEnvio = descEstatusEnvio;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public Integer getId_titulo() {
		return id_titulo;
	}

	public void setId_titulo(Integer id_titulo) {
		this.id_titulo = id_titulo;
	}

	public Integer getId_institucion() {
		return id_institucion;
	}

	public void setId_institucion(Integer id_institucion) {
		this.id_institucion = id_institucion;
	}

	public Integer getId_carrera() {
		return id_carrera;
	}

	public void setId_carrera(Integer id_carrera) {
		this.id_carrera = id_carrera;
	}

	public Integer getExpedicion_idModalidadTitulacion() {
		return expedicion_idModalidadTitulacion;
	}

	public void setExpedicion_idModalidadTitulacion(Integer expedicion_idModalidadTitulacion) {
		this.expedicion_idModalidadTitulacion = expedicion_idModalidadTitulacion;
	}

	public Integer getExpedicion_idFundamentoLegalServicioSocial() {
		return expedicion_idFundamentoLegalServicioSocial;
	}

	public void setExpedicion_idFundamentoLegalServicioSocial(Integer expedicion_idFundamentoLegalServicioSocial) {
		this.expedicion_idFundamentoLegalServicioSocial = expedicion_idFundamentoLegalServicioSocial;
	}

	public Integer getExpedicion_idEntidadFederativa() {
		return expedicion_idEntidadFederativa;
	}

	public void setExpedicion_idEntidadFederativa(Integer expedicion_idEntidadFederativa) {
		this.expedicion_idEntidadFederativa = expedicion_idEntidadFederativa;
	}

	public Integer getAntecedente_idTipoEstudioAntecedente() {
		return antecedente_idTipoEstudioAntecedente;
	}

	public void setAntecedente_idTipoEstudioAntecedente(Integer antecedente_idTipoEstudioAntecedente) {
		this.antecedente_idTipoEstudioAntecedente = antecedente_idTipoEstudioAntecedente;
	}

	public Integer getAntecedente_idEntidadFederativa() {
		return antecedente_idEntidadFederativa;
	}

	public void setAntecedente_idEntidadFederativa(Integer antecedente_idEntidadFederativa) {
		this.antecedente_idEntidadFederativa = antecedente_idEntidadFederativa;
	}

	public Integer getEstatus_titulo() {
		return estatus_titulo;
	}

	public void setEstatus_titulo(Integer estatus_titulo) {
		this.estatus_titulo = estatus_titulo;
	}

	public Integer getExpedicion_cumplioServicioSocial() {
		return expedicion_cumplioServicioSocial;
	}

	public void setExpedicion_cumplioServicioSocial(Integer expedicion_cumplioServicioSocial) {
		this.expedicion_cumplioServicioSocial = expedicion_cumplioServicioSocial;
	}

	

	public String getAntecedente_institucionProcedencia() {
		return antecedente_institucionProcedencia;
	}

	public void setAntecedente_institucionProcedencia(String antecedente_institucionProcedencia) {
		this.antecedente_institucionProcedencia = antecedente_institucionProcedencia;
	}

	public String getAntecedente_noCedula() {
		return antecedente_noCedula;
	}

	public void setAntecedente_noCedula(String antecedente_noCedula) {
		this.antecedente_noCedula = antecedente_noCedula;
	}

	public Date getExpedicion_fechaExpedicion() {
		return expedicion_fechaExpedicion;
	}

	public void setExpedicion_fechaExpedicion(Date expedicion_fechaExpedicion) {
		this.expedicion_fechaExpedicion = expedicion_fechaExpedicion;
	}

	public Date getExpedicion_fechaExamenProfesional() {
		return expedicion_fechaExamenProfesional;
	}

	public void setExpedicion_fechaExamenProfesional(Date expedicion_fechaExamenProfesional) {
		this.expedicion_fechaExamenProfesional = expedicion_fechaExamenProfesional;
	}

	public Date getExpedicion_fechaExencionExamenProfesional() {
		return expedicion_fechaExencionExamenProfesional;
	}

	public void setExpedicion_fechaExencionExamenProfesional(Date expedicion_fechaExencionExamenProfesional) {
		this.expedicion_fechaExencionExamenProfesional = expedicion_fechaExencionExamenProfesional;
	}

	public Date getAntecedente_fechaInicio() {
		return antecedente_fechaInicio;
	}

	public void setAntecedente_fechaInicio(Date antecedente_fechaInicio) {
		this.antecedente_fechaInicio = antecedente_fechaInicio;
	}

	public Date getAntecedente_fechaTerminacion() {
		return antecedente_fechaTerminacion;
	}

	public void setAntecedente_fechaTerminacion(Date antecedente_fechaTerminacion) {
		this.antecedente_fechaTerminacion = antecedente_fechaTerminacion;
	}

	public Date getCarrera_fechaInicio() {
		return carrera_fechaInicio;
	}

	public void setCarrera_fechaInicio(Date carrera_fechaInicio) {
		this.carrera_fechaInicio = carrera_fechaInicio;
	}

	public Date getCarrera_fechaTermino() {
		return carrera_fechaTermino;
	}

	public void setCarrera_fechaTermino(Date carrera_fechaTermino) {
		this.carrera_fechaTermino = carrera_fechaTermino;
	}

	@Override
	public String toString() {
		return "Titulo [id_titulo=" + id_titulo + ", numeroLote=" + numeroLote + ", id_institucion=" + id_institucion
				+ ", id_carrera=" + id_carrera + ", expedicion_idModalidadTitulacion="
				+ expedicion_idModalidadTitulacion + ", expedicion_idFundamentoLegalServicioSocial="
				+ expedicion_idFundamentoLegalServicioSocial + ", expedicion_idEntidadFederativa="
				+ expedicion_idEntidadFederativa + ", antecedente_idTipoEstudioAntecedente="
				+ antecedente_idTipoEstudioAntecedente + ", antecedente_idEntidadFederativa="
				+ antecedente_idEntidadFederativa + ", estatus_titulo=" + estatus_titulo
				+ ", expedicion_cumplioServicioSocial=" + expedicion_cumplioServicioSocial + ", matricula=" + matricula
				+ ", folioControl=" + folioControl + ", descEstatusEnvio=" + descEstatusEnvio + ", motivoCancelacion="
				+ motivoCancelacion + ", antecedente_institucionProcedencia=" + antecedente_institucionProcedencia
				+ ", antecedente_noCedula=" + antecedente_noCedula + ", expedicion_fechaExpedicion="
				+ expedicion_fechaExpedicion + ", expedicion_fechaExamenProfesional="
				+ expedicion_fechaExamenProfesional + ", expedicion_fechaExencionExamenProfesional="
				+ expedicion_fechaExencionExamenProfesional + ", antecedente_fechaInicio=" + antecedente_fechaInicio
				+ ", antecedente_fechaTerminacion=" + antecedente_fechaTerminacion + ", fecha_registro="
				+ fecha_registro + ", fecha_modificacion=" + fecha_modificacion + ", carrera_fechaTermino="
				+ carrera_fechaTermino + ", carrera_fechaInicio=" + carrera_fechaInicio + ", estudios_fechaTermino="
				+ estudios_fechaTermino + "]";
	}
	

}
