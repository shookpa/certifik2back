package com.ibm.models;



public class TituloFirmantes {

	
	private int id_rel;	
	private int id_titulo;	
	
	private int id_firmante;

	public int getId_rel() {
		return id_rel;
	}

	public void setId_rel(int id_rel) {
		this.id_rel = id_rel;
	}

	public int getId_titulo() {
		return id_titulo;
	}

	public void setId_titulo(int id_titulo) {
		this.id_titulo = id_titulo;
	}

	public int getId_firmante() {
		return id_firmante;
	}

	public void setId_firmante(int id_firmante) {
		this.id_firmante = id_firmante;
	}

	@Override
	public String toString() {
		return "TituloFirmantes [id_rel=" + id_rel + ", id_titulo=" + id_titulo + ", id_firmante=" + id_firmante + "]";
	}

		
}
