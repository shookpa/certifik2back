package com.ibm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;


public class User {

	private String nombre;
	private String a_paterno;
	private String a_materno;
	private String password;
	private String user;
	private String email;
	private Integer id_licencia;
	private Integer rol;
	private Integer status;
	

	
	private Integer id_usuario;

	public Integer getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}
	public Integer getId_licencia() {
		return id_licencia;
	}

	public void setId_licencia(Integer id_licencia) {
		this.id_licencia = id_licencia;
	}
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getA_paterno() {
		return a_paterno;
	}

	public void setA_paterno(String a_paterno) {
		this.a_paterno = a_paterno;
	}

	public String getA_materno() {
		return a_materno;
	}

	public void setA_materno(String a_materno) {
		this.a_materno = a_materno;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getRol() {
		return rol;
	}

	public void setRol(Integer rol) {
		this.rol = rol;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "User [nombre=" + nombre + ", a_paterno=" + a_paterno + ", a_materno=" + a_materno + ", password="
				+ password + ", email=" + email + ", rol=" + rol + ", status=" + status + ", id_usuario=" + id_usuario
				+ "]";
	}

}
