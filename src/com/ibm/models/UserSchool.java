package com.ibm.models;



public class UserSchool {

	
	private int id_rel;	
	private int id_usuario;	
	
	private int id_institucion;

	public int getId_rel() {
		return id_rel;
	}
	public void setId_rel(int id_rel) {
		this.id_rel = id_rel;
	}
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	public int getId_institucion() {
		return id_institucion;
	}
	public void setId_institucion(int id_institucion) {
		this.id_institucion = id_institucion;
	}
	@Override
	public String toString() {
		return "UserSchool [id_rel=" + id_rel + ", id_usuario=" + id_usuario + ", id_institucion=" + id_institucion
				+ "]";
	}
	
}
