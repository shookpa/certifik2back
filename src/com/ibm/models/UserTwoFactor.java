package com.ibm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;


public class UserTwoFactor {

	private Integer id_usuario;	
	private Integer twoFactor;	
	private String secretKey;

	public Integer getId_usuario() {
		return id_usuario;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public Integer getTwoFactor() {
		return twoFactor;
	}

	public void setTwoFactor(Integer twoFactor) {
		this.twoFactor = twoFactor;
	}

	public void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}
	
}
