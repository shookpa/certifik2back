package com.ibm.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;


public class VistaCalificacionesCertificado {
	
	private Integer id_calificacion, id_certificado;
	private Integer id_institucion, id_alumno, id_carrera, id_asignatura, id_asignatura_mec, 
	id_observacion,periodo;	
	Double creditos;
	private String ciclo,clave_asignatura,desc_asignatura,tipo_asignatura, desc_observacion;
	private Double calificacion;
	public Double getCreditos() {
		return creditos;
	}
	public void setCreditos(Double creditos) {
		this.creditos = creditos;
	}
	public String getDesc_observacion() {
		return desc_observacion;
	}
	public void setDesc_observacion(String desc_observacion) {
		this.desc_observacion = desc_observacion;
	}
	public String getClave_asignatura() {
		return clave_asignatura;
	}
	public void setClave_asignatura(String clave_asignatura) {
		this.clave_asignatura = clave_asignatura;
	}
	public String getDesc_asignatura() {
		return desc_asignatura;
	}
	public void setDesc_asignatura(String desc_asignatura) {
		this.desc_asignatura = desc_asignatura;
	}
	public String getTipo_asignatura() {
		return tipo_asignatura;
	}
	public void setTipo_asignatura(String tipo_asignatura) {
		this.tipo_asignatura = tipo_asignatura;
	}
	
	public Integer getId_calificacion() {
		return id_calificacion;
	}
	public void setId_calificacion(Integer id_calificacion) {
		this.id_calificacion = id_calificacion;
	}
	public Integer getId_institucion() {
		return id_institucion;
	}
	public void setId_institucion(Integer id_institucion) {
		this.id_institucion = id_institucion;
	}
	public Integer getId_alumno() {
		return id_alumno;
	}
	public void setId_alumno(Integer id_alumno) {
		this.id_alumno = id_alumno;
	}
	public Integer getId_carrera() {
		return id_carrera;
	}
	public void setId_carrera(Integer id_carrera) {
		this.id_carrera = id_carrera;
	}
	public Integer getId_asignatura() {
		return id_asignatura;
	}
	public void setId_asignatura(Integer id_asignatura) {
		this.id_asignatura = id_asignatura;
	}
	public Integer getId_observacion() {
		return id_observacion;
	}
	public void setId_observacion(Integer id_observacion) {
		this.id_observacion = id_observacion;
	}
	public Integer getPeriodo() {
		return periodo;
	}
	public void setPeriodo(Integer periodo) {
		this.periodo = periodo;
	}
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	public Double getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(Double calificacion) {
		this.calificacion = calificacion;
	}
	
	public Integer getId_certificado() {
		return id_certificado;
	}
	public void setId_certificado(Integer id_certificado) {
		this.id_certificado = id_certificado;
	}
	@Override
	public String toString() {
		return "VistaCalificacionesCertificado [id_calificacion=" + id_calificacion + ", id_certificado="
				+ id_certificado + ", id_institucion=" + id_institucion + ", id_alumno=" + id_alumno + ", id_carrera="
				+ id_carrera + ", id_asignatura=" + id_asignatura + ", id_asignatura_mec=" + id_asignatura_mec
				+ ", creditos=" + creditos + ", id_observacion=" + id_observacion + ", periodo=" + periodo + ", ciclo="
				+ ciclo + ", clave_asignatura=" + clave_asignatura + ", desc_asignatura=" + desc_asignatura
				+ ", tipo_asignatura=" + tipo_asignatura + ", desc_observacion=" + desc_observacion + ", calificacion="
				+ calificacion + "]";
	}
	public Integer getId_asignatura_mec() {
		return id_asignatura_mec;
	}
	public void setId_asignatura_mec(Integer id_asignatura_mec) {
		this.id_asignatura_mec = id_asignatura_mec;
	}
		
}
