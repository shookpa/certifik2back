package com.ibm.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;


public class VistaCertificado {
	private Integer id_certificado;
	private Integer id_institucion, id_firmante, id_carrera, id_alumno, estatus_certificado,
	total_materias,total_asignadas,total_aprobadas, numero_ciclos;
	private Double promedio, calif_minima, calif_maxima, calif_aprob, total_creditos,creditos_obtenidos;
	private String matricula,nombre,primerApellido,segundoApellido,curp, cve_carrera,nombre_carrera,nivel_educativo,modalidad, rvoe_dgp;

	private String tipo_certif,num_certif, tipo_periodo, nombreFirmante,paternoFirmante,maternoFirmante, folioControl;
	
	private String fecha_expedicion, fecha_expedicion_rvoe,
	fecha_registro, fecha_modificacion;
	public Double getCalif_minima() {
		return calif_minima;
	}
	public void setCalif_minima(Double calif_minima) {
		this.calif_minima = calif_minima;
	}
	public Double getCalif_maxima() {
		return calif_maxima;
	}
	public void setCalif_maxima(Double calif_maxima) {
		this.calif_maxima = calif_maxima;
	}
	public Double getCalif_aprob() {
		return calif_aprob;
	}
	public void setCalif_aprob(Double calif_aprob) {
		this.calif_aprob = calif_aprob;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPrimerApellido() {
		return primerApellido;
	}
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	public String getSegundoApellido() {
		return segundoApellido;
	}
	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}
	public String getCurp() {
		return curp;
	}
	public void setCurp(String curp) {
		this.curp = curp;
	}
	public String getCve_carrera() {
		return cve_carrera;
	}
	public void setCve_carrera(String cve_carrera) {
		this.cve_carrera = cve_carrera;
	}
	public String getNombre_carrera() {
		return nombre_carrera;
	}
	public void setNombre_carrera(String nombre_carrera) {
		this.nombre_carrera = nombre_carrera;
	}
	public String getNivel_educativo() {
		return nivel_educativo;
	}
	public void setNivel_educativo(String nivel_educativo) {
		this.nivel_educativo = nivel_educativo;
	}
	public String getModalidad() {
		return modalidad;
	}
	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}
	public String getRvoe_dgp() {
		return rvoe_dgp;
	}
	public void setRvoe_dgp(String rvoe_dgp) {
		this.rvoe_dgp = rvoe_dgp;
	}
	public String getTipo_periodo() {
		return tipo_periodo;
	}
	public void setTipo_periodo(String tipo_periodo) {
		this.tipo_periodo = tipo_periodo;
	}
	public String getNombreFirmante() {
		return nombreFirmante;
	}
	public void setNombreFirmante(String nombreFirmante) {
		this.nombreFirmante = nombreFirmante;
	}
	public String getPaternoFirmante() {
		return paternoFirmante;
	}
	public void setPaternoFirmante(String paternoFirmante) {
		this.paternoFirmante = paternoFirmante;
	}
	public String getMaternoFirmante() {
		return maternoFirmante;
	}
	public void setMaternoFirmante(String maternoFirmante) {
		this.maternoFirmante = maternoFirmante;
	}
	public String getFecha_expedicion_rvoe() {
		return fecha_expedicion_rvoe;
	}
	public void setFecha_expedicion_rvoe(String fecha_expedicion_rvoe) {
		this.fecha_expedicion_rvoe = fecha_expedicion_rvoe;
	}
	
	public Integer getId_certificado() {
		return id_certificado;
	}
	public void setId_certificado(Integer id_certificado) {
		this.id_certificado = id_certificado;
	}
	public Integer getId_institucion() {
		return id_institucion;
	}
	public void setId_institucion(Integer id_institucion) {
		this.id_institucion = id_institucion;
	}
	public Integer getId_firmante() {
		return id_firmante;
	}
	public void setId_firmante(Integer id_firmante) {
		this.id_firmante = id_firmante;
	}
	public Integer getId_carrera() {
		return id_carrera;
	}
	public void setId_carrera(Integer id_carrera) {
		this.id_carrera = id_carrera;
	}
	public Integer getId_alumno() {
		return id_alumno;
	}
	public void setId_alumno(Integer id_alumno) {
		this.id_alumno = id_alumno;
	}
	public Integer getEstatus_certificado() {
		return estatus_certificado;
	}
	public void setEstatus_certificado(Integer estatus_certificado) {
		this.estatus_certificado = estatus_certificado;
	}
	public Integer getTotal_materias() {
		return total_materias;
	}
	public void setTotal_materias(Integer total_materias) {
		this.total_materias = total_materias;
	}
	public Integer getTotal_asignadas() {
		return total_asignadas;
	}
	public void setTotal_asignadas(Integer total_asignadas) {
		this.total_asignadas = total_asignadas;
	}
	public Integer getTotal_aprobadas() {
		return total_aprobadas;
	}
	public void setTotal_aprobadas(Integer total_aprobadas) {
		this.total_aprobadas = total_aprobadas;
	}
	public Double getTotal_creditos() {
		return total_creditos;
	}
	public void setTotal_creditos(Double total_creditos) {
		this.total_creditos = total_creditos;
	}
	public Double getCreditos_obtenidos() {
		return creditos_obtenidos;
	}
	public void setCreditos_obtenidos(Double creditos_obtenidos) {
		this.creditos_obtenidos = creditos_obtenidos;
	}
	public Double getPromedio() {
		return promedio;
	}
	public void setPromedio(Double promedio) {
		this.promedio = promedio;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getTipo_certif() {
		return tipo_certif;
	}
	public void setTipo_certif(String tipo_certif) {
		this.tipo_certif = tipo_certif;
	}
	public String getNum_certif() {
		return num_certif;
	}
	public void setNum_certif(String num_certif) {
		this.num_certif = num_certif;
	}
	public String getFecha_expedicion() {
		return fecha_expedicion;
	}
	public void setFecha_expedicion(String fecha_expedicion) {
		this.fecha_expedicion = fecha_expedicion;
	}
	public String getFecha_registro() {
		return fecha_registro;
	}
	public void setFecha_registro(String fecha_registro) {
		this.fecha_registro = fecha_registro;
	}
	public String getFecha_modificacion() {
		return fecha_modificacion;
	}
	public void setFecha_modificacion(String fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}
	public String getFolioControl() {
		return folioControl;
	}
	public void setFolioControl(String folioControl) {
		this.folioControl = folioControl;
	}
	public Integer getNumero_ciclos() {
		return numero_ciclos;
	}
	public void setNumero_ciclos(Integer numero_ciclos) {
		this.numero_ciclos = numero_ciclos;
	}
	@Override
	public String toString() {
		return "VistaCertificado [id_certificado=" + id_certificado + ", id_institucion=" + id_institucion
				+ ", id_firmante=" + id_firmante + ", id_carrera=" + id_carrera + ", id_alumno=" + id_alumno
				+ ", estatus_certificado=" + estatus_certificado + ", total_materias=" + total_materias
				+ ", total_asignadas=" + total_asignadas + ", total_aprobadas=" + total_aprobadas + ", total_creditos="
				+ total_creditos + ", creditos_obtenidos=" + creditos_obtenidos + ", promedio=" + promedio
				+ ", calif_minima=" + calif_minima + ", calif_maxima=" + calif_maxima + ", calif_aprob=" + calif_aprob
				+ ", matricula=" + matricula + ", nombre=" + nombre + ", primerApellido=" + primerApellido
				+ ", segundoApellido=" + segundoApellido + ", curp=" + curp + ", cve_carrera=" + cve_carrera
				+ ", nombre_carrera=" + nombre_carrera + ", nivel_educativo=" + nivel_educativo + ", modalidad="
				+ modalidad + ", rvoe_dgp=" + rvoe_dgp + ", tipo_certif=" + tipo_certif + ", num_certif=" + num_certif
				+ ", tipo_periodo=" + tipo_periodo + ", nombreFirmante=" + nombreFirmante + ", paternoFirmante="
				+ paternoFirmante + ", maternoFirmante=" + maternoFirmante + ", folioControl=" + folioControl
				+ ", fecha_expedicion=" + fecha_expedicion + ", fecha_expedicion_rvoe=" + fecha_expedicion_rvoe
				+ ", fecha_registro=" + fecha_registro + ", fecha_modificacion=" + fecha_modificacion + "]";
	}
	

	

}
