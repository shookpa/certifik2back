package com.ibm.models;

public class VistaFirmante {

	private Integer id_responsable, id_institucion;

	private String nombre, primerApellido, segundoApellido;

	public Integer getId_responsable() {
		return id_responsable;
	}

	public void setId_responsable(Integer id_responsable) {
		this.id_responsable = id_responsable;
	}

	public Integer getId_institucion() {
		return id_institucion;
	}

	public void setId_institucion(Integer id_institucion) {
		this.id_institucion = id_institucion;
	}

	public String getNombre() {
		return nombre;
	}

	@Override
	public String toString() {
		return "VistaFirmante [id_responsable=" + id_responsable + ", id_institucion=" + id_institucion + ", nombre="
				+ nombre + ", primerApellido=" + primerApellido + ", segundoApellido=" + segundoApellido + "]";
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

}
