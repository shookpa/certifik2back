package com.ibm.services;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

public interface CatalogService {
	public Response getKeyValueItems(@HeaderParam("token") String token, String json);
	public Response searchStudent(@HeaderParam("token") String token, String json);
	public Response getStudentData(@HeaderParam("token") String token, String json);
	public Response setScores(@HeaderParam("token") String token, String json);
	public Response setScore(@HeaderParam("token") String token, String json);
	public Response setStudents(@HeaderParam("token") String token, String json);
	public Response setStudent(@HeaderParam("token") String token, String json);
	public Response setCareers(@HeaderParam("token") String token, String json);
	public Response setCareer(@HeaderParam("token") String token, String json);
	public Response updateCareer(@HeaderParam("token") String token,int id,  String json);
	public Response updateSubject(@HeaderParam("token") String token,@PathParam("id_career") int id_career, @PathParam("id_subject") int id_subject,  String json);
	public Response updateStudent(@HeaderParam("token") String token,int id,  String json);
	public Response updateScore(@HeaderParam("token") String token,int id,  String json);
	public Response setSubject(@HeaderParam("token") String token, int id, String json);
	public Response getSubjects(@HeaderParam("token") String token, int id);
	public Response getScores(@HeaderParam("token") String token, String id);
	public Response getStudents(@HeaderParam("token") String token, int id);
	public Response getStudentList(@HeaderParam("token") String token, String json);
	public Response getCareerList(@HeaderParam("token") String token, String json);
	public Response deleteScore(@HeaderParam("token") String token, @PathParam("id") int id);
	public Response deactivateCareer(@HeaderParam("token") String token, @PathParam("id") int id);
	public Response deactivateStudent(@HeaderParam("token") String token, @PathParam("id") int id);
	public Response deactivateSubject(@HeaderParam("token") String token, @PathParam("id_career") int id_career, @PathParam("id_subject") int id_subject);
	public Response getSchools(@HeaderParam("token") String token);
	public Response addSchool(@HeaderParam("token") String token, String json);
	public Response updateSchool(@HeaderParam("token") String token,int id,  String json);
	public Response deactivateSchool(@HeaderParam("token") String token, @PathParam("id") int id);
	//PARA OBTENER EL COMBO DE FIRMANTES
	public Response getFirmantes(@HeaderParam("token") String token);
	//PARA EL CATALOGO DE FIRMANTES LAS SIGUIENTES 4 OPERACIONES:
	public Response setSigner(@HeaderParam("token") String token, String json);
	public Response getSignerList(@HeaderParam("token") String token, String json);
	public Response deactivateSigner(@HeaderParam("token") String token, @PathParam("id") int id);	
	public Response updateSigner(@HeaderParam("token") String token, @PathParam("id") int id, String json);
	public Response getDatosConfiguracionSEP(@HeaderParam("token") String token);
	public Response updateConfig(@HeaderParam("token") String token, int id, String json);
	
	
}
