package com.ibm.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.ibm.dao.CatalogsDao;
import com.ibm.dao.RolOperationDao;
import com.ibm.dao.TituloDao;
import com.ibm.models.Asignatura;
import com.ibm.models.AutorizacionReconocimientoShort;
import com.ibm.models.Calificacion;
import com.ibm.models.CalificacionInfo;
import com.ibm.models.Carrera;
import com.ibm.models.CarreraShort;
import com.ibm.models.CarreraView;
import com.ibm.models.Configurations;
import com.ibm.models.Firmante;
import com.ibm.models.GenericResponse;
import com.ibm.models.School;
import com.ibm.models.Student;
import com.ibm.models.StudentCareer;
import com.ibm.models.StudentInfoCalif;
import com.ibm.models.VistaFirmante;
import com.ibm.util.JWTUtil;
import com.ibm.util.StringUtils;

@Path("/rest/catalogs")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CatalogServiceImpl implements CatalogService {
	private static final Logger logger = Logger.getLogger(CatalogServiceImpl.class);
	@Override
	@POST
	@Path("/getKeyValueItems")
	public Response getKeyValueItems(@HeaderParam("token") String token, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Faltan criterios de busqueda");
			response.setErrorCode("Codigo pendiente");
			return Response.status(404).entity(response).build();
		}
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;
		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			String cat = jo.get("catalogo").toString();
			cat = cat.substring(0, 1).toUpperCase() + cat.substring(1);
			response.setData(dao.getCatalogItems(cat + "Short"));
			response.setMessage("Elementos del catalogo: " + cat);
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@POST
	@Path("/students/search")
	public Response searchStudent(@HeaderParam("token") String token, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		//PENDIENTE VALIDACION DEL LENGTH DE LA MATRICULA ENVIADA POR EL USUARIO:
		
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Faltan criterios de busqueda");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;
		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			
			String mat = jo.get("matricula").toString();
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("matricula", mat);
			filtro.put("id_institucion", idInst);

			Student stu = dao.searchStudent(filtro);
			//en general la forma de la busqueda se puede mejorar drasticamente haciendo la búsqueda directamente en la vista:
			
			StudentCareer stuCar = new StudentCareer();
			stuCar.setCorreoElectronico(stu.getCorreoElectronico());
			stuCar.setId_alumno(stu.getId_alumno());
			stuCar.setCurp(stu.getCurp());
			stuCar.setEstatus(stu.getEstatus());
			stuCar.setF_fin_carrera(stu.getF_fin_carrera());
			stuCar.setF_inicio_carrera(stu.getF_inicio_carrera());
			stuCar.setGeneracion(stu.getGeneracion());
			
			stuCar.setId_institucion(stu.getId_institucion());
			stuCar.setMatricula(stu.getMatricula());
			stuCar.setNombre(stu.getNombre());
			int id_carrera_estudiante = stu.getId_carrera();
			stuCar.setNombre_carrera(dao.getCarrera(id_carrera_estudiante).getNombre_carrera());
			stuCar.setPrimerApellido(stu.getPrimerApellido());
			stuCar.setSegundoApellido(stu.getSegundoApellido());
			stuCar.setSexo(stu.getSexo());
			
			if (stu == null) {
				response.setStatus(false);
				response.setMessage("El estudiante no existe");
				response.setErrorCode("");
				return Response.status(404).entity(response).build();
			}
			if (stu.getId_institucion() != idInst) {
				response.setStatus(false);
				response.setMessage("Esta matrícula no corresponde a esta institución");
				response.setErrorCode("");
				return Response.status(404).entity(response).build();
			}
			response.setData(stuCar);
			response.setMessage("Estudiante localizado");
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@POST
	@Path("/students/getInfo")
	public Response getStudentData(@HeaderParam("token") String token, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Faltan criterios de busqueda");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}

		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			String mat = jo.get("matricula").toString();
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("matricula", mat);
			filtro.put("id_institucion", idInst);
			StudentInfoCalif stuInfo = dao.searchStudentInfo(filtro);
			if (stuInfo == null) {
				response.setStatus(false);
				response.setMessage("El estudiante no tiene calificaciones registradas");
				response.setErrorCode("");
				return Response.status(404).entity(response).build();
			}
			if (stuInfo.getId_institucion() != idInst) {
				response.setStatus(false);
				response.setMessage("Esta matrícula no corresponde a esta institución");
				response.setErrorCode("");
				return Response.status(404).entity(response).build();
			}
			// FALTA PONER EL TOTAL DE CREDITOS QUE DEBEN DE SER DE ESA CARRERA

			response.setData(stuInfo);
			response.setMessage("Estudiante localizado");
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
	
	//8146241551
	
	@Override
	@POST
	@Path("/students/scores")
	public Response setScores(@HeaderParam("token") String token, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 5);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;
		int numBuenos = 0;
		int numMalos = 0;
		boolean valido=true;
		Map<Integer, String> respuesta = new HashMap<Integer, String>();
		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();

			String base64 = jo.get("archivo").toString();
			String nomArchivo = jo.get("nombre_archivo").toString();
			File excelFile = new File("webapps/tmp/" + nomArchivo);

			byte[] bytes = Base64.decodeBase64(base64);
			FileUtils.writeByteArrayToFile(excelFile, bytes);
			FileInputStream fis;

			fis = new FileInputStream(excelFile);
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			// we get first sheet
			XSSFSheet sheet = workbook.getSheetAt(0);
			String matricula;
			
			
			try {
				matricula =	sheet.getRow(4).getCell(1).getStringCellValue().trim();
			} catch (Exception e) {				
				matricula = String.valueOf(Math.round(sheet.getRow(4).getCell(1).getNumericCellValue())).trim();
			}
			if (matricula.equalsIgnoreCase("")) {
				respuesta.put((5), "Falta: matricula" );
				numMalos++;
				valido = false;
			}
			
			String nom_alumno = sheet.getRow(5).getCell(1).getStringCellValue().trim();
			if (nom_alumno.equalsIgnoreCase("")) {				
				
				respuesta.put((6), "Falta: nombre de alumno" );
				numMalos++;
				valido = false;
				
			}
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("matricula", matricula);
			filtro.put("id_institucion", idInst);
			System.out.println("matricula"+ matricula);
			List<Object> resultados = dao.searchCatalog(new Student(), filtro);

			if (resultados.size() == 0) {
				
				respuesta.put((5), "Incorrecta: matrícula "+ matricula  );
				numMalos++;
				valido = false;
			}
			Student stu = ((Student) resultados.get(0));

			
			
			String cve_carrera ;
			try
			{
				cve_carrera = sheet.getRow(6).getCell(1).getStringCellValue().trim();
			}
			catch (Exception e)
			{
				cve_carrera = String.valueOf(Math.round(sheet.getRow(6).getCell(1).getNumericCellValue())).trim();
			}
			
			if (cve_carrera.equalsIgnoreCase("")) {
				respuesta.put((7), "Falta: Clave de la carrera" );
				numMalos++;
				valido = false;
			}
			
			
//			filtro = new HashMap<String, Object>();
//			filtro.put("cve_carrera", cve_carrera);//estan ingresando el plan, pero es un dato innecesario.
//			filtro.put("id_institucion", idInst);
//
//			resultados = dao.searchCatalog(new Carrera(), filtro);
//			if (resultados.size() == 0) {				
//				respuesta.put((7), "Incorrecta: Clave Carrera"+ cve_carrera );
//				numMalos++;
//				valido = false;
//			}
						
			String nom_carrera = sheet.getRow(7).getCell(1).getStringCellValue().trim();
			if (nom_carrera.equalsIgnoreCase("")) {				
				respuesta.put((8), "Falta: Nombre de Carrera");
				numMalos++;
				valido = false;
			}
			// FALTA LA VALIDACION QUE EL ID DE CARRERA CORRESPONDA AL NOMBRE DE
			// CARRERA
			// FALTA LA VALIDACION QUE LA CARRERA CORRESPONDA A LA INSTITUCION

			Calificacion cal = new Calificacion();
			cal.setId_carrera(stu.getId_carrera());//aqui
			cal.setId_alumno(stu.getId_alumno());
			// A PARTIR DE LA LINEA 11 COMIENZAN LAS CALIFICACIONES DE CADA
			// ASIGNATURA:
			for (int i = 9; i < sheet.getLastRowNum(); i++) {

				filtro = new HashMap<String, Object>();
				String clave_asignatura;
				if (sheet.getRow(i).getCell(0) == null) {
					break;
				}
				
				
				try
				{
					clave_asignatura = sheet.getRow(i).getCell(0).getStringCellValue().trim();
				}
				catch (Exception e)
				{
					clave_asignatura = String.valueOf(Math.round(sheet.getRow(i).getCell(0).getNumericCellValue())).trim();
				}
				
				if (!clave_asignatura.trim().equalsIgnoreCase("")) {
					filtro.put("clave_asignatura", clave_asignatura);
					filtro.put("id_institucion", idInst);
					filtro.put("id_carrera", stu.getId_carrera());
					resultados = dao.searchCatalog(new Asignatura(), filtro);
					System.out.println("Veamos la clave_asignatura:"+ clave_asignatura);
					if (resultados.size()>0) {
						cal.setId_asignatura(((Asignatura) resultados.get(0)).getId_asignatura()); 
						if (sheet.getRow(i).getCell(1) == null) {
							respuesta.put((i+1), "Falta: periodo");
							numMalos++;
							valido = false;
						}
						if (sheet.getRow(i).getCell(1).getStringCellValue().trim().equalsIgnoreCase("")) {
							respuesta.put((i+1), "Falta: periodo");
							numMalos++;
							valido = false;
						} 
						if (sheet.getRow(i).getCell(2) == null) {
							respuesta.put((i+1), "Falta: ciclo");
							numMalos++;
							valido = false;
						}
						if (sheet.getRow(i).getCell(2).getStringCellValue().trim().equalsIgnoreCase("")) {
							respuesta.put((i+1), "Falta: ciclo");
							numMalos++;
							valido = false;
						} 
						if (sheet.getRow(i).getCell(3) == null) {
							respuesta.put((i+1), "Falta: calificación");
							numMalos++;
							valido = false;
						}
						if (sheet.getRow(i).getCell(3).getNumericCellValue()<0) {
							respuesta.put((i+1), "Falta: calificación");
							numMalos++;
							valido = false;
						} 
						if (sheet.getRow(i).getCell(4) == null) {
							respuesta.put((i+1), "Falta: id de observación");
							numMalos++;
							valido = false;
						}
						if (sheet.getRow(i).getCell(4).getNumericCellValue()<0) {
							respuesta.put((i+1), "Falta: id de observación");
							numMalos++;
							valido = false;
						} 
						if(valido)
						{	
							cal.setPeriodo(Integer.valueOf(sheet.getRow(i).getCell(1).getStringCellValue()));
	
							cal.setCiclo(sheet.getRow(i).getCell(2).getStringCellValue().trim());
	
							cal.setCalificacion(sheet.getRow(i).getCell(3).getNumericCellValue());
							cal.setId_observacion(sheet.getRow(i).getCell(4).getNumericCellValue() > 0
									? Integer.parseInt(sheet.getRow(i).getCell(4).getRawValue()) : 100);
	
							cal.setId_institucion(idInst);
							//cal.setId_carrera(Integer.parseInt(id_carrera));
							cal.setId_calificacion(null);
							System.out.println("Veamos las calificaciones calificacion:" + cal);
							// Antes de insertar debemos verificar que no haya
							// duplicados:
							filtro = new HashMap<String, Object>();
							filtro.put("id_asignatura", cal.getId_asignatura());
							filtro.put("id_alumno", cal.getId_alumno());
							filtro.put("id_carrera", cal.getId_carrera());
							filtro.put("id_institucion", cal.getId_institucion());
							if (dao.searchCatalog(new Calificacion(), filtro).size() == 0)
							{	
								dao.setCalificacion(cal);
								numBuenos++;
							}
							else
							{
								respuesta.put((i+1), "Duplicado: calificación ya registrada");
								numMalos++;
								valido = false;
							}
							
						}
					}
					
				}

			}

			workbook.close();
			fis.close();

			
			response.setData(respuesta);
			response.setMessage("Se registraron exitosamente " + numBuenos + " calificaciones, registros erroneos: " + numMalos);
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@POST
	@Path("/students/score")
	public Response setScore(@HeaderParam("token") String token, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 5);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();

			String matricula = jo.get("matricula").toString();
			String id_carrera = jo.get("id_carrera").toString();
			String clave_asignatura = jo.get("clave_asignatura").toString();
			Integer periodo = Integer.parseInt(jo.get("periodo").toString());
			String ciclo = jo.get("ciclo").toString();
			Double calificacion = Double.parseDouble(jo.get("calificacion").toString());
			Integer id_observacion = Integer.parseInt(jo.get("id_observacion").toString());
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("matricula", matricula);
			filtro.put("id_institucion", idInst);
			List resultados = dao.searchCatalog(new Student(), filtro);

			if (resultados.size() == 0) {
				response.setStatus(false);
				response.setMessage("El estudiante no existe");
				response.setErrorCode("");
				response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));
				return Response.status(404).entity(response).build();
			}
			Student stu = ((Student) resultados.get(0));

			if (stu.getId_institucion() != idInst) {
				response.setStatus(false);
				response.setMessage("Esta matrícula no corresponde a esta institución");
				response.setErrorCode("");
				response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));
				return Response.status(404).entity(response).build();
			}
			// FALTA LA VALIDACION QUE LA CARRERA CORRESPONDA A LA INSTITUCION

			Calificacion cal = new Calificacion();
			cal.setId_alumno(stu.getId_alumno());
			filtro = new HashMap<String, Object>();

			if (!clave_asignatura.trim().equalsIgnoreCase("")) {
				filtro.put("clave_asignatura", clave_asignatura);
				filtro.put("id_institucion", idInst);
				resultados = dao.searchCatalog(new Asignatura(), filtro);
				if (resultados.size() == 0) {
					response.setStatus(false);
					response.setMessage("No existe la clave de asignatura: " + clave_asignatura);
					response.setErrorCode("");
					response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));
					return Response.status(404).entity(response).build();
				}

				cal.setId_asignatura(((Asignatura) resultados.get(0)).getId_asignatura());
				cal.setPeriodo(periodo);
				cal.setCiclo(ciclo);
				cal.setCalificacion(calificacion);
				cal.setId_observacion(id_observacion);

				cal.setId_institucion(1);
				cal.setId_carrera(Integer.parseInt(id_carrera));
				cal.setId_calificacion(null);

				// Antes de insertar debemos verificar que no haya
				// duplicados:
				filtro = new HashMap<String, Object>();
				filtro.put("id_asignatura", cal.getId_asignatura());
				filtro.put("id_alumno", cal.getId_alumno());
				filtro.put("id_carrera", cal.getId_carrera());
				filtro.put("id_institucion", cal.getId_institucion());
				if (dao.searchCatalog(new Calificacion(), filtro).size() == 0)
					dao.setCalificacion(cal);
				else {
					response.setStatus(false);
					response.setMessage("La calificacion de la asignatura: " + clave_asignatura
							+ " ya existia en las calificaciones de la matrícula: " + matricula + cal);
					response.setErrorCode("");
					response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));
					return Response.status(404).entity(response).build();
				}
			}
			response.setData(null);
			response.setMessage("Calificacion registrada");
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@PUT
	@Path("/students/score/{id}")
	public Response updateScore(@HeaderParam("token") String token, @PathParam("id") int id, String json) {
		GenericResponse response = new GenericResponse();

		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 5);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func2", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();

			Integer periodo = Integer.parseInt(jo.get("periodo").toString());
			String ciclo = jo.get("ciclo").toString();
			Double calificacion = Double.parseDouble(jo.get("calificacion").toString());
			Integer id_observacion = Integer.parseInt(jo.get("id_observacion").toString());
			Map<String, Object> filtro = new HashMap<String, Object>();

			// FALTA LA VALIDACION QUE LA CARRERA CORRESPONDA A LA INSTITUCION

			Calificacion cal = new Calificacion();

			filtro = new HashMap<String, Object>();

			filtro.put("id_calificacion", id);			
			
			List resultados = dao.searchCatalog(new Calificacion(), filtro);
			if (resultados.size() == 0) {
				response.setStatus(false);
				response.setMessage("No existe el id de calificacion: " + id);
				response.setErrorCode("");
				response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));
				return Response.status(404).entity(response).build();
			}

			cal.setId_asignatura(((Calificacion) resultados.get(0)).getId_asignatura());
			cal.setPeriodo(periodo);
			cal.setCiclo(ciclo);
			cal.setCalificacion(calificacion);
			cal.setId_observacion(id_observacion);

			cal.setId_institucion(1);
			cal.setId_carrera(((Calificacion) resultados.get(0)).getId_carrera());
			cal.setId_alumno(((Calificacion) resultados.get(0)).getId_alumno());
			cal.setId_calificacion(id);

			dao.updateRecord(cal);

			response.setData(null);
			response.setMessage("Calificacion actualizada");
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@DELETE
	@Path("/students/score/{id}")
	public Response deleteScore(@HeaderParam("token") String token, @PathParam("id") int id) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 5);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func3", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();

			filtro.put("id_calificacion", id);
			List resultados = dao.searchCatalog(new Calificacion(), filtro);
			if (resultados.size() == 0) {
				response.setStatus(false);
				response.setMessage("No existe la calificacion con el id: " + id);
				response.setErrorCode("");
				response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));
				return Response.status(404).entity(response).build();
			} else {
				Calificacion cal = (Calificacion) resultados.get(0);
				dao.deleteRecord(cal);
			}

			response.setData(null);
			response.setMessage("Calificacion eliminada");
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@DELETE
	@Path("/career/{id_career}/subject/{id_subject}")
	public Response deactivateSubject(@HeaderParam("token") String token, @PathParam("id_career") int id_career,
			@PathParam("id_subject") int id_subject) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 10);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func3", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();

			filtro.put("id_carrera", id_career);
			filtro.put("id_asignatura", id_subject);
			filtro.put("id_institucion", idInst);
			List resultados = dao.searchCatalog(new Asignatura(), filtro);
			if (resultados.size() == 0) {
				response.setStatus(false);
				response.setMessage("No existe la asignatura con el id: " + id_subject + ", con la carrera con el id: "
						+ id_career);
				response.setErrorCode("");
				response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));
				return Response.status(404).entity(response).build();
			} else {
				Asignatura cal = (Asignatura) resultados.get(0);
				cal.setEstatus(0);
				dao.updateRecord(cal);
			}

			response.setData(null);
			response.setMessage("Asignatura eliminada");
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@DELETE
	@Path("/student/{id}")
	public Response deactivateStudent(@HeaderParam("token") String token, @PathParam("id") int id) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 3);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func3", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();

			filtro.put("id_alumno", id);
			filtro.put("id_institucion", idInst);
			List resultados = dao.searchCatalog(new Student(), filtro);
			if (resultados.size() == 0) {
				response.setStatus(false);
				response.setMessage("No existe el estudiante con el id: " + id);
				response.setErrorCode("");
				response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));
				return Response.status(404).entity(response).build();
			} else {
				Student cal = (Student) resultados.get(0);
				cal.setEstatus(0);
				dao.updateRecord(cal);
			}

			response.setData(null);
			response.setMessage("Estudiante eliminado");
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
	@Override
	@DELETE
	@Path("/signer/{id}")
	public Response deactivateSigner(@HeaderParam("token") String token, @PathParam("id") int id) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 4);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func3", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();

			filtro.put("id_responsable", id);
			filtro.put("id_institucion", idInst);
			List resultados = dao.searchCatalog(new Firmante(), filtro);
			if (resultados.size() == 0) {
				response.setStatus(false);
				response.setMessage("No existe el firmante con el id: " + id);
				response.setErrorCode("");
				response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));
				return Response.status(404).entity(response).build();
			} else {
				Firmante cal = (Firmante) resultados.get(0);
				cal.setStatus(0);
				dao.updateRecord(cal);
			}

			response.setData(null);
			response.setMessage("Firmante eliminado");
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@DELETE
	@Path("/career/{id}")
	public Response deactivateCareer(@HeaderParam("token") String token, @PathParam("id") int id) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 6);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func3", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();

			filtro.put("id_carrera", id);
			filtro.put("id_institucion", idInst);
			List resultados = dao.searchCatalog(new Carrera(), filtro);
			if (resultados.size() == 0) {
				response.setStatus(false);
				response.setMessage("No existe la carrera con el id: " + id);
				response.setErrorCode("");
				response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));
				return Response.status(404).entity(response).build();
			} else {
				Carrera cal = (Carrera) resultados.get(0);
				cal.setEstatus(0);
				dao.updateRecord(cal);
			}

			response.setData(null);
			response.setMessage("Carrera eliminada");
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@POST
	@Path("/students")
	public Response setStudents(@HeaderParam("token") String token, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
//			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 3);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			Map<Integer, String> respuesta = new HashMap<Integer, String>();
			String base64 = jo.get("archivo").toString();
			String nomArchivo = jo.get("nombre_archivo").toString();
			File excelFile = new File("webapps/tmp/" + nomArchivo);

			byte[] bytes = Base64.decodeBase64(base64);
			FileUtils.writeByteArrayToFile(excelFile, bytes);
			FileInputStream fis;

			fis = new FileInputStream(excelFile);
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			// we get first sheet
			XSSFSheet sheet = workbook.getSheetAt(0);

			Student stu = new Student();

			int num = 0;
			int numMalos = 0;
			boolean valido=true;
			for (int i = 5; i < sheet.getLastRowNum(); i++) {
				valido=true;
				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro = new HashMap<String, Object>();
				String matricula = "";
				if (sheet.getRow(i).getCell(0) == null) {
					break;
				}
				try
				{
					matricula = sheet.getRow(i).getCell(0).getStringCellValue().trim();
				}
				catch (Exception e)
				{
					matricula = String.valueOf(Math.round(sheet.getRow(i).getCell(0).getNumericCellValue())).trim();
				}
				
				if (!matricula.trim().equalsIgnoreCase("")) {
					filtro.put("matricula", matricula);
					filtro.put("id_institucion", idInst);
					List resultados = dao.searchCatalog(new Student(), filtro);
					if (resultados.size() == 0) {
						stu.setMatricula(matricula);
						if (sheet.getRow(i).getCell(1) == null) {
							respuesta.put((i+1), "Falta: nombre" );
							numMalos++;
							valido = false;
						} else
						if (sheet.getRow(i).getCell(1).getStringCellValue().trim().equalsIgnoreCase("")) {							
							respuesta.put((i+1), "Falta: nombre" );
							numMalos++;
							valido = false;
						} 
						if (sheet.getRow(i).getCell(2) == null) {
							respuesta.put((i+1), "Falta: apellido paterno" );
							numMalos++;
							valido = false;
						}
						else
						if (sheet.getRow(i).getCell(2).getStringCellValue().trim().equalsIgnoreCase("")) {
							respuesta.put((i+1), "Falta: apellido paterno" );
							numMalos++;
							valido = false;
						} 
						String apmaterno;
						if (sheet.getRow(i).getCell(3) == null) {
							apmaterno="";
						}
						else
							apmaterno= sheet.getRow(i).getCell(3).getStringCellValue().trim();
						if (sheet.getRow(i).getCell(4) == null) {
							respuesta.put((i+1), "Falta: CURP" );
							numMalos++;
							valido = false;
						}
						else
						if (sheet.getRow(i).getCell(4).getStringCellValue().trim().equalsIgnoreCase("")) {							
							respuesta.put((i+1), "Falta: CURP" );
							numMalos++;
							valido = false;
						} 
						if (sheet.getRow(i).getCell(5) == null) {
							valido = false;
						}
						if (sheet.getRow(i).getCell(5).getStringCellValue().trim().equalsIgnoreCase("")) {
							respuesta.put((i+1), "Falta: campo sexo" );
							numMalos++;
							valido = false;
						} 
						if (sheet.getRow(i).getCell(6) == null) {
							valido = false;
						}
						if (sheet.getRow(i).getCell(6).getStringCellValue().trim().equalsIgnoreCase("")) {							
							respuesta.put((i+1), "Falta: generacion" );
							numMalos++;
							valido = false;
						} 
						if (sheet.getRow(i).getCell(7) == null) {
							
							respuesta.put((i+1), "Falta: Clave carrera" );
							numMalos++;
							valido = false;
						}
						if (sheet.getRow(i).getCell(8) == null) {
							
							respuesta.put((i+1), "Falta: Id carrera del MEC" );
							numMalos++;
							valido = false;
						}
						if (sheet.getRow(i).getCell(9) == null) {
							
							respuesta.put((i+1), "Falta: fecha inicio de carrera" );
							numMalos++;
							valido = false;
						}
						if (sheet.getRow(i).getCell(10) == null) {
							respuesta.put((i+1), "Falta: fecha fin de carrera" );
							numMalos++;
							valido = false;
						}
						
						if (sheet.getRow(i).getCell(11) == null) {
							
							valido = false;
						}
						if (sheet.getRow(i).getCell(11).getStringCellValue().trim().equalsIgnoreCase("")) {
							numMalos++;
							respuesta.put((i+1), "Falta: correo electronico" );
							valido = false;
						} 
						if(valido)
						{	
							stu.setNombre(sheet.getRow(i).getCell(1).getStringCellValue().trim());
							stu.setPrimerApellido(sheet.getRow(i).getCell(2).getStringCellValue().trim());
							stu.setSegundoApellido(apmaterno);
							stu.setCurp(sheet.getRow(i).getCell(4).getStringCellValue().trim());
							stu.setSexo(sheet.getRow(i).getCell(5).getStringCellValue().trim());
							stu.setGeneracion(sheet.getRow(i).getCell(6).getStringCellValue().trim());
						}
						Map<String, Object> filtroCarrera = new HashMap<String, Object>();
						try {
							filtroCarrera.put("cve_carrera", sheet.getRow(i).getCell(7).getStringCellValue().trim());
							
						} catch (Exception e) {
							filtroCarrera.put("cve_carrera",
									new Double(sheet.getRow(i).getCell(7).getNumericCellValue()).intValue());							
						}
						try {
							filtroCarrera.put("idCarrera_mec", (new Double(sheet.getRow(i).getCell(8).getNumericCellValue()).intValue()));							
						} catch (Exception e) {
							filtroCarrera.put("idCarrera_mec", sheet.getRow(i).getCell(8).getStringCellValue().trim());
												
						}
						
						logger.info("cve_carrera : {}" + filtroCarrera.get("cve_carrera"));
						logger.info("idCarrera_mec: {}" + filtroCarrera.get("idCarrera_mec"));
					
						
						
						if (filtroCarrera.get("cve_carrera").toString().equalsIgnoreCase("")) {
							numMalos++;
							respuesta.put((i+1), "Falta: Clave carrera" );
							valido = false;
						} 
						if (filtroCarrera.get("idCarrera_mec").toString().equalsIgnoreCase("")) {
							numMalos++;
							respuesta.put((i+1), "Falta: Id carrera MEC" );
							valido = false;
						} 
						filtroCarrera.put("id_institucion", idInst);
						List<Object> resultadosCarreras = dao.searchCatalog(new Carrera(), filtroCarrera);
						if (resultadosCarreras.size()<=0) {
							numMalos++;
							respuesta.put((i+1), "Incorrecta: Clave carrera " +filtroCarrera.get("cve_carrera").toString());
							valido = false;
						}
						if(valido)
						{
							Carrera car = ((Carrera) resultadosCarreras.get(0));
							stu.setId_carrera(car.getId_carrera());
							stu.setF_inicio_carrera(sheet.getRow(i).getCell(9).getDateCellValue());
							stu.setF_fin_carrera(sheet.getRow(i).getCell(10).getDateCellValue());
							stu.setCorreoElectronico(sheet.getRow(i).getCell(11).getStringCellValue());
							stu.setId_institucion(idInst);
							stu.setEstatus(1);
							num++;

							dao.setStudent(stu);
						}
					}
					else
					{
						numMalos++;
						respuesta.put((i+1), "Duplicada:" + matricula);
					}					
				}

			}
			workbook.close();
			fis.close();
			response.setData(respuesta);
			response.setMessage("Se registraron exitosamente " + num + " alumnos, registros erroneos: " + numMalos);
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@POST
	@Path("/student")
	public Response setStudent(@HeaderParam("token") String token, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 3);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			CatalogsDao dao = new CatalogsDao();
			String matricula = jo.get("matricula").toString();
			String nombre = jo.get("nombre").toString();
			String ap_pat = jo.get("ap_pat").toString();
			String ap_mat = jo.get("ap_mat").toString();
			String curp = jo.get("curp").toString();
			String sexo = jo.get("sexo").toString();
			String generacion = jo.get("generacion").toString();
			Integer id_carrera = Integer.parseInt(jo.get("id_carrera").toString());
			Date inicio_carrera = jo.get("inicio_carrera").toString().trim().equals("") ? null
					: df.parse(jo.get("inicio_carrera").toString());
			Date fin_carrera = jo.get("inicio_carrera").toString().trim().equals("") ? null
					: df.parse(jo.get("fin_carrera").toString());
			String correo = jo.get("correo").toString();
			Student stu = new Student();
			
			//PENDIENTE VALIDACIONES DE DATOS OBLIGATORIO
			
			int num = 0;

			Map<String, Object> filtro = new HashMap<String, Object>();
			
			if (!matricula.trim().equalsIgnoreCase("")) {
				filtro.put("matricula", matricula);
				filtro.put("id_institucion", idInst);
				List resultados = dao.searchCatalog(new Student(), filtro);
				if (resultados.size() == 0) {
					stu.setMatricula(matricula);
					stu.setNombre(nombre);
					stu.setPrimerApellido(ap_pat);
					stu.setSegundoApellido(ap_mat);
					stu.setCurp(curp);
					stu.setSexo(sexo);
					stu.setGeneracion(generacion);
					stu.setId_carrera(id_carrera);
					stu.setF_inicio_carrera(inicio_carrera);
					stu.setF_fin_carrera(fin_carrera);
					stu.setCorreoElectronico(correo);
					stu.setId_institucion(idInst);
					stu.setEstatus(1);
					dao.setStudent(stu);
				} else {
					response.setStatus(false);
					response.setMessage("La matricula ya existe, no se puede agregar nuevamente");
					response.setErrorCode("");
					return Response.status(409).entity(response).build();

				}

			}

			response.setData(null);
			response.setMessage("Se registro exitosamente el alumno");
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (java.text.ParseException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
	@Override
	@POST
	@Path("/signer")
	public Response setSigner(@HeaderParam("token") String token, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 4);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {			
			CatalogsDao dao = new CatalogsDao();
			String nombre = jo.get("nombre").toString();
			String primerApellido = jo.get("primerApellido").toString();
			String segundoApellido = jo.get("segundoApellido").toString();
			String curp = jo.get("curp").toString();
			String abrTitulo = jo.get("abrTitulo").toString();
			
			Integer idCargo = Integer.parseInt(jo.get("idCargo").toString());
			Integer status = Integer.parseInt(jo.get("status").toString());
			String pwdCertificado = jo.get("pwdCertificado").toString();			
			String certificadoResponsable = jo.get("certificadoResponsable").toString();			
			
			String keyFile = jo.get("keyFile").toString();			
			
			String noCertificadoResponsable = jo.get("noCertificadoResponsable").toString();
			
			Firmante fir = new Firmante();

			int num = 0;

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro = new HashMap<String, Object>();

			if (!curp.trim().equalsIgnoreCase("")) {
				filtro.put("curp", curp);

				List resultados = dao.searchCatalog(new Firmante(), filtro);
				if (resultados.size() == 0) {
					fir.setNombre(nombre);
					fir.setPrimerApellido(primerApellido);
					fir.setSegundoApellido(segundoApellido);
					fir.setCurp(curp);
					fir.setAbrTitulo(abrTitulo);
					fir.setPwdCertificado(StringUtils.encriptar(pwdCertificado, noCertificadoResponsable));										
					fir.setIdCargo(idCargo);					
					fir.setCertificadoResponsable(certificadoResponsable);
					fir.setStatus(status);
					fir.setKeyFile(keyFile);
					fir.setId_institucion(idInst);
					fir.setNoCertificadoResponsable(noCertificadoResponsable);
					
					dao.setSigner(fir);
				} else {
					response.setStatus(false);
					response.setMessage("El firmante ya existe, no se puede agregar nuevamente");
					response.setErrorCode("");
					return Response.status(409).entity(response).build();

				}

			}

			response.setData(null);
			response.setMessage("Se registro exitosamente el firmante");
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
	@Override
	@PUT
	@Path("/student/{id}")
	public Response updateStudent(@HeaderParam("token") String token, @PathParam("id") int id, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 3);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func2", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			CatalogsDao dao = new CatalogsDao();
			String matricula = jo.get("matricula").toString();
			String nombre = jo.get("nombre").toString();
			String ap_pat = jo.get("ap_pat").toString();
			String ap_mat = jo.get("ap_mat").toString();
			String curp = jo.get("curp").toString();
			String sexo = jo.get("sexo").toString();
			String generacion = jo.get("generacion").toString();
			Integer id_carrera = Integer.parseInt(jo.get("id_carrera").toString());
			Date inicio_carrera = jo.get("inicio_carrera").toString().trim().equals("") ? null
					: df.parse(jo.get("inicio_carrera").toString());
			Date fin_carrera = jo.get("inicio_carrera").toString().trim().equals("") ? null
					: df.parse(jo.get("fin_carrera").toString());
			String correo = jo.get("correo").toString();
			Student stu = new Student();

			int num = 0;

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro = new HashMap<String, Object>();

			if (!matricula.trim().equalsIgnoreCase("")) {
				filtro.put("id_alumno", id);

				List resultados = dao.searchCatalog(new Student(), filtro);
				if (resultados.size() > 0) {
					stu.setId_alumno(((Student) resultados.get(0)).getId_alumno());
					stu.setMatricula(((Student) resultados.get(0)).getMatricula());
					stu.setNombre(nombre);
					stu.setPrimerApellido(ap_pat);
					stu.setSegundoApellido(ap_mat);
					stu.setCurp(curp);
					stu.setSexo(sexo);
					stu.setGeneracion(generacion);
					stu.setId_carrera(id_carrera);
					stu.setF_inicio_carrera(inicio_carrera);
					stu.setF_fin_carrera(fin_carrera);
					stu.setCorreoElectronico(correo);
					stu.setId_institucion(idInst);
					stu.setEstatus(1);
					dao.updateRecord(stu);
				} else {
					response.setStatus(false);
					response.setMessage("No existe el id de alumno:" + id);
					response.setErrorCode("");
					return Response.status(409).entity(response).build();

				}

			}

			response.setData(null);
			response.setMessage("Se actualizo exitosamente el alumno");
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (java.text.ParseException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
	@Override
	@PUT
	@Path("/signer/{id}")
	public Response updateSigner(@HeaderParam("token") String token, @PathParam("id") int id, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 4);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func2", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			
			CatalogsDao dao = new CatalogsDao();
			
			String nombre = jo.get("nombre").toString();
			String primerApellido = jo.get("primerApellido").toString();
			String segundoApellido = jo.get("segundoApellido").toString();
			String curp = jo.get("curp").toString();
			String abrTitulo = jo.get("abrTitulo").toString();
			
			Integer idCargo = Integer.parseInt(jo.get("idCargo").toString());
			Integer status = Integer.parseInt(jo.get("status").toString());
			String pwdCertificado = jo.get("pwdCertificado").toString();			
			String certificadoResponsable = jo.get("certificadoResponsable").toString();			
			
			String keyFile = jo.get("keyFile").toString();			
			
			String noCertificadoResponsable = jo.get("noCertificadoResponsable").toString();			
			
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro = new HashMap<String, Object>();

			if (!nombre.trim().equalsIgnoreCase("")) {
				filtro.put("id_responsable", id);

				List resultados = dao.searchCatalog(new Firmante(), filtro);				
				Firmante fir = (Firmante) resultados.get(0);
				
				if (resultados.size() > 0) {
					
					
					fir.setNombre(nombre);
					fir.setPrimerApellido(primerApellido);
					fir.setSegundoApellido(segundoApellido);
					fir.setCurp(curp);
					fir.setAbrTitulo(abrTitulo);					 
					if(!pwdCertificado.trim().equalsIgnoreCase("") && !jo.get("pwdCertificado").toString().equalsIgnoreCase(fir.getPwdCertificado()))
					{
						fir.setPwdCertificado(StringUtils.encriptar(pwdCertificado, noCertificadoResponsable));
					}					
					fir.setIdCargo(idCargo);		
					if(certificadoResponsable.trim().length()>10)
						fir.setCertificadoResponsable(certificadoResponsable);
					fir.setStatus(status);
					
					if(keyFile.trim().length()>10)
						fir.setKeyFile(keyFile);
					fir.setId_institucion(idInst);
					fir.setNoCertificadoResponsable(noCertificadoResponsable);
					dao.updateRecord(fir);
				} else {
					response.setStatus(false);
					response.setMessage("No existe el id de firmante:" + id);
					response.setErrorCode("");
					return Response.status(409).entity(response).build();

				}

			}

			response.setData(null);
			response.setMessage("Se actualizo exitosamente el firmante");
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@POST
	@Path("/students/list")
	public Response getStudentList(@HeaderParam("token") String token, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
//			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}

		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();
			String cve_carrera = jo.get("cve_carrera").toString().trim();
			String nombre = jo.get("nombre").toString().trim();
			String matricula = jo.get("matricula").toString().trim();
			String generacion = jo.get("ciclo").toString().trim();
			if (!cve_carrera.equalsIgnoreCase(""))
				filtro.put("cve_carrera", cve_carrera);
			if (!nombre.equalsIgnoreCase(""))
				filtro.put("nombre", nombre);
			if (!matricula.equalsIgnoreCase(""))
				filtro.put("matricula", matricula);
			if (!generacion.equalsIgnoreCase(""))
				filtro.put("generacion", generacion);
			filtro.put("id_institucion", idInst);
			filtro.put("estatus", 1);// SOLO LOS ESTUDIANTES ACTIVOS
			List resultados = dao.searchCatalog(new StudentCareer(), filtro);

			response.setData(resultados);
			response.setMessage("Se encontraron " + resultados.size() + " alumnos");
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
	@Override
	@POST
	@Path("/signers/list")
	public Response getSignerList(@HeaderParam("token") String token, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}

		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();
			String nombre = jo.get("nombre").toString().trim();
			String primerApellido = jo.get("primerApellido").toString().trim();
			String segundoApellido = jo.get("segundoApellido").toString().trim();
			
			
			if (!nombre.equalsIgnoreCase(""))
				filtro.put("nombre", nombre);
			if (!primerApellido.equalsIgnoreCase(""))
				filtro.put("primerApellido", primerApellido);
			if (!segundoApellido.equalsIgnoreCase(""))
				filtro.put("segundoApellido", segundoApellido);

			filtro.put("id_institucion", idInst);
			filtro.put("status", 1);// SOLO LOS FIRMANTES ACTIVOS
			
			List resultados = dao.searchCatalog(new Firmante(), filtro);

			response.setData(resultados);
			response.setMessage("Se encontraron " + resultados.size() + " firmantes");
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@POST
	@Path("/careers/list")
	public Response getCareerList(@HeaderParam("token") String token, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}

		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();
			String id_carrera = jo.get("id_carrera").toString().trim();
			String nombre_carrera = jo.get("nombre_carrera").toString().trim();
			String cve_carrera = jo.get("cve_carrera").toString().trim();
			String nivel_educativo = jo.get("nivel_educativo").toString().trim();
			String rvoe_dgp = jo.get("rvoe_dgp").toString().trim();
			String autorizacion_reconocimiento = jo.get("autorizacion_reconocimiento").toString().trim();
			if (!id_carrera.equalsIgnoreCase(""))
				filtro.put("id_carrera", id_carrera);
			if (!nombre_carrera.equalsIgnoreCase(""))
				filtro.put("nombre_carrera", nombre_carrera);
			if (!nivel_educativo.equalsIgnoreCase(""))
				filtro.put("nivel_educativo", nivel_educativo);
			if (!cve_carrera.equalsIgnoreCase(""))
				filtro.put("cve_carrera", cve_carrera);
			if (!rvoe_dgp.equalsIgnoreCase(""))
				filtro.put("rvoe_dgp", rvoe_dgp);
			if (!autorizacion_reconocimiento.equalsIgnoreCase(""))
				filtro.put("autorizacion_reconocimiento", autorizacion_reconocimiento);
			filtro.put("id_institucion", idInst);
			filtro.put("estatus", 1);
			List resultados = dao.searchCatalog(new CarreraView(), filtro);

			response.setData(resultados);
			response.setMessage("Se encontraron " + resultados.size() + " carreras");
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@POST
	@Path("/careers")
	public Response setCareers(@HeaderParam("token") String token, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
//			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 6);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;
		boolean valido=true;
		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			Map<Integer, String> respuesta = new HashMap<Integer, String>();
			String base64 = jo.get("archivo").toString();
			String nomArchivo = jo.get("nombre_archivo").toString();
			File excelFile = new File("webapps/tmp/" + nomArchivo);

			byte[] bytes = Base64.decodeBase64(base64);
			FileUtils.writeByteArrayToFile(excelFile, bytes);
			FileInputStream fis;

			fis = new FileInputStream(excelFile);
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			// we get first sheet
			XSSFSheet sheet = workbook.getSheetAt(0);

			// we iterate on rows
			// Iterator<Row> rowIt = sheet.iterator();

			String nombre_carrera = sheet.getRow(3).getCell(1).getStringCellValue().trim();
			if (nombre_carrera.equalsIgnoreCase("") ) {
				
				respuesta.put((4), "Falta: nombre de la carrera" );
				valido = false;
			}
			String cve_carrera;
			try
			{
				cve_carrera =  sheet.getRow(4).getCell(1).getStringCellValue().trim();
			}
			catch (Exception e)
			{
				cve_carrera = String.valueOf(Math.round( sheet.getRow(4).getCell(1).getNumericCellValue())).trim();
			}
			if (cve_carrera.equalsIgnoreCase("")) {
				respuesta.put((5), "Falta: clave de la carrera" );
				valido = false;
				
			}
			
			String clavePlan ;
			
			try
			{
				clavePlan =  sheet.getRow(5).getCell(1).getStringCellValue().trim();
			}
			catch (Exception e)
			{
				clavePlan = String.valueOf(Math.round( sheet.getRow(5).getCell(1).getNumericCellValue())).trim();
			}
			if (clavePlan.equalsIgnoreCase("")) {
				respuesta.put((6), "Falta: clave del plan" );
				valido = false;
			}
			String modalidad = sheet.getRow(6).getCell(1).getStringCellValue().trim();
			if (modalidad.equalsIgnoreCase("")) {
				respuesta.put((7), "Falta: modalidad" );
				valido = false;
			}
			
			String rvoe_dgp;
			String autorizacion_reconocimiento = sheet.getRow(7).getCell(1).getStringCellValue().trim();
			if (autorizacion_reconocimiento == "") {
				respuesta.put((8), "Falta: autorización reconocimiento" );
				valido = false;				
			}
			
			try
			{
				rvoe_dgp =  sheet.getRow(8).getCell(1).getStringCellValue().trim();
			}
			catch (Exception e)
			{
				rvoe_dgp = String.valueOf(Math.round( sheet.getRow(8).getCell(1).getNumericCellValue())).trim();
			}
			if (rvoe_dgp.equalsIgnoreCase("")) {
				respuesta.put((9), "Falta: RVOE" );
				valido = false;
			}
			
			
			Date fecha_expedicion = sheet.getRow(9).getCell(1).getDateCellValue();
			if (fecha_expedicion == null) {
				respuesta.put((10), "Falta: fecha de expedición" );
				valido = false;
			}
			String idMec ;
			
			try
			{
				idMec =  sheet.getRow(10).getCell(1).getStringCellValue().trim();
			}
			catch (Exception e)
			{
				idMec = String.valueOf(Math.round( sheet.getRow(10).getCell(1).getNumericCellValue())).trim();
			}
			if (idMec.equalsIgnoreCase("")) {
				respuesta.put((11), "Falta: Id de la Carrera en el MEC" );
				valido = false;
			}
			String idTipoPeriodo = sheet.getRow(11).getCell(1).getStringCellValue().trim().substring(0,2);
			if (idTipoPeriodo.equalsIgnoreCase("")) {				
				respuesta.put((12), "Falta: tipo periodo del MEC" );
				valido = false;
			}
			String idNivelEstudios = sheet.getRow(12).getCell(1).getStringCellValue().trim().substring(0,2);
			if (idNivelEstudios.equalsIgnoreCase("")) {				
				respuesta.put((13), "Falta: Nivel Estudios del MEC" );
				valido = false;
			}
			String tipo_periodo = sheet.getRow(13).getCell(1).getStringCellValue().trim();
			if (tipo_periodo.equalsIgnoreCase("")) {				
				respuesta.put((14), "Falta: tipo periodo" );
				valido = false;
			}
			
			
			String nivel_educativo = sheet.getRow(14).getCell(1).getStringCellValue().trim();
			if (nivel_educativo.equalsIgnoreCase("")) {
				respuesta.put((15), "Falta: nivel educativo" );
				valido = false;
			}
			Integer total_materias =new Double(sheet.getRow(15).getCell(1).getNumericCellValue()).intValue();
			if (total_materias == 0) {				
				respuesta.put((16), "Falta: total de materias" );
				valido = false;
			}
			Double calificacion_minima = sheet.getRow(16).getCell(1).getNumericCellValue();
			if (calificacion_minima == 0) {				
				respuesta.put((17), "Falta: calificacion minima" );
				valido = false;
			}
			Double calificacion_maxima = sheet.getRow(17).getCell(1).getNumericCellValue();
			if (calificacion_maxima == 0) {				
				respuesta.put((18), "Falta: calificacion maxima" );
				valido = false;
			}
			Double calificacion_aprobatoria = sheet.getRow(18).getCell(1).getNumericCellValue();
			if (calificacion_aprobatoria == 0) {
				respuesta.put((19), "Falta: calificacion aprobatoria" );
				valido = false;
			}
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("rvoe_dgp", rvoe_dgp);
			filtro.put("cve_carrera", cve_carrera);
			filtro.put("id_institucion", idInst);
			List resultados = dao.searchCatalog(new Carrera(), filtro);
			Carrera car = new Carrera();
			if (resultados.size() == 0) {
				// EN EL CASO DE QUE NO EXISTA ESE RVOE DEBE AGREGAR ESA
				// CARRERA:

				car.setNombre_carrera(nombre_carrera);
				car.setId_institucion(idInst);

				Map<String, Object> fil = new HashMap<String, Object>();
				fil.put("value", autorizacion_reconocimiento);
				List resAut = dao.searchCatalog(new AutorizacionReconocimientoShort(), fil);
				if (resAut.size() == 0) {
					respuesta.put((8), "No existe esa Autorización Reconocimiento" );
					valido = false;					
				}
				if(valido)
				{
					AutorizacionReconocimientoShort au = ((AutorizacionReconocimientoShort) resAut.get(0));
					car.setId_autorizacion_reconocimiento(au.getKey());
					car.setCve_carrera(cve_carrera);
					car.setClavePlan(clavePlan);
					car.setIdCarrera_mec(idMec);
					car.setIdTipoPeriodo(idTipoPeriodo);
					car.setIdNivelEstudios(idNivelEstudios);
					car.setModalidad(modalidad);
					car.setRvoe_dgp(rvoe_dgp);
					car.setFecha_expedicion_rvoe(fecha_expedicion);
					car.setTipo_periodo(tipo_periodo);
					car.setNivel_educativo(nivel_educativo);
					car.setTotal_materias(total_materias);
					car.setCalif_minima(calificacion_minima);
					car.setCalif_maxima(calificacion_maxima);
					car.setCalif_aprob(calificacion_aprobatoria);
					car.setEstatus(1);
					
					dao.setCarrera(car);
				}
			} else {
				car = (Carrera) resultados.get(0);
			}

			// A PARTIR DE LA LINEA 17 COMIENZAN LOS DATOS DE LA ASIGNATURA:
			
			boolean validoA = true;
			int numExitosos=0, numErroneos=0;
			for (int i = 20; i < sheet.getLastRowNum(); i++) {
				validoA = true;
				filtro = new HashMap<String, Object>();
				if (sheet.getRow(i).getCell(0) == null) {
					break;
				}
				
				String clave_asignatura ;
				
				try
				{
					clave_asignatura =  sheet.getRow(i).getCell(0).getStringCellValue().trim();
				}
				catch (Exception e)
				{
					clave_asignatura = String.valueOf(Math.round( sheet.getRow(i).getCell(0).getNumericCellValue())).trim();
				}
				if (clave_asignatura.equalsIgnoreCase("")) {
//					respuesta.put((i+1), "Falta: Clave de la asignatura" );
					break;
//					numErroneos++;
					
				} 
				if (!clave_asignatura.trim().equalsIgnoreCase("")) {
					filtro.put("clave_asignatura", clave_asignatura);
					filtro.put("id_carrera", car.getId_carrera());
					filtro.put("id_institucion", idInst);
					resultados = dao.searchCatalog(new Asignatura(), filtro);
					if (resultados.size() == 0) { // EN EL CASO DE QUE NO
													// EXISTAN ESAS CLAVES DE
													// MATERIAS:
						Asignatura asig = new Asignatura();
						asig.setId_institucion(idInst);
						asig.setClave_asignatura(clave_asignatura);
						asig.setId_carrera(car.getId_carrera());
						if (sheet.getRow(i).getCell(1) == null) {
							numErroneos++;
						}
						if (sheet.getRow(i).getCell(1).getStringCellValue().trim().equalsIgnoreCase("")) {
							respuesta.put((i+1), "Falta: Descripcion asignatura" );
							validoA = false;
							numErroneos++;
						} 
						asig.setDesc_asignatura(sheet.getRow(i).getCell(1).getStringCellValue().trim());
						if (sheet.getRow(i).getCell(2) == null) {
							validoA = false;
							numErroneos++;
						}
						if (sheet.getRow(i).getCell(2).getNumericCellValue()<=0) {
							respuesta.put((i+1), "Falta: Creditos de la asignatura" );
							validoA = false;
							numErroneos++;
						}
						asig.setCreditos((sheet.getRow(i).getCell(2).getNumericCellValue()));
						if (sheet.getRow(i).getCell(3) == null) {
							validoA = false;
							numErroneos++;
						}
						if (sheet.getRow(i).getCell(3).getStringCellValue().trim().equalsIgnoreCase("")) {
							respuesta.put((i+1), "Falta: Tipo de la asignatura" );
							validoA = false;
							numErroneos++;
						} 
						if (validoA)
						{	
							asig.setTipo_asignatura(sheet.getRow(i).getCell(3).getStringCellValue().trim());
							asig.setEstatus(1);
							dao.setAsignatura(asig);
							numExitosos++;
						}
					}
					else
					{
						respuesta.put((i+1), "Duplicado: Asignatura ya asociada anteriormente "+clave_asignatura );
						validoA = false;
						numErroneos++;
					}

				}

			}

			workbook.close();
			fis.close();

//			filtro = new HashMap<String, Object>();
//			filtro.put("id_carrera", car.getId_carrera());
//			resultados = dao.searchCatalog(new Asignatura(), filtro);
			response.setData(respuesta);
			response.setMessage("Asignaturas asociadas a la carrera: " +numExitosos+", registros de asignaturas erroneas: "+ numErroneos);
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@POST
	@Path("/career")
	public Response setCareer(@HeaderParam("token") String token, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 6);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();

			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

			// we iterate on rows
			// Iterator<Row> rowIt = sheet.iterator();

			String nombre_carrera = jo.get("nombre_carrera").toString();
			String cve_carrera = jo.get("cve_carrera").toString();
			String clavePlan = jo.get("clavePlan").toString();
			String modalidad = jo.get("modalidad").toString();
			String autorizacion_reconocimiento = jo.get("autorizacion_reconocimiento").toString();
			String rvoe_dgp = jo.get("rvoe_dgp").toString();
			Date fecha_expedicion = jo.get("fecha_expedicion").toString().trim().equals("") ? null
					: df.parse(jo.get("fecha_expedicion").toString());
			String tipo_periodo = jo.get("tipo_periodo").toString();
			String nivel_educativo = jo.get("nivel_educativo").toString();
			Integer total_materias = Integer.parseInt(jo.get("total_materias").toString());
			Double calificacion_minima = Double.parseDouble(jo.get("calificacion_minima").toString());
			Double calificacion_maxima = Double.parseDouble(jo.get("calificacion_maxima").toString());
			Double calificacion_aprobatoria = Double.parseDouble(jo.get("calificacion_aprobatoria").toString());
			String idCarrera_mec= jo.get("idCarrera_mec").toString();
			String idTipoPeriodo= jo.get("idTipoPeriodo").toString();
			String idNivelEstudios = jo.get("idNivelEstudios").toString();
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("rvoe_dgp", rvoe_dgp);
			List resultados = dao.searchCatalog(new Carrera(), filtro);
			Carrera car = new Carrera();
			if (resultados.size() == 0) {
				// EN EL CASO DE QUE NO EXISTA ESE RVOE DEBE AGREGAR ESA
				// CARRERA:

				car.setNombre_carrera(nombre_carrera);
				car.setId_institucion(idInst);

				Map<String, Object> fil = new HashMap<String, Object>();
				fil.put("value", autorizacion_reconocimiento);
				List resAut = dao.searchCatalog(new AutorizacionReconocimientoShort(), fil);
				if (resAut.size() == 0) {
					response.setStatus(false);
					response.setMessage("No existe esa Autorización Reconocimiento, favor de verificar");
					response.setErrorCode("");
					return Response.status(404).entity(response).build();
				}
				AutorizacionReconocimientoShort au = ((AutorizacionReconocimientoShort) resAut.get(0));
				car.setId_autorizacion_reconocimiento(au.getKey());
				car.setRvoe_dgp(rvoe_dgp);
				car.setClavePlan(clavePlan);
				car.setModalidad(modalidad);
				car.setIdCarrera_mec(idCarrera_mec);
				car.setIdTipoPeriodo(idTipoPeriodo);
				car.setIdNivelEstudios(idNivelEstudios);
				
				car.setCve_carrera(cve_carrera);
				car.setFecha_expedicion_rvoe(fecha_expedicion);
				car.setTipo_periodo(tipo_periodo);
				car.setNivel_educativo(nivel_educativo);
				car.setTotal_materias(total_materias);
				car.setCalif_minima(calificacion_minima);
				car.setCalif_maxima(calificacion_maxima);
				car.setCalif_aprob(calificacion_aprobatoria);
				car.setEstatus(1);
				dao.setCarrera(car);
			} else {
				car = (Carrera) resultados.get(0);
			}

			response.setData(null);
			response.setMessage("Carrera agregada: " + car.getNombre_carrera());
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (java.text.ParseException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();

		}
	}

	@Override
	@PUT
	@Path("/career/{id}")
	public Response updateCareer(@HeaderParam("token") String token, @PathParam("id") int id, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 6);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func2", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();

			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

			// we iterate on rows
			// Iterator<Row> rowIt = sheet.iterator();

			String nombre_carrera = jo.get("nombre_carrera").toString();
			String cve_carrera = jo.get("cve_carrera").toString();
			String modalidad = jo.get("modalidad").toString();
			String clavePlan = jo.get("clavePlan").toString();
			String autorizacion_reconocimiento = jo.get("autorizacion_reconocimiento").toString();
			String rvoe_dgp = jo.get("rvoe_dgp").toString();
			Date fecha_expedicion = jo.get("fecha_expedicion").toString().trim().equals("") ? null
					: df.parse(jo.get("fecha_expedicion").toString());
			String tipo_periodo = jo.get("tipo_periodo").toString();
			String nivel_educativo = jo.get("nivel_educativo").toString();
			Integer total_materias = Integer.parseInt(jo.get("total_materias").toString());
			Double calificacion_minima = Double.parseDouble(jo.get("calificacion_minima").toString());
			Double calificacion_maxima = Double.parseDouble(jo.get("calificacion_maxima").toString());
			Double calificacion_aprobatoria = Double.parseDouble(jo.get("calificacion_aprobatoria").toString());
			String idCarrera_mec= jo.get("idCarrera_mec").toString();
			String idTipoPeriodo= jo.get("idTipoPeriodo").toString();
			String idNivelEstudios = jo.get("idNivelEstudios").toString();
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("id_carrera", id);
			filtro.put("id_institucion", idInst);
			List resultados = dao.searchCatalog(new Carrera(), filtro);
			Carrera car = new Carrera();
			if (resultados.size() > 0) {
				car.setId_carrera(((Carrera) resultados.get(0)).getId_carrera());
				car.setNombre_carrera(nombre_carrera);
				car.setId_institucion(idInst);
				Map<String, Object> fil = new HashMap<String, Object>();
				fil.put("value", autorizacion_reconocimiento);
				List resAut = dao.searchCatalog(new AutorizacionReconocimientoShort(), fil);
				if (resAut.size() == 0) {
					response.setStatus(false);
					response.setMessage("No existe esa Autorización Reconocimiento, favor de verificar");
					response.setErrorCode("");
					return Response.status(404).entity(response).build();
				}
				AutorizacionReconocimientoShort au = ((AutorizacionReconocimientoShort) resAut.get(0));
				car.setId_autorizacion_reconocimiento(au.getKey());
				car.setRvoe_dgp(rvoe_dgp);
				car.setClavePlan(clavePlan);
				car.setModalidad(modalidad);
				car.setIdCarrera_mec(idCarrera_mec);
				car.setIdTipoPeriodo(idTipoPeriodo);
				car.setIdNivelEstudios(idNivelEstudios);
				car.setCve_carrera(cve_carrera);				
				car.setFecha_expedicion_rvoe(fecha_expedicion);
				car.setTipo_periodo(tipo_periodo);
				car.setNivel_educativo(nivel_educativo);
				car.setTotal_materias(total_materias);
				car.setCalif_minima(calificacion_minima);
				car.setCalif_maxima(calificacion_maxima);
				car.setCalif_aprob(calificacion_aprobatoria);
				car.setEstatus(1);
				dao.updateRecord(car);
			} else {
				response.setStatus(false);
				response.setMessage("No existe el id de carrera:" + id);
				response.setErrorCode("");
				return Response.status(404).entity(response).build();
			}

			response.setData(null);
			response.setMessage("Carrera modificada: " + car.getNombre_carrera());
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (java.text.ParseException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();

		}
	}

	@Override
	@GET
	@Path("/students/{id}/scores")
	public Response getScores(@HeaderParam("token") String token, @PathParam("id") String id) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		String strJson = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("matricula", id);
			filtro.put("id_institucion", idInst);
			List resultados = dao.searchCatalog(new Student(), filtro);

			if (resultados.size() == 0) {
				response.setStatus(false);
				response.setMessage("El estudiante no existe");
				response.setErrorCode("");
				return Response.status(404).entity(response).build();
			}
			Student stu = ((Student) resultados.get(0));
			filtro = new HashMap<String, Object>();
			filtro.put("id_alumno", stu.getId_alumno());
			
			response.setData(dao.searchCatalog(new CalificacionInfo(), filtro));
			response.setMessage("Calificaciones del alumno:" + id);
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@GET
	@Path("/career/{id}/subjects")
	public Response getSubjects(@HeaderParam("token") String token, @PathParam("id") int id) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		String strJson = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("key", id);
			List resultados = dao.searchCatalog(new CarreraShort(), filtro);

			if (resultados.size() == 0) {
				response.setStatus(false);
				response.setMessage("La carrera no existe");
				response.setErrorCode("");
				return Response.status(404).entity(response).build();
			}
			CarreraShort car = ((CarreraShort) resultados.get(0));
			filtro = new HashMap<String, Object>();
			filtro.put("id_carrera", car.getKey());
			filtro.put("estatus", 1);// SOLO REGISTROS ACTIVOS
			response.setData(dao.searchCatalog(new Asignatura(), filtro));
			response.setMessage("Asignaturas de la carrera: " + car.getValue());
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@POST
	@Path("/career/{id}/subject")
	public Response setSubject(@HeaderParam("token") String token, @PathParam("id") int id_carrera, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 10);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();

			// we iterate on rows
			// Iterator<Row> rowIt = sheet.iterator();

			String clave_asignatura = jo.get("clave_asignatura").toString();
			String id_asignatura_mec = jo.get("id_asignatura_mec").toString();
			String desc_asignatura = jo.get("desc_asignatura").toString();
			String tipo_asignatura = jo.get("tipo_asignatura").toString();
			Double creditos = Double.parseDouble(jo.get("creditos").toString());

			Map<String, Object> filtro = new HashMap<String, Object>();

			List resultados = null;

			if (!clave_asignatura.trim().equalsIgnoreCase("")) {
				filtro.put("clave_asignatura", clave_asignatura);
				filtro.put("id_carrera", id_carrera);

				resultados = dao.searchCatalog(new Asignatura(), filtro);
				if (resultados.size() == 0) { // EN EL CASO DE QUE NO
												// EXISTAN ESAS CLAVES DE
												// MATERIAS:
					Asignatura asig = new Asignatura();
					asig.setId_institucion(idInst);
					asig.setClave_asignatura(clave_asignatura);
					asig.setId_asignatura_mec(Integer.parseInt(id_asignatura_mec));
					asig.setId_carrera(id_carrera);
					asig.setDesc_asignatura(desc_asignatura);
					asig.setCreditos(creditos);
					asig.setTipo_asignatura(tipo_asignatura);
					asig.setEstatus(1);
					dao.setAsignatura(asig);
				} else {
					response.setStatus(false);
					response.setMessage("Esa clave de asignatura ya existe");
					response.setErrorCode("");
					return Response.status(404).entity(response).build();
				}

			}

			response.setData(null);
			response.setMessage("Asignatura agregada: " + clave_asignatura);
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();

		}
	}

	@Override
	@PUT
	@Path("/career/{id_carrera}/subject/{id_subject}")
	public Response updateSubject(@HeaderParam("token") String token, @PathParam("id_carrera") int id_carrera,
			@PathParam("id_subject") int id_subject, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 10);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func2", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();

			// we iterate on rows
			// Iterator<Row> rowIt = sheet.iterator();

			String clave_asignatura = jo.get("clave_asignatura").toString();
			String id_asignatura_mec = jo.get("id_asignatura_mec").toString();
			String desc_asignatura = jo.get("desc_asignatura").toString();
			String tipo_asignatura = jo.get("tipo_asignatura").toString();
			Double creditos = Double.parseDouble(jo.get("creditos").toString());

			Map<String, Object> filtro = new HashMap<String, Object>();

			List resultados = null;

			if (!clave_asignatura.trim().equalsIgnoreCase("")) {
				filtro.put("id_asignatura", id_subject);
				filtro.put("id_carrera", id_carrera);

				resultados = dao.searchCatalog(new Asignatura(), filtro);
				if (resultados.size() > 0) {
					Asignatura asig = new Asignatura();
					asig.setId_asignatura(id_subject);
					asig.setId_asignatura_mec(Integer.parseInt(id_asignatura_mec));
					asig.setId_institucion(idInst);
					asig.setClave_asignatura(clave_asignatura);
					asig.setId_carrera(id_carrera);
					asig.setDesc_asignatura(desc_asignatura);
					asig.setCreditos(creditos);
					asig.setTipo_asignatura(tipo_asignatura);
					asig.setEstatus(1);
					dao.updateRecord(asig);
				} else {
					response.setStatus(false);
					response.setMessage("Esa asignatura no existe con la carrera seleccionada");
					response.setErrorCode("");
					return Response.status(404).entity(response).build();
				}

			}

			response.setData(null);
			response.setMessage("Asignatura modificada: " + clave_asignatura);
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();

		}
	}

	@Override
	@GET
	@Path("/careers/{id}/students")
	public Response getStudents(@HeaderParam("token") String token, @PathParam("id") int id) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		String strJson = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("key", id);
			List resultados = dao.searchCatalog(new CarreraShort(), filtro);

			if (resultados.size() == 0) {
				response.setStatus(false);
				response.setMessage("La carrera no existe");
				response.setErrorCode("");
				return Response.status(404).entity(response).build();
			}
			CarreraShort car = ((CarreraShort) resultados.get(0));
			filtro = new HashMap<String, Object>();
			filtro.put("id_carrera", car.getKey());
			filtro.put("estatus", 1);// SOLO REGISTROS ACTIVOS
			response.setData(dao.searchCatalog(new Student(), filtro));
			response.setMessage("Alumnos de la carrera: " + car.getValue());
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
	
	@Override
	@GET
	@Path("/firmantes")
	public Response getFirmantes(@HeaderParam("token") String token) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}		
		String strJson = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("id_institucion", idInst);			
			response.setData(dao.searchCatalog(new VistaFirmante(), filtro));
			response.setMessage("Firmantes de la institución" );
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
	
	@Override
	@GET
	@Path("/configuraciones")
	public Response getDatosConfiguracionSEP(@HeaderParam("token") String token) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}		
		String strJson = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("id_institucion", idInst);			
			response.setData(dao.searchCatalog(new Configurations(), filtro));
			response.setMessage("Datos de configuracion de la institución" );
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
	@Override
	@PUT
	@Path("/configuraciones/{id}")
	public Response updateConfig(@HeaderParam("token") String token, @PathParam("id") int id_registro, String json) {
		GenericResponse response = new GenericResponse();

		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			 return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 12);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();

			String url_tit = jo.get("url_tit").toString();
			String usuario_tit = jo.get("usuario_tit").toString();
			String password_tit = jo.get("password_tit").toString();
			String url_cert = jo.get("url_cert").toString();
			String usuario_cert = jo.get("usuario_cert").toString();
			String password_cert = jo.get("password_cert").toString();
			Integer id_tipoAmbiente_tit = Integer.parseInt(jo.get("id_tipoAmbiente_tit").toString());
			Integer id_tipoAmbiente_cert = Integer.parseInt(jo.get("id_tipoAmbiente_cert").toString());
			

			Configurations cfg = dao.getConfig(id_registro);

			if(!cfg.getPassword_tit().equalsIgnoreCase(password_tit))
				cfg.setPassword_tit(StringUtils.encriptar(password_tit, usuario_tit));
			if(!cfg.getPassword_cert().equalsIgnoreCase(password_cert))
				cfg.setPassword_cert(StringUtils.encriptar(password_cert, usuario_cert));
			
			cfg.setUrl_tit(url_tit);
			cfg.setUrl_cert(url_cert);
			cfg.setUsuario_tit(usuario_tit);
			cfg.setUsuario_cert(usuario_cert);
			cfg.setId_tipoAmbiente_cert(id_tipoAmbiente_cert);
			cfg.setId_tipoAmbiente_tit(id_tipoAmbiente_tit);
			dao.updateRecord(cfg);

			response.setData(null);
			response.setMessage("Configuracion actualizada");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
	@Override
	@GET
	@Path("/schools")
	public Response getSchools(@HeaderParam("token") String token) {

		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			 return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		CatalogsDao dao = new CatalogsDao();
		String strJson = "";
		try {
			GenericResponse response = new GenericResponse();
			response.setData(dao.getCatalogItems("School"));
			response.setMessage("Lista de instituciones");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));

			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@POST
	@Path("/schools")
	public Response addSchool(@HeaderParam("token") String token, String json) {

		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			 return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 8);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();

			String cveInstitucion = jo.get("cveInstitucion").toString();
			String nombreInstitucion = jo.get("nombreInstitucion").toString();
			String idNombreInstitucion = jo.get("idNombreInstitucion").toString();
			String campus = jo.get("campus").toString();
			String rfc = jo.get("rfc").toString();
			String razonSocial = jo.get("razonSocial").toString();
			String localidad = jo.get("localidad").toString();
			Integer id_entidad_federativa = Integer.parseInt(jo.get("id_entidad_federativa").toString());

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("cveInstitucion", cveInstitucion);
			List resultados = dao.searchCatalog(new School(), filtro);
			School sch = new School();
			if (resultados.size() == 0) {
				sch.setEstatus(1);
				sch.setTitulos_timbrados(0);
				sch.setCveInstitucion(cveInstitucion);
				sch.setLocalidad(localidad);
				sch.setNombreInstitucion(nombreInstitucion);
				sch.setId_entidad_federativa(id_entidad_federativa);
				sch.setRfc(rfc);
				sch.setCampus(campus);
				sch.setIdNombreInstitucion(idNombreInstitucion);
				sch.setRazonSocial(razonSocial);
				dao.setSchool(sch);
			} else {
				sch = (School) resultados.get(0);
			}

			response.setData(null);
			response.setMessage("Institucion agregada: " + sch.getNombreInstitucion());
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@PUT
	@Path("/schools/{id}")
	public Response updateSchool(@HeaderParam("token") String token, @PathParam("id") int id, String json) {
		GenericResponse response = new GenericResponse();

		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			 return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 8);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func2", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();

			String cveInstitucion = jo.get("cveInstitucion").toString();
			String nombreInstitucion = jo.get("nombreInstitucion").toString();
			String idNombreInstitucion = jo.get("idNombreInstitucion").toString();
			String campus = jo.get("campus").toString();
			String rfc = jo.get("rfc").toString();
			String razonSocial = jo.get("razonSocial").toString();
			String localidad = jo.get("localidad").toString();
			Integer id_entidad_federativa = Integer.parseInt(jo.get("id_entidad_federativa").toString());
			Map<String, Object> filtro = new HashMap<String, Object>();
			School sch = new School();
			filtro = new HashMap<String, Object>();
			filtro.put("id_institucion", id);
			List resultados = dao.searchCatalog(new School(), filtro);
			if (resultados.size() == 0) {
				response.setStatus(false);
				response.setMessage("No existe el id de institucion: " + id);
				response.setErrorCode("");
				response.setToken(jwtutil.refreshToken(token));
				return Response.status(404).entity(response).build();
			}
			sch.setId_entidad_federativa(id_entidad_federativa);
			sch.setId_institucion(id);
			sch.setCveInstitucion(cveInstitucion);
			sch.setLocalidad(localidad);
			sch.setNombreInstitucion(nombreInstitucion);
			sch.setRazonSocial(razonSocial);
			sch.setRfc(rfc);
			sch.setCampus(campus);
			sch.setIdNombreInstitucion(idNombreInstitucion);
			dao.updateRecord(sch);
			response.setData(null);
			response.setMessage("Institucion actualizada");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@DELETE
	@Path("/schools/{id}")
	public Response deactivateSchool(@HeaderParam("token") String token, @PathParam("id") int id) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 8);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func3", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL

		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();

			filtro.put("id_institucion", id);

			List resultados = dao.searchCatalog(new School(), filtro);
			if (resultados.size() == 0) {
				response.setStatus(false);
				response.setMessage("No existe la institucion con el id: " + id);
				response.setErrorCode("");
				response.setToken(jwtutil.refreshToken(token));
				return Response.status(404).entity(response).build();
			} else {
				School ent = (School) resultados.get(0);
				ent.setEstatus(0);
				dao.updateRecord(ent);
			}

			response.setData(null);
			response.setMessage("Institucion eliminada");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
}
