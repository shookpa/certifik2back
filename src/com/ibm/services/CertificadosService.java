package com.ibm.services;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

public interface CertificadosService {
	
	
	public Response setCertificado(@HeaderParam("token") String token, String json);
	public Response updateCertificado(@HeaderParam("token") String token, String json,int id_certif);
	//public Response getCertificados(@HeaderParam("token") String token, String json, int limit, int offset);
	public Response getCertificados(@HeaderParam("token") String token, String json);
	public Response getCertificadoImpresion(String token,  int id);
	public Response getCertificadoEdicion(String token,  int id);
	public Response sendCertificado(@HeaderParam("token") String token, Integer id);
	public Response getXML(@HeaderParam("token") String token, Integer id);
}
