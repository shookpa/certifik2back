package com.ibm.services;

import java.io.File;
import java.io.IOException;
import java.math.RoundingMode;
import java.security.PrivateKey;
import java.security.Signature;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.ssl.PKCS8Key;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ibm.dao.CatalogsDao;
import com.ibm.dao.CertificadoDao;
import com.ibm.dao.RolOperationDao;
import com.ibm.dao.UserDao;
import com.ibm.models.Asignatura;
import com.ibm.models.CargosFirmantesShort;
import com.ibm.models.Carrera;
import com.ibm.models.Certificado;
import com.ibm.models.EntidadFederativaShort;
import com.ibm.models.Firmante;
import com.ibm.models.GenericResponse;
import com.ibm.models.School;
import com.ibm.models.Student;
import com.ibm.models.User;
import com.ibm.models.VistaCalificacionesCertificado;
import com.ibm.models.VistaCertificado;
import com.ibm.util.JWTUtil;
import com.ibm.util.StringUtils;

@Path("/rest")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CertificadosServiceImpl implements CertificadosService {
	private static final Logger logger = Logger.getLogger(TitulosServiceImpl.class);
	@Override
	@POST
	// @Path("/certificados/limit/{limit}/offset/{offset}")
	// public Response getCertificados(@HeaderParam("token") String token,
	// String json, @PathParam("limit") int limit, @PathParam("offset") int
	// offset) {
	@Path("/certificados/limit/{limit}/offset/{offset}")
	public Response getCertificados(@HeaderParam("token") String token, String json) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		//
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}

		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;
		String tokenDato = "";
		try {
			Map<String, Object> filtro = new HashMap<String, Object>();
			String id_carrera = jo.get("id_carrera").toString().trim();
			String matricula = jo.get("matricula").toString().trim();

			if (!id_carrera.equalsIgnoreCase(""))
				filtro.put("id_carrera", id_carrera);

			if (!matricula.equalsIgnoreCase(""))
				filtro.put("matricula", matricula);

			filtro.put("id_institucion", idInst);
			CertificadoDao dao = new CertificadoDao();
			Map<String, Object> responseData = new HashMap<String, Object>();
			List resultados = dao.listCertificados(new VistaCertificado(), filtro);
			responseData.put("countAll", resultados.size());
			responseData.put("pageResults", resultados);
			response.setData(responseData);
			response.setMessage("Lista Certificados");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@GET
	@Path("/certificados/{id}/impresion")
	public Response getCertificadoImpresion(@HeaderParam("token") String token, @PathParam("id") int id) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		String strJson = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("id_certificado", id);
			List resultados = dao.searchCatalog(new VistaCertificado(), filtro);

			if (resultados.size() == 0) {
				response.setStatus(false);
				response.setMessage("El certificado no existe");
				response.setErrorCode("");
				return Response.status(404).entity(response).build();
			}
			VistaCertificado certif = ((VistaCertificado) resultados.get(0));
			filtro = new HashMap<String, Object>();
			filtro.put("id_alumno", certif.getId_alumno());
			filtro.put("id_carrera", certif.getId_carrera());
			filtro.put("id_certificado", certif.getId_certificado());
			List resultadosCalif = dao.searchCatalog(new VistaCalificacionesCertificado(), filtro);
			HashMap<String, Object> respuesta = new HashMap<String, Object>();
			respuesta.put("encabezado", certif);
			respuesta.put("numCalif", resultadosCalif.size());
			respuesta.put("calificaciones", resultadosCalif);

			response.setData(respuesta);
			response.setMessage("Datos para la impresion del Certificado: " + id);
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@GET
	@Path("/certificados/{id}/edicion")
	public Response getCertificadoEdicion(@HeaderParam("token") String token, @PathParam("id") int id) {
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		String strJson = "";
		try {
			CatalogsDao dao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("id_certificado", id);
			List resultados = dao.searchCatalog(new VistaCertificado(), filtro);

			if (resultados.size() == 0) {
				response.setStatus(false);
				response.setMessage("El certificado no existe");
				response.setErrorCode("");
				return Response.status(404).entity(response).build();
			}
			VistaCertificado certif = ((VistaCertificado) resultados.get(0));

			response.setData(certif);
			response.setMessage("Datos para la edicion del Certificado: " + id);
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@POST
	@Path("/certificados/add")
	public Response setCertificado(@HeaderParam("token") String token, String json) {
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		GenericResponse response = new GenericResponse();
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Faltan los parametros del json");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 1);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		org.json.JSONObject jo = new org.json.JSONObject(json);
		CertificadoDao dao = new CertificadoDao();
		CatalogsDao catdao = new CatalogsDao();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		String tokenDato = "";
		try {
			Certificado certif = new Certificado();
			certif.setId_institucion(idInst);
			certif.setId_firmante(Integer.parseInt(jo.get("id_firmante").toString()));
			certif.setId_carrera(Integer.parseInt(jo.get("id_carrera").toString()));
			certif.setMatricula(jo.get("matricula").toString());
			certif.setTipo_certif(jo.get("tipo_certif").toString());
			certif.setNum_certif(jo.get("num_certif").toString());
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("matricula", certif.getMatricula());
			filtro.put("id_institucion", idInst);
			Student stu = catdao.searchStudent(filtro);
			certif.setId_alumno(stu.getId_alumno());			
			certif.setTotal_materias(Integer.parseInt(jo.get("total_materias").toString()));
			certif.setTotal_asignadas(Integer.parseInt(jo.get("total_asignadas").toString()));
			certif.setTotal_aprobadas(Integer.parseInt(jo.get("total_aprobadas").toString()));
			certif.setPromedio(Double.parseDouble(jo.get("promedio").toString()));
			certif.setCreditos_obtenidos(Double.parseDouble(jo.get("creditos_obtenidos").toString()));			
			certif.setTotal_creditos(Double.parseDouble(jo.get("total_creditos").toString()));
			certif.setNumero_ciclos(Integer.parseInt(jo.get("numero_ciclos").toString()));
			certif.setFecha_expedicion(jo.get("fecha_expedicion").toString().trim().equals("") ? null
					: df.parse(jo.get("fecha_expedicion").toString()));
			certif.setFecha_registro(new Date());
			certif.setFecha_modificacion(new Date());
			certif.setEstatus_certificado(1);
			certif.setFolioControl("");
			dao.setCertificado(certif);
			//PENDIENTE ALGO SUPER IMPORTANTE:
			/**
			 * INSERT INTO `certificado_calificaciones` ( `id_certificado`, `id_institucion`, `id_carrera`, `id_alumno`, `id_asignatura`, `periodo`, `ciclo`, `calificacion`, `id_observacion`) SELECT 3 as id_certificado,`id_institucion`, `id_carrera`, `id_alumno`, `id_asignatura`, `periodo`, `ciclo`, `calificacion`, `id_observacion` FROM `calificaciones` WHERE id_alumno = 645

			 */
			response.setMessage("Certificado creado exitosamente");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
	@Override
	@PUT
	@Path("/certificados/{id_certif}")
	public Response updateCertificado(@HeaderParam("token") String token, String json,@PathParam("id_certif") int id_certif) {
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		GenericResponse response = new GenericResponse();
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Faltan los parametros del json");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();
		Map<String, Object> filtro = new HashMap<String, Object>();
		List resultados = null;
		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 1);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		org.json.JSONObject jo = new org.json.JSONObject(json);
		CertificadoDao dao = new CertificadoDao();
		CatalogsDao catdao = new CatalogsDao();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		String tokenDato = "";
		try {
			
			filtro.put("id_certificado", id_certif);

			resultados = catdao.searchCatalog(new Certificado(), filtro);
			if (resultados.size() > 0) {
				Certificado certif = (Certificado) resultados.get(0);
				certif.setId_certificado(id_certif);
				certif.setFecha_expedicion(jo.get("fecha_expedicion").toString().trim().equals("") ? null
						: df.parse(jo.get("fecha_expedicion").toString()));
				certif.setFecha_modificacion(new Date());
				catdao.updateRecord(certif);
				
			} else {
				response.setStatus(false);
				response.setMessage("No existe dicho id de certificado");
				response.setErrorCode("");
				return Response.status(404).entity(response).build();
			}
			
			
		
			
			response.setMessage("Certificado actualizado exitosamente");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
	@Override
	@GET
	@Path("/certificados/{id}/send")
	public Response sendCertificado(String token, Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@GET
	@Path("/certificados/{id}/xml")
	public Response getXML(@HeaderParam("token") String token, @PathParam("id") Integer id) {
		logger.info("Por Generar el XML de Certificado con el id : {}" + id);
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
//		if (!jwtutil.validateToken(token)) {
//			 return Response.status(Response.Status.UNAUTHORIZED).build();
//		}
		GenericResponse response = new GenericResponse();

		int idInst = jwtutil.getInstitucionFromToken(token);

		int idUsuario = jwtutil.getIdUsuarioFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 1);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func4", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		 if (rod.searchRolOperation(filtroRol).length == 0)
			 return Response.status(Response.Status.UNAUTHORIZED).build();
		CatalogsDao catDao = new CatalogsDao();
		CertificadoDao dao = new CertificadoDao();
		UserDao usDao = new UserDao();
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String tokenDato;
		String xmlFilePath = "certificado_" + id + ".xml";
		
		// FALTA VALIDAR QUE NO ESTE ENVIADO, ENTONCES TENEMOS QUE HCER UNA
		// BUSQUEDA:

		try {
			StringUtils cadena = new StringUtils();

			Certificado certif = dao.getCertificado(id);
			School sch = usDao.getSchool(certif.getId_institucion());
		
			String folioControlStr = new String();
			if (certif.getFolioControl().trim().equalsIgnoreCase("")) {
				folioControlStr = cadena.getCadenaAleatoria(20);
			} else
				folioControlStr = certif.getFolioControl();

			Carrera car = catDao.getCarrera(certif.getId_carrera());
			Firmante firmante = catDao.getFirmante(certif.getId_firmante());
			CargosFirmantesShort cargoFirmante = catDao.getCargosFirmantesShort(firmante.getIdCargo());
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("matricula", certif.getMatricula());
			Student stu = catDao.searchStudent(filtro);
			EntidadFederativaShort ent = catDao.getEntidadFederativaShort(sch.getId_entidad_federativa());
			filtro = new HashMap<String, Object>();
			filtro.put("id_alumno", certif.getId_alumno());
			filtro.put("id_carrera", certif.getId_carrera());
			filtro.put("id_certificado", certif.getId_certificado());
			List resultadosCalif = catDao.searchCatalog(new VistaCalificacionesCertificado(), filtro);
			
			
			
			

			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
			Document document = documentBuilder.newDocument();

			// root element
			Element root = document.createElement("Dec");
			Attr xmlns = document.createAttribute("xmlns");
			Attr xmlnsxsi = document.createAttribute("xmlns:xsi");
			Attr version = document.createAttribute("version");
			Attr tipoCertificado = document.createAttribute("tipoCertificado");
			//Quitamos esta madre que esta mandando error con este atributo:
//			Attr xsischemaLocation = document.createAttribute("xsi:schemaLocation");
			Attr folioControl = document.createAttribute("folioControl");
			Attr certificadoResponsable = document.createAttribute("certificadoResponsable");
			Attr noCertificadoResponsable = document.createAttribute("noCertificadoResponsable");
			Attr sello = document.createAttribute("sello");
			xmlns.setValue("https://www.siged.sep.gob.mx/certificados/");
			//xmlnsxsi.setValue("http://www.w3.org/2001/XMLSchema-instance");
			version.setValue("3.0");
			tipoCertificado.setValue("5");
//			xsischemaLocation.setValue("https://www.siged.sep.gob.mx/titulos/schema.xsd");
			folioControl.setValue(folioControlStr);
			certificadoResponsable.setValue(firmante.getCertificadoResponsable());
			noCertificadoResponsable.setValue(firmante.getNoCertificadoResponsable());
			root.setAttributeNode(xmlns);
			root.setAttributeNode(xmlnsxsi);
			root.setAttributeNode(version);
			root.setAttributeNode(tipoCertificado);
//			root.setAttributeNode(xsischemaLocation);
			root.setAttributeNode(folioControl);
			root.setAttributeNode(certificadoResponsable);
			root.setAttributeNode(noCertificadoResponsable);
			document.appendChild(root);

			Element Ipes = document.createElement("Ipes");		

			Element Responsable = document.createElement("Responsable");
			Attr curpResponsable = document.createAttribute("curp");
			Attr nombreResponsable = document.createAttribute("nombre");
			Attr primerApellidoResponsable = document.createAttribute("primerApellido");
			Attr segundoApellidoResponsable = document.createAttribute("segundoApellido");
			Attr idCargoResponsable = document.createAttribute("idCargo");
			Attr cargoResponsable = document.createAttribute("cargo");
			System.out.println("Firmante seleccionado:" + firmante);
			nombreResponsable.setValue(firmante.getNombre());
			primerApellidoResponsable.setValue("RUÍZ"); //PENDIENTE firmante.getPrimerApellido(
			segundoApellidoResponsable.setValue(firmante.getSegundoApellido());
			curpResponsable.setValue(firmante.getCurp());
			idCargoResponsable.setValue(firmante.getIdCargo().toString());
			cargoResponsable.setValue(cargoFirmante.getValue().toString());
			Responsable.setAttributeNode(nombreResponsable);
			Responsable.setAttributeNode(primerApellidoResponsable);
			Responsable.setAttributeNode(segundoApellidoResponsable);
			Responsable.setAttributeNode(curpResponsable);
			Responsable.setAttributeNode(idCargoResponsable);
			Responsable.setAttributeNode(cargoResponsable);
			Ipes.appendChild(Responsable);
			//CREO QUE EL ID DE INSTITUCION ES LA CLAVE QUE TIENE CON LA SEP, PERO POR VERIFICAR:
			Attr idNombreInstitucion = document.createAttribute("idNombreInstitucion");
			idNombreInstitucion.setValue(sch.getIdNombreInstitucion());
			//EL NOMBRE DE LA INSTITUCION LO TOMARE DE LA RAZON SOCIAL 
			Attr nombreInstitucion = document.createAttribute("nombreInstitucion");
			nombreInstitucion.setValue(sch.getNombreInstitucion());
			//PENDIENTE REVISAR DE DONDE OBTENEMOS EL ID DE CAMPUS:  			
			Attr idCampus = document.createAttribute("idCampus");
			idCampus.setValue(String.valueOf(sch.getCveInstitucion())); //aun no se de donde sacarlo			
			//EL NOMBRE DEL CAMPUS LO TOMARE DEL NOMBRE DE LA INSTITUCION
			Attr campus = document.createAttribute("campus");
			campus.setValue(sch.getCampus());
			
			Attr idEntidadFederativa = document.createAttribute("idEntidadFederativa");
			idEntidadFederativa.setValue(String.valueOf(ent.getKey()));		
			Attr entidadFederativa = document.createAttribute("entidadFederativa");
			entidadFederativa.setValue(ent.getValue());
			
			Ipes.setAttributeNode(idNombreInstitucion);
			Ipes.setAttributeNode(nombreInstitucion);
			Ipes.setAttributeNode(idCampus);
			Ipes.setAttributeNode(campus);
			Ipes.setAttributeNode(idEntidadFederativa);
			Ipes.setAttributeNode(entidadFederativa);
			root.appendChild(Ipes);
			Element Rvoe = document.createElement("Rvoe");
			Attr numero = document.createAttribute("numero");
			numero.setValue(car.getRvoe_dgp());
			Attr fechaExpedicion = document.createAttribute("fechaExpedicion");
			fechaExpedicion.setValue(df.format(car.getFecha_expedicion_rvoe())+"T00:00:00");		
			Rvoe.setAttributeNode(numero);
			Rvoe.setAttributeNode(fechaExpedicion);
			root.appendChild(Rvoe);
			
			Element Carrera = document.createElement("Carrera");

			Attr idCarrera = document.createAttribute("idCarrera");
			//AL ID DE CARRERA LE PONGO LA CLAVE DE LA CARRERA:
			idCarrera.setValue(car.getIdCarrera_mec());			
			Attr nombreCarrera = document.createAttribute("nombreCarrera");
			nombreCarrera.setValue(car.getNombre_carrera().replaceAll("LICENCIATURA EN", "").trim());
			Attr idTipoPeriodo = document.createAttribute("idTipoPeriodo");
			idTipoPeriodo.setValue(car.getIdTipoPeriodo());
			Attr tipoPeriodo = document.createAttribute("tipoPeriodo");
			tipoPeriodo.setValue(car.getTipo_periodo());
			Attr clavePlan = document.createAttribute("clavePlan");
			clavePlan.setValue(car.getClavePlan());
			Attr calificacionMinima = document.createAttribute("calificacionMinima");
			calificacionMinima.setValue(String.format("%d",car.getCalif_minima().intValue()));
			Attr calificacionMaxima = document.createAttribute("calificacionMaxima");
			calificacionMaxima.setValue(String.format("%d", car.getCalif_maxima().intValue()));
			Attr calificacionMinimaAprobatoria = document.createAttribute("calificacionMinimaAprobatoria");
			calificacionMinimaAprobatoria.setValue(String.format("%.2f",car.getCalif_aprob()));
			Attr idNivelEstudios = document.createAttribute("idNivelEstudios");
			idNivelEstudios.setValue(car.getIdNivelEstudios());
			
			Carrera.setAttributeNode(idCarrera);
			Carrera.setAttributeNode(idNivelEstudios);
			Carrera.setAttributeNode(nombreCarrera);
			Carrera.setAttributeNode(idTipoPeriodo);
			Carrera.setAttributeNode(tipoPeriodo);
			Carrera.setAttributeNode(clavePlan);			
			Carrera.setAttributeNode(calificacionMinima);			
			Carrera.setAttributeNode(calificacionMaxima);			
			Carrera.setAttributeNode(calificacionMinimaAprobatoria);			
			root.appendChild(Carrera);
			Element Alumno = document.createElement("Alumno");
			Attr numeroControl = document.createAttribute("numeroControl");
			numeroControl.setValue(certif.getMatricula());
			Attr curpAlumno = document.createAttribute("curp");
			curpAlumno.setValue(stu.getCurp());
			
			Attr nombreAlumno = document.createAttribute("nombre");
			nombreAlumno.setValue(stu.getNombre());
			Attr primerApellidoAlumno = document.createAttribute("primerApellido");
			primerApellidoAlumno.setValue(stu.getPrimerApellido());
			Attr segundoApellidoAlumno = document.createAttribute("segundoApellido");
			segundoApellidoAlumno.setValue(stu.getSegundoApellido());
			Attr idGeneroAlumno = document.createAttribute("idGenero");
			idGeneroAlumno.setValue(stu.getSexo().equalsIgnoreCase("M")?"250":"251");
			Attr fechaNacimientoAlumno = document.createAttribute("fechaNacimiento");
			fechaNacimientoAlumno.setValue("DESDE AQUI SACAMOS LA FECHA DE NACIMIENTO:"+ stu.getCurp());
			String curp =stu.getCurp();
			int anyo = Integer.parseInt(curp.substring(4, 6))+1900;
			if( anyo < 1950 ) anyo += 100;
			String fechaNac=anyo+"-"+curp.substring(6,8)+"-"+curp.substring(8,10);
			
			fechaNacimientoAlumno.setValue(fechaNac+"T00:00:00"); 
			Attr fotoAlumno = document.createAttribute("foto");
			fotoAlumno.setValue("ESTE ES CAMPO OPCIONAL - Atributo que contiene el valor de la digestión aplicada al archivo de la foto del alumno.");
			fotoAlumno.setValue("");
			Attr firmaAutografaAlumno = document.createAttribute("firmaAutografa");
			firmaAutografaAlumno.setValue("ESTE ES CAMPO OPCIONAL - Atributo que contiene el valor de la digestión aplicada al archivo de la firma autógrafa del alumno.");
			firmaAutografaAlumno.setValue("");
			
			
			Alumno.setAttributeNode(numeroControl);
			Alumno.setAttributeNode(curpAlumno);
			Alumno.setAttributeNode(nombreAlumno);
			Alumno.setAttributeNode(primerApellidoAlumno);
			Alumno.setAttributeNode(segundoApellidoAlumno);
			Alumno.setAttributeNode(idGeneroAlumno);
			Alumno.setAttributeNode(fechaNacimientoAlumno);
//			Alumno.setAttributeNode(fotoAlumno);
//			Alumno.setAttributeNode(firmaAutografaAlumno);
//			
			root.appendChild(Alumno);
			Element Expedicion = document.createElement("Expedicion");
			
			Attr idTipoCertificacion = document.createAttribute("idTipoCertificacion");
			idTipoCertificacion.setValue(certif.getTipo_certif().equalsIgnoreCase("T")?"79":"80");
			Attr tipoCertificacion = document.createAttribute("tipoCertificacion");
			tipoCertificacion.setValue(certif.getTipo_certif().equalsIgnoreCase("T")?"TOTAL":"PARCIAL");
			Attr fecha = document.createAttribute("fecha");
			fecha.setValue(df.format(certif.getFecha_expedicion())+"T00:00:00");
			Attr idLugarExpedicion = document.createAttribute("idLugarExpedicion");
			idLugarExpedicion.setValue(String.valueOf(sch.getId_entidad_federativa()));
			Attr lugarExpedicion = document.createAttribute("lugarExpedicion");
			lugarExpedicion.setValue(ent.getValue());
			
			
			
			Expedicion.setAttributeNode(idTipoCertificacion);
			Expedicion.setAttributeNode(tipoCertificacion);
			Expedicion.setAttributeNode(fecha);
			Expedicion.setAttributeNode(idLugarExpedicion);
			Expedicion.setAttributeNode(lugarExpedicion);
			
			
			root.appendChild(Expedicion);
			Element Asignaturas = document.createElement("Asignaturas");
			Double credAcum=(double) 0;
			Double sumCalif=(double) 0;
			for (Object resCalif : resultadosCalif) {
				VistaCalificacionesCertificado calif = (VistaCalificacionesCertificado) resCalif; 
				Element Asignatura = document.createElement("Asignatura");
				Attr idAsignatura = document.createAttribute("idAsignatura");
				idAsignatura.setValue(String.valueOf(calif.getId_asignatura_mec()));
				Attr claveAsignatura = document.createAttribute("claveAsignatura");
				claveAsignatura.setValue(calif.getClave_asignatura());
				Attr nombreAsignatura = document.createAttribute("nombre");
				nombreAsignatura.setValue(calif.getDesc_asignatura());
				Attr cicloAsignatura = document.createAttribute("ciclo");
				cicloAsignatura.setValue(calif.getCiclo());
				
				Attr calificacionAsignatura = document.createAttribute("calificacion");
				calificacionAsignatura.setValue(Double.compare( new Double(calif.getCalificacion()),  new Double(10.0)) == 0?String.format("%.0f",calif.getCalificacion()):String.format("%.2f",calif.getCalificacion()));
				sumCalif+=calif.getCalificacion();
				Attr idObservacionesAsignatura = document.createAttribute("idObservaciones");
				idObservacionesAsignatura.setValue(String.valueOf(calif.getId_observacion()));
				Attr observacionesAsignatura = document.createAttribute("observaciones");
				observacionesAsignatura.setValue(String.valueOf(calif.getDesc_observacion()));	
				
				Attr creditos = document.createAttribute("creditos");
				creditos.setValue(String.format("%.2f", ( (double) calif.getCreditos())));
				Attr idTipoAsignatura = document.createAttribute("idTipoAsignatura");
				idTipoAsignatura.setValue(calif.getTipo_asignatura().startsWith("OBLIG")?"263":"264");
				credAcum+=calif.getCreditos();
				Asignatura.setAttributeNode(creditos);
				Asignatura.setAttributeNode(claveAsignatura);
				Asignatura.setAttributeNode(idAsignatura);
				Asignatura.setAttributeNode(idTipoAsignatura);
				Asignatura.setAttributeNode(nombreAsignatura);
				Asignatura.setAttributeNode(cicloAsignatura);				
				Asignatura.setAttributeNode(calificacionAsignatura);
				Asignatura.setAttributeNode(idObservacionesAsignatura);
				Asignatura.setAttributeNode(observacionesAsignatura);
				logger.info("Por agregar la asignatura: {}" + calif.toString());
				Asignaturas.appendChild(Asignatura);
			}
			Double prom = sumCalif / resultadosCalif.size();
			
			Attr totalAsignaturas = document.createAttribute("total");
			totalAsignaturas.setValue(String.valueOf(certif.getTotal_materias()));
			
			Attr asignaturasAsignadas = document.createAttribute("asignadas");
			asignaturasAsignadas.setValue(String.valueOf(certif.getTotal_asignadas()));
			NumberFormat nf = DecimalFormat.getInstance();
			nf.setMinimumFractionDigits(2);
			nf.setMaximumFractionDigits(2);
			nf.setRoundingMode(RoundingMode.DOWN);
			Attr promedio = document.createAttribute("promedio");
			promedio.setValue(nf.format(prom));//String.format("%.2f",prom));//String.valueOf(certif.getPromedio()));
			
			
			Attr totalCreditos = document.createAttribute("totalCreditos");
			totalCreditos.setValue(String.format("%.2f", (certif.getTotal_creditos().doubleValue())));
			Attr numeroCiclos = document.createAttribute("numeroCiclos");
			numeroCiclos.setValue(String.valueOf(certif.getNumero_ciclos()));
			
			Attr creditosObtenidos = document.createAttribute("creditosObtenidos");
			creditosObtenidos.setValue(String.format("%.2f", ( (double)credAcum)));
			
			
			Asignaturas.setAttributeNode(totalCreditos);
			Asignaturas.setAttributeNode(numeroCiclos);
			Asignaturas.setAttributeNode(creditosObtenidos);
			Asignaturas.setAttributeNode(totalAsignaturas);
			Asignaturas.setAttributeNode(asignaturasAsignadas);
			Asignaturas.setAttributeNode(promedio);
			root.appendChild(Asignaturas);
			/**
			 * Obtención de la cadena original:
			 * 
			 * 
			*/
			
			String co;
			co = "||";
			co += version.getValue() + "|";
			co += tipoCertificado.getValue() + "|";
			co += idNombreInstitucion.getValue() + "|";
			co += idCampus.getValue() + "|";
			co += idEntidadFederativa.getValue() + "|";
			co += curpResponsable.getValue()+ "|";
			co += idCargoResponsable.getValue() + "|";
			co += numero.getValue() + "|";
			co += fechaExpedicion.getValue() + "|";
			co += idCarrera.getValue() + "|";
			co += idTipoPeriodo.getValue() + "|";
			co += clavePlan.getValue() + "|";
			co +=  idNivelEstudios.getValue()+"|"; 
			co += calificacionMinima.getValue() + "|";
			co += calificacionMaxima.getValue() + "|";
			co += calificacionMinimaAprobatoria.getValue() + "|";
			co += numeroControl.getValue() + "|";
			co += curpAlumno.getValue() + "|";
			co += nombreAlumno.getValue() + "|";
			co += primerApellidoAlumno.getValue() + "|";
			co += segundoApellidoAlumno.getValue() + "|";
			co += idGeneroAlumno.getValue() + "|";
			co += fechaNacimientoAlumno.getValue() + "|";
			//co += fotoAlumno.getValue() + "|";
			//co += firmaAutografaAlumno.getValue() + "|";
			
			co += idTipoCertificacion.getValue() + "|";
			co += fecha.getValue() + "|";
			co += idLugarExpedicion.getValue() + "|";
			co += totalAsignaturas.getValue() + "|";
			co += asignaturasAsignadas.getValue() + "|";
			co += nf.format(prom) + "|";
			co += String.format("%.2f", ( certif.getTotal_creditos().doubleValue()))+ "|";
			co += String.format("%.2f", ( (double)credAcum)) + "|";
			co += numeroCiclos.getValue() + "|";
			for (Object resCalif : resultadosCalif) {
				VistaCalificacionesCertificado calif = (VistaCalificacionesCertificado) resCalif; 
				co += calif.getId_asignatura_mec() + "|";
				co += calif.getCiclo() + "|";
				co += (Double.compare( new Double(calif.getCalificacion()),  new Double(10.0)) == 0?String.format("%.0f",calif.getCalificacion()):String.format("%.2f",calif.getCalificacion())) + "|"; //AQUI HABIA FALTADO EL PIPE AL CONCATENAR EL 10.
				co +=  (calif.getTipo_asignatura().startsWith("OBLIG")?"263":"264")+"|";
				co += String.format("%.2f",( (double) calif.getCreditos())) + "|";			
				
			}
			
			
		

			
			co += "|";
			
			// ATENCION AQUI RUTINA PARA OBTENER EL SELLO:
			System.out.println("Cadena original" + co);
			// String keyPath = firm.getKeyFile();
			String password = StringUtils.desencriptar(firmante.getPwdCertificado(), firmante.getNoCertificadoResponsable());

			// final PKCS8Key pkcs8Key = new PKCS8Key(toByteArray(keyPath),
			// password.toCharArray());
			final PKCS8Key pkcs8Key = new PKCS8Key(Base64.decodeBase64(firmante.getKeyFile()), password.toCharArray());

			final PrivateKey privateKey = pkcs8Key.getPrivateKey();

			final Signature signature = Signature.getInstance("SHA256withRSA");
			signature.initSign(privateKey);
			signature.update(co.getBytes("UTF-8"));

			sello.setValue(Base64.encodeBase64String(signature.sign()));

			root.setAttributeNode(sello);		
		
			
			// create the xml file
			// transform the DOM Object to an XML File
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			DOMSource domSource = new DOMSource(document);
			StreamResult streamResult = new StreamResult(new File(xmlFilePath));
			transformer.transform(domSource, streamResult);
			// EN ESTE MOMENTO YA TENEMOS EL XML COMPLETO, POR LO TANTO HAY QUE
			byte[] fileContent = FileUtils.readFileToByteArray(new File(xmlFilePath));
			String encodedString = Base64.encodeBase64String(fileContent);
			// le actualizamos al registro del certificado algunos datos:
			certif.setFolioControl(folioControlStr);
			catDao.updateRecord(certif);

			response.setMessage("XML del certificado generado");
			response.setStatus(true);
			response.setErrorCode(co);
			response.setData(encodedString);
			response.setToken(jwtutil.refreshToken(token));
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
}
