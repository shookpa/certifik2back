package com.ibm.services;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.core.Response;

import com.lambdaworks.crypto.SCryptUtil;

public interface SecurityService {
	
	public Response login(String json);
	public Response setGoogleCode(@HeaderParam("token") String token, String json);
	public Response searchMenuItems(@HeaderParam("token") String token);
	public Response updateToken(@HeaderParam("token") String token);
	public Response getLicense(@HeaderParam("token") String token);
	public Response getLicenses(@HeaderParam("token") String token);
	public Response setLicense(@HeaderParam("token") String token, String json);
	
	
	
	public Response deleteLicense(@HeaderParam("token") String token, int id);
	public Response updateLicense(@HeaderParam("token") String token, int id, String json);
}
