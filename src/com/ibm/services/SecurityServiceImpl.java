package com.ibm.services;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.zxing.WriterException;
import com.ibm.dao.CatalogsDao;
import com.ibm.dao.LicenseDao;
import com.ibm.dao.LoginDao;
import com.ibm.dao.RolMenuDao;
import com.ibm.dao.RolOperationDao;
import com.ibm.dao.UserDao;
import com.ibm.models.GenericResponse;
import com.ibm.models.License;
import com.ibm.models.RolMenu;
import com.ibm.models.User;
import com.ibm.models.UserSchool;
import com.ibm.models.UserTwoFactor;
import com.ibm.util.GoogleAuthenticator;
import com.ibm.util.JWTUtil;
import com.lambdaworks.crypto.SCryptUtil;

@Path("/rest/security")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SecurityServiceImpl implements SecurityService {

	@Override
	@POST
	@Path("/login")
	public Response login(String json) {

		GenericResponse response = new GenericResponse();
		if (json.trim().equalsIgnoreCase("")) {
			response.setStatus(false);
			response.setMessage("Faltan criterios de busqueda");
			response.setErrorCode("Codigo pendiente");
			return Response.status(404).entity(response).build();
		}
		LoginDao loginDao = new LoginDao();
		CatalogsDao catDao = new CatalogsDao();
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;
		if (jo.get("user").toString().trim().equalsIgnoreCase("")
				|| jo.get("password").toString().trim().equalsIgnoreCase("")) {
			response.setStatus(false);
			response.setMessage("Debe capturar su usuario y contraseña");
			response.setErrorCode("Codigo pendiente");
			return Response.status(404).entity(response).build();
		}
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("user", jo.get("user")); // al filtro en lugar de mandarle
											// todos los datos del json que me
											// envian, solo le estoy enviando el
											// user ok

		User us = loginDao.searchUser(filtro);
		if (us == null) {
			response.setStatus(false);
			response.setMessage("Verifique su usuario y contraseña");
			response.setErrorCode("Codigo pendiente");
			return Response.status(401).entity(response).build();
		}
		System.out.println("Veamos el usuario encontrado:" + us.toString());

		boolean matched = SCryptUtil.check(jo.get("password").toString(), us.getPassword());
		if (matched == false) {
			response.setStatus(false);
			response.setMessage("Verifique su usuario y contraseña");
			response.setErrorCode("Codigo pendiente");
			return Response.status(401).entity(response).build();
		}
		System.out.println("el password es correcto?" + matched);
		JWTUtil util = new JWTUtil();
		// int rol = util.getRolFromToken(jo.get("rol").toString());

		String token = util.generateToken(jo.get("user").toString(), us.getRol(), us.getId_usuario(),
				us.getNombre() + " " + us.getA_paterno() + " " + us.getA_materno());
		System.out.println("TOKEN: " + token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();
		filtroRol.put("id_rol", us.getRol());
		UserTwoFactor twoF = loginDao.getUserTwoFactor(us.getId_usuario());
		String qrFilePath = "/opt/tomcat/certif/tempQR/qr_" + us.getId_usuario() + ".png";
//		qrFilePath = "/home/jcenteno/Dropbox/soft-webcosmos/CC-DATWEB/DGP_SEP/QRCodesExample/qr_" + us.getId_usuario() + ".png";
		Boolean nuevoQR = false;
		// EN EL CASO DE QUE NO TENGA DATOS DE LA DOBLE AUTENTICACIÓN:
		if (twoF.getTwoFactor() == 0) {
			// TENEMOS QUE SETEAR SU CLAVE, Y GENERARLE UNA IMAGEN QRCODE:
			String sk = GoogleAuthenticator.generateSecretKey();

			twoF.setSecretKey(sk);
			
			catDao.updateRecord(twoF);
			String barCodeUrl = GoogleAuthenticator.getGoogleAuthenticatorBarCode(sk, us.getEmail(),
					"Certifik2-Online");
			System.out.println(barCodeUrl);

			nuevoQR = true;
			try {
				GoogleAuthenticator.createQRCode(barCodeUrl, qrFilePath, 300, 300);
			} catch (WriterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		Map<String, Object> respuestLogin = new HashMap<String, Object>();
		respuestLogin.put("usuario", us.getNombre() + " " + us.getA_paterno() + " " + us.getA_materno());
		if (nuevoQR) {
			byte[] fileContent;
			String encodedString = "";
			try {
				fileContent = FileUtils.readFileToByteArray(new File(qrFilePath));
				encodedString = Base64.getEncoder().encodeToString(fileContent);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			respuestLogin.put("qrCode", encodedString);
		}

		response.setData(respuestLogin);
		response.setMessage("Acceso autorizado");
		response.setStatus(true);
		response.setToken(token);

		return Response.ok(response).build();
		// return Response.status(200).entity(response).build();

	}

	@Override
	@POST
	@Path("/2fa")
	public Response setGoogleCode(@HeaderParam("token") String token, String json) {
		JWTUtil jwtutil = new JWTUtil();
		GenericResponse response = new GenericResponse();
		if (json.trim().equalsIgnoreCase("")) {
			response.setStatus(false);
			response.setMessage("Faltan criterios de busqueda");
			response.setErrorCode("Codigo pendiente");
			return Response.status(404).entity(response).build();
		}
		LoginDao loginDao = new LoginDao();
		CatalogsDao catDao = new CatalogsDao();
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String tokenDato = "";
		

		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idUsuario = jwtutil.getIdUsuarioFromToken(token);
		User us = loginDao.getUser(idUsuario);
		UserTwoFactor twoF = loginDao.getUserTwoFactor(idUsuario);
		JSONObject jo = (JSONObject) obj;
		if (jo.get("googleCode").toString().trim().equalsIgnoreCase("")) {
			response.setStatus(false);
			response.setMessage("Debe capturar el codigo de google");
			response.setErrorCode("Codigo pendiente");
			return Response.status(404).entity(response).build();
		}
		Integer gooCode=Integer.parseInt(jo.get("googleCode").toString().trim());
		Integer codigoGenerado=Integer.parseInt(GoogleAuthenticator.getTOTPCode(twoF.getSecretKey()));
		
//		System.out.println("Veamos el código generado: "+codigoGenerado+", ahora el código que estamos enviando:"+gooCode);
		
		
		if (gooCode.intValue()!=codigoGenerado.intValue()) {
			response.setStatus(false);
			response.setMessage("El código de google es incorrecto");			
			return Response.status(404).entity(response).build();
		}
		
		
		
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();
		filtroRol.put("id_rol", us.getRol());
		
		Map<String, Object> respuestLogin = new HashMap<String, Object>();
		respuestLogin.put("usuario", us.getNombre() + " " + us.getA_paterno() + " " + us.getA_materno());
		respuestLogin.put("permisos", rod.searchRolOperation(filtroRol));
		twoF.setTwoFactor(1);
		catDao.updateRecord(twoF);
		response.setData(respuestLogin);
		response.setMessage("Acceso autorizado");
		response.setStatus(true);
		response.setToken(token);

		return Response.ok(response).build();
		// return Response.status(200).entity(response).build();

	}

	@Override
	@GET
	@Path("/license")
	public Response getLicense(@HeaderParam("token") String token) {
		GenericResponse response = new GenericResponse();
		JWTUtil jwtutil = new JWTUtil();
		String tokenDato = "";
		if (token == null) {
			System.out.println("Veamos, parece que el token no llega:" + token);
			response.setData("");
			response.setMessage("NO LEESTA LLEGANDO EL TOKEN AL SERVER");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			try {
				tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return Response.ok(tokenDato).build();
			// return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idUsuario = jwtutil.getIdUsuarioFromToken(token);

		LicenseDao dao = new LicenseDao();
		UserDao usdao = new UserDao();
		User us = usdao.getUser(idUsuario);

		try {

			License lic = dao.getLicense(us.getId_licencia());
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			lic.setFecha_expiracionStr(df.format(lic.getFecha_expiracion()));
			response.setData(lic);
			response.setMessage("Valores de la licencia del usuario logueado");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@GET
	@Path("/licenses")
	public Response getLicenses(@HeaderParam("token") String token) {

		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("id_rol", rol);
		filtro.put("id_permiso", 11);// Conforme al catalogo de permisos y
										// operaciones
		filtro.put("per_modulo", 1);
		// filtro.put("per_func1", 1); //CONFORME AL ID DE COLUMNA QUE
		// CORRESPONDA EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtro).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();

		LicenseDao dao = new LicenseDao();

		String tokenDato = "";
		try {
			GenericResponse response = new GenericResponse();
			License[] lic = dao.getAllLicenses();

			response.setData(lic);
			response.setMessage("Lista de licencias");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@POST
	@Path("/licenses")
	public Response setLicense(@HeaderParam("token") String token, String json) {

		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		// // IDENTIFICACION DEL ROL
		// int rol = jwtutil.getRolFromToken(token);
		// RolOperationDao rod = new RolOperationDao();
		// Map<String, Object> filtroRol = new HashMap<String, Object>();
		// filtroRol.put("id_rol", rol);
		// filtroRol.put("id_operation", 2); // Conforme al catalogo de
		// operaciones
		// if (rod.searchRolOperation(filtroRol).length == 0)
		// return Response.status(Response.Status.UNAUTHORIZED).build();
		// // FIN DE IDENTIFICACION DE ROL

		GenericResponse response = new GenericResponse();
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Faltan criterios de busqueda");
			response.setErrorCode("Codigo pendiente");
			return Response.status(404).entity(response).build();
		}
		LicenseDao dao = new LicenseDao();

		String strJson = "";
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// JSONObject jo = (JSONObject) obj;
		org.json.JSONObject jo = new org.json.JSONObject(json);

		/**
		 * Comienzo de la seccion de busqueda de duplicados con el codigo de
		 * licencia:
		 */
		System.out.println("el json enviado es " + json);
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("codigo_licencia", jo.get("codigo_licencia")); // MISMO CASO
																	// SOLAMENTE
																	// BUSCAMOS
																	// EN
		// BASE AL USER
		License[] li = dao.searchLicense(filtro); // EL RESULTADO DE LA BUSQUEDA
													// ESTA EN ESE ARRAY

		if (li.length > 0) {
			response.setToken(jwtutil.refreshToken(token));
			response.setStatus(false);
			response.setMessage("Ya existe una licencia con ese codigo");
			response.setErrorCode("EC-02");
			return Response.status(422).entity(response).build();
		}
		/**
		 * Fin de la seccion de busqueda de duplicados del campo codigo de
		 * licencia
		 */
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			License lic = new License();
			lic.setCodigo_licencia(jo.get("codigo_licencia").toString());
			lic.setFecha_creacion(jo.get("fecha_creacion").toString().trim().equals("") ? null
					: df.parse(jo.get("fecha_creacion").toString()));
			lic.setFecha_expiracion(jo.get("fecha_expiracion").toString().trim().equals("") ? null
					: df.parse(jo.get("fecha_expiracion").toString()));
			lic.setTimbres_disponibles(Integer.parseInt(jo.get("timbres_disponibles").toString()));
			lic.setTimbres_usados(0);
			dao.setLicense(lic);
			response.setMessage("Licencia creada exitosamente");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@DELETE
	@Path("/licenses/{id}")
	public Response deleteLicense(@HeaderParam("token") String token, @PathParam("id") int id) {
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("id_rol", rol);
		filtro.put("id_permiso", 11);// Conforme al catalogo de permisos y
										// operaciones
		filtro.put("per_modulo", 1);
		filtro.put("per_func3", 1); // CONFORME AL ID DE COLUMNA QUE
		// CORRESPONDA EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtro).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		System.out.println("el id recibido: " + id);
		GenericResponse response = new GenericResponse();
		License lic = new License();

		LicenseDao dao = new LicenseDao();
		lic = dao.getLicense(id);

		if (lic == null) {
			response.setData(null);

			response.setToken(jwtutil.refreshToken(token));
			response.setStatus(false);
			response.setMessage("EL id de licencia no existe");
			response.setErrorCode("EC-02");
			return Response.status(404).entity(response).build();
		}
		dao.deleteLicense(lic);
		response.setStatus(true);
		response.setMessage("Licencia eliminada satisfactoriamente");
		response.setToken(jwtutil.refreshToken(token));
		return Response.status(200).entity(response).build();
	}

	@Override
	@PUT
	@Path("/licenses/{id}")
	public Response updateLicense(@HeaderParam("token") String token, @PathParam("id") int id, String json) {

		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("id_rol", rol);
		filtro.put("id_permiso", 11);// Conforme al catalogo de permisos y
										// operaciones
		filtro.put("per_modulo", 1);
		filtro.put("per_func2", 1); // CONFORME AL ID DE COLUMNA QUE
		// CORRESPONDA EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtro).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		GenericResponse response = new GenericResponse();
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("No se ingresaron todos los campos");
			response.setErrorCode("Codigo pendiente");
			return Response.status(404).entity(response).build();
		}
		LicenseDao dao = new LicenseDao();

		String strJson = "";
		JSONParser parser = new JSONParser();
		Object obj = null;

		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;
		org.json.JSONObject jo2 = new org.json.JSONObject(json);

		if (dao.getLicense(id) == null) {
			response.setStatus(false);
			response.setMessage("La licencia no existe");
			response.setErrorCode("EC-02");
			return Response.status(404).entity(response).build();
		}
		String opcion = "";
		boolean op = true;

		try {
			License lic = dao.getLicense(id);
			Iterator entries = jo.entrySet().iterator();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			while (entries.hasNext()) {
				Map.Entry entry = (Map.Entry) entries.next();

				try {
					if (entry.getKey().toString().equalsIgnoreCase("fecha_creacion")
							|| entry.getKey().toString().equalsIgnoreCase("fecha_expiracion"))
						PropertyUtils.setSimpleProperty(lic, entry.getKey().toString(),
								df.parse(entry.getValue().toString()));
					else if (entry.getKey().toString().equalsIgnoreCase("timbres_disponibles"))
						PropertyUtils.setSimpleProperty(lic, entry.getKey().toString(),
								Integer.parseInt(entry.getValue().toString()));
					else
						PropertyUtils.setSimpleProperty(lic, entry.getKey().toString(), entry.getValue());
				} catch (Exception e) {
					System.out.println("CAYO EN EXCEPCION: " + entry.getKey().toString() + " -- " + entry.getValue());
					e.printStackTrace();
				}
			}
			System.out.println("Veamos la licencia a punto de actualizar:" + lic);
			dao.updateLicense(lic);

			response.setMessage("Licencia actualizada exitosamente");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));

			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);

		} catch (Exception e) {
			System.out.println(e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(strJson).build();

	}

	@Override
	@GET
	@Path("/menuitems")
	public Response searchMenuItems(@HeaderParam("token") String token) {
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int rol = jwtutil.getRolFromToken(token);

		RolMenuDao rolMenuDao = new RolMenuDao();
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("id_rol", rol); // al filtro en lugar de mandarle
									// todos los datos del json que me
									// envian, solo le estoy enviando el
									// user ok

		String tokenDato = "";
		try {
			GenericResponse response = new GenericResponse();
			RolMenu[] rolMenu = rolMenuDao.searchRolMenu(filtro);
			Map[] map = buildMenu(rolMenu, null);
			response.setData(map);
			response.setMessage("Lista de menus permitidos del usuario");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	public RolMenu[] listaHijos(RolMenu[] rolMenu, Integer padre) {
		int contHijos = 0;
		for (int i = 0; i < rolMenu.length; i++) {
			if (rolMenu[i].id_padre != null && rolMenu[i].id_padre == padre)
				contHijos++;
		}
		RolMenu[] rolMenuHijos = new RolMenu[contHijos];
		int j = 0;
		for (int i = 0; i < rolMenu.length; i++) {
			if (rolMenu[i].id_padre != null && rolMenu[i].id_padre == padre) {
				rolMenuHijos[j] = rolMenu[i];
				j++;
			}
		}
		return rolMenuHijos;
	}

	public Map[] buildMenu(RolMenu[] rolMenu, Integer padre) {
		int contPadres = 0;
		for (int i = 0; i < rolMenu.length; i++) {
			if (rolMenu[i].id_padre == padre)
				contPadres++;
		}
		Map[] map = new Map[contPadres];
		int j = 0;
		for (RolMenu rolMenu2 : rolMenu) {
			Map<String, Object> aMap = new HashMap<String, Object>();
			if (rolMenu2.id_padre == padre) {
				aMap.put("id_menu", Integer.toString(rolMenu2.id_menu));
				aMap.put("id_rol", Integer.toString(rolMenu2.id_rol));
				aMap.put("id_padre", null);
				aMap.put("nombre_menu", rolMenu2.nombre_menu);
				aMap.put("class_i", rolMenu2.class_i);
				aMap.put("class_s", rolMenu2.class_s);
				aMap.put("href", rolMenu2.href);
				aMap.put("nivel", rolMenu2.nivel);
				RolMenu[] hijos = listaHijos(rolMenu, rolMenu2.id_menu);
				if (hijos.length > 0) {
					aMap.put("children", buildMenu(rolMenu, rolMenu2.id_menu));
				}
				map[j] = aMap;
				j++;
			}
		}
		return map;
	}

	@Override
	@GET
	@Path("/updateToken")
	public Response updateToken(@HeaderParam("token") String token) {
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		GenericResponse response = new GenericResponse();
		response.setData(null);
		response.setMessage("Actualización de token exitosa");
		response.setStatus(true);
		response.setToken(jwtutil.refreshToken(token));
		String tokenDato = "";
		try {
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(tokenDato).build();
	}

}
