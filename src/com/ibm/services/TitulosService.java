package com.ibm.services;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.core.Response;

public interface TitulosService {
	
	
	public Response setTitulo(@HeaderParam("token") String token, String json);
	public Response setTitulos(@HeaderParam("token") String token, String json);
	public Response updateTitulo(@HeaderParam("token") String token,Integer id_titulo, String json);
	public Response getTitulos(@HeaderParam("token") String token, String json, int limit, int offset);
	public Response sendTitulo(@HeaderParam("token") String token, Integer id);
	public Response verifyStatusTitulo(@HeaderParam("token") String token, Integer id);
	public Response getTitulo(@HeaderParam("token") String token, Integer id);
	public Response getInformeSepTitulo(@HeaderParam("token") String token, Integer id);
	public Response cancelTitulo(@HeaderParam("token") String token, Integer id_titulo, String json);
	public Response getXML(@HeaderParam("token") String token, Integer id);
	public Response sendTitulos(@HeaderParam("token") String token, String json);
	
}
