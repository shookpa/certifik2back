package com.ibm.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.security.PrivateKey;
import java.security.Signature;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.ssl.PKCS8Key;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.apache.log4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ibm.dao.CatalogsDao;
import com.ibm.dao.LicenseDao;
import com.ibm.dao.RolOperationDao;
import com.ibm.dao.TituloDao;
import com.ibm.dao.UserDao;
import com.ibm.models.AutorizacionReconocimientoShort;
import com.ibm.models.Carrera;
import com.ibm.models.Configurations;
import com.ibm.models.EntidadFederativaShort;
import com.ibm.models.Firmante;
import com.ibm.models.FundamentoLegalServicioSocialShort;
import com.ibm.models.GenericResponse;
import com.ibm.models.License;
import com.ibm.models.ModalidadTitulacionShort;
import com.ibm.models.School;
import com.ibm.models.Student;
import com.ibm.models.TipoEstudioAntecedenteShort;
import com.ibm.models.Titulo;
import com.ibm.models.TituloFirmantes;
import com.ibm.models.User;
import com.ibm.models.VistaTitulo;
import com.ibm.models.VistaTituloFirmante;
import com.ibm.util.JWTUtil;
import com.ibm.util.StringUtils;

import mx.sep.mec.web.ws.schemas.AutenticacionType;
import mx.sep.mec.web.ws.schemas.CancelaTituloElectronicoRequest;
import mx.sep.mec.web.ws.schemas.CancelaTituloElectronicoResponse;
import mx.sep.mec.web.ws.schemas.CargaTituloElectronicoRequest;
import mx.sep.mec.web.ws.schemas.CargaTituloElectronicoResponse;
import mx.sep.mec.web.ws.schemas.ConsultaProcesoTituloElectronicoRequest;
import mx.sep.mec.web.ws.schemas.ConsultaProcesoTituloElectronicoResponse;
import mx.sep.mec.web.ws.schemas.DescargaTituloElectronicoRequest;
import mx.sep.mec.web.ws.schemas.DescargaTituloElectronicoResponse;
import mx.sep.mec.web.ws.schemas.TitulosPortTypeSoap11Stub;

@Path("/rest")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TitulosServiceImpl implements TitulosService {
	private static final Logger logger = Logger.getLogger(TitulosServiceImpl.class);
	
	@Override
	@POST
	@Path("/titulos/limit/{limit}/offset/{offset}")
	public Response getTitulos(@HeaderParam("token") String token, String json, @PathParam("limit") int limit,
			@PathParam("offset") int offset) {
		logger.info("Inside getTitulos...");
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		//
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}

		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;
		String tokenDato = "";
		try {
			Map<String, Object> filtro = new HashMap<String, Object>();
			String id_carrera = jo.get("id_carrera").toString().trim();
			String matricula = jo.get("matricula").toString().trim();

			if (!id_carrera.equalsIgnoreCase(""))
				filtro.put("id_carrera", id_carrera);

			if (!matricula.equalsIgnoreCase(""))
				filtro.put("matricula", matricula);

			filtro.put("id_institucion", idInst);
			TituloDao dao = new TituloDao();
			Map<String, Object> responseData = new HashMap<String, Object>();
			responseData.put("countAll", dao.listTitulos(new Titulo(), filtro).size());
			limit=5000;
			offset=0;
			responseData.put("pageResults", dao.listTitulos(new VistaTitulo(), filtro, limit, offset));
			response.setData(responseData);
			response.setMessage("Lista Titulos");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@POST
	@Path("/titulos")
	public Response setTitulos(@HeaderParam("token") String token, String json) {
		logger.info("Inside setTitulos...");
		GenericResponse response = new GenericResponse();
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
//			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		UserDao usDao = new UserDao();
		// EN EL CASO DE QUE YA SE ENCUENTRE EN UN AMBIENTE DE PRODUCCION:

		// VERIFICAR SI AUN TIENE TIMBRES DISPONIBLES

		// VERIFICAR SI SU LICENCIA ES VIGENTE

		// TERMINA VERIFICACION AMBIENTE PRODUCCION

		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 7);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
//		if (rod.searchRolOperation(filtroRol).length == 0)
//			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
	
		org.json.JSONObject jo = new org.json.JSONObject(json);
		String tokenDato = "";
		boolean valido=true;
		try {
			TituloDao dao = new TituloDao();
			Map<Integer, String> respuesta = new HashMap<Integer, String>();
			String base64 = jo.get("archivo").toString();
			String nomArchivo = jo.get("nombre_archivo").toString();
			File excelFile = new File("webapps/tmp/" + nomArchivo);

			byte[] bytes = Base64.decodeBase64(base64);
			FileUtils.writeByteArrayToFile(excelFile, bytes);
			FileInputStream fis;
			
			fis = new FileInputStream(excelFile);
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			// we get first sheet
			XSSFSheet sheet = workbook.getSheetAt(0);

			// FALTA LA VALIDACION QUE EL ID DE CARRERA CORRESPONDA AL NOMBRE DE
			// CARRERA
			// FALTA LA VALIDACION QUE LA CARRERA CORRESPONDA A LA INSTITUCION

			CatalogsDao catDao = new CatalogsDao();
			
			int numBuenos = 0;
			int numMalos = 0;
			for (int i = 1; i < sheet.getLastRowNum(); i++) {
				valido=true;
				if (sheet.getRow(i).getCell(0) == null) {
					break;
				}
				Map<String, Object> filtro = new HashMap<String, Object>();

				String matricula = "";

				try {
					matricula = sheet.getRow(i).getCell(0).getStringCellValue().trim().length() == 0 ? ""
							: sheet.getRow(i).getCell(0).getStringCellValue();

				} catch (Exception e) {
					matricula = String.valueOf(Math.round( sheet.getRow(i).getCell(0).getNumericCellValue())).trim();
				}
				if(matricula.trim().equalsIgnoreCase("")) 
					break;
				if (!matricula.trim().equalsIgnoreCase("")) {
					filtro = new HashMap<String, Object>();
					filtro.put("matricula", matricula);
					filtro.put("id_institucion", idInst);
					List resultados = catDao.searchCatalog(new Student(), filtro);
					System.out.println("veamos el numero de resultados de Student:" + resultados.size());
					if (resultados.size() > 0) {
						
						Titulo tit = new Titulo();

						tit.setId_institucion(idInst);

						// HAY QUE BUSCAR EL ID DE CARRERA:

						Map<String, Object> filtroCarrera = new HashMap<String, Object>();
						try {
							filtroCarrera.put("cve_carrera", sheet.getRow(i).getCell(4).getStringCellValue());
						} catch (Exception e) {
							filtroCarrera.put("cve_carrera",
									new Double(sheet.getRow(i).getCell(4).getNumericCellValue()).intValue());
						}
						filtroCarrera.put("id_institucion", idInst);
						System.out.println("veamos el fitro de la carrera:" + filtroCarrera.get("cve_carrera"));
						List<Object> resultadosCarreras = catDao.searchCatalog(new Carrera(), filtroCarrera);
						System.out.println("veamos el numero de resultados de Carreras:" + resultadosCarreras.size());

						if (resultadosCarreras.size() > 0) {
							Carrera car = ((Carrera) resultadosCarreras.get(0));
							// DEBEMOS VERIFICAR QUE NO SE DUPLIQUE, usando
							// matricula e id_ carrera:
							filtro = new HashMap<String, Object>();
							filtro.put("matricula", matricula);
							filtro.put("id_institucion", idInst);
							filtro.put("id_carrera", car.getId_carrera());
							filtro.put("carrera_fechaInicio", sheet.getRow(i).getCell(5).getDateCellValue());
							List resTitulos = catDao.searchCatalog(new Titulo(), filtro);
							if (resTitulos.size() <= 0) {
								tit.setId_carrera(car.getId_carrera());
								tit.setMatricula(matricula);
								tit.setCarrera_fechaInicio(sheet.getRow(i).getCell(5).getDateCellValue());
								tit.setCarrera_fechaTermino(sheet.getRow(i).getCell(6).getDateCellValue());
								try {
									// COMO ES UNA FECHA OPCIONAL, PONGAMOS EN
									// UN
									// TRY PARA VERIFICAR
									// SI VIENE
									tit.setEstudios_fechaTermino(sheet.getRow(i).getCell(7).getDateCellValue());
								} catch (Exception e) {
									tit.setEstudios_fechaTermino(null);
								}

								// al final lo de los
								// firmantes**********************************************
								tit.setExpedicion_fechaExpedicion(sheet.getRow(i).getCell(9).getDateCellValue());
								// HAY QUE BUSCAR EL ID DE MODALIDAD DE
								// TITULACION:
								Map<String, Object> filtroModalidad = new HashMap<String, Object>();
								filtroModalidad.put("value", sheet.getRow(i).getCell(10).getStringCellValue());
								List<Object> resultadosModalidad = catDao.searchCatalog(new ModalidadTitulacionShort(),
										filtroModalidad);
								tit.setExpedicion_idModalidadTitulacion(
										((ModalidadTitulacionShort) resultadosModalidad.get(0)).getKey());
								tit.setExpedicion_fechaExamenProfesional(
										sheet.getRow(i).getCell(11).getDateCellValue());
								try {
									// COMO ES UNA FECHA OPCIONAL, PONGAMOS EN
									// UN
									// TRY PARA VERIFICAR
									// SI VIENE
									tit.setExpedicion_fechaExencionExamenProfesional(
											sheet.getRow(i).getCell(12).getDateCellValue());
								} catch (Exception e) {
									tit.setExpedicion_fechaExencionExamenProfesional(null);
								}

								tit.setExpedicion_cumplioServicioSocial(
										sheet.getRow(i).getCell(13).getStringCellValue().equalsIgnoreCase("SI") ? 1
												: 0);
								Map<String, Object> filtroFundamentoLegal = new HashMap<String, Object>();
								filtroFundamentoLegal.put("value", sheet.getRow(i).getCell(14).getStringCellValue());
								List<Object> resultadosFundamentoLegal = catDao
										.searchCatalog(new FundamentoLegalServicioSocialShort(), filtroFundamentoLegal);
								tit.setExpedicion_idFundamentoLegalServicioSocial(
										((FundamentoLegalServicioSocialShort) resultadosFundamentoLegal.get(0))
												.getKey());
								tit.setExpedicion_idEntidadFederativa(
										usDao.getSchool(idInst).getId_entidad_federativa());
								try {
									tit.setAntecedente_institucionProcedencia(
											sheet.getRow(i).getCell(15).getStringCellValue().trim());
								} catch (Exception e) {
									// TODO: handle exception
									tit.setAntecedente_institucionProcedencia("");
								}
								if (tit.getAntecedente_institucionProcedencia().equalsIgnoreCase("")) {
									respuesta.put((i+1), "Falta: Institución de procedencia" );
									numMalos++;
									valido=false;
								}
								if (sheet.getRow(i).getCell(16).getStringCellValue().trim().equalsIgnoreCase("")) {
									respuesta.put((i+1), "Falta: Tipo de estudio antecedente" );
									numMalos++;
									valido=false;
								}
								else
								{	
									Map<String, Object> filtroTipoEstudioAntecedente = new HashMap<String, Object>();
									filtroTipoEstudioAntecedente.put("value",
											sheet.getRow(i).getCell(16).getStringCellValue());
									List<Object> resultadosTipoEstudioAntecedente = catDao
											.searchCatalog(new TipoEstudioAntecedenteShort(), filtroTipoEstudioAntecedente);
									tit.setAntecedente_idTipoEstudioAntecedente(
											((TipoEstudioAntecedenteShort) resultadosTipoEstudioAntecedente.get(0))
													.getKey());
								}
								//LA BUSQUEDA DE LA ENTIDAD DE LA FEDERATIVA TIENE UN PROBLEMA PORQUE ESTA ENCONTRANDO CIUDAD DE MEXICO EN LUGAR DE MEXICO.
								Map<String, Object> filtroEntidadFederativa = new HashMap<String, Object>();
								String entidadFederativaSelected = sheet.getRow(i).getCell(17).getStringCellValue().trim();
								if (entidadFederativaSelected.equalsIgnoreCase("ESTADO DE MÉXICO")) {
									entidadFederativaSelected= "MÉXICO";
								}
								if (entidadFederativaSelected.equalsIgnoreCase("")) {
									respuesta.put((i+1), "Falta: Entidad Federativa del Antecedente" );
									numMalos++;
									valido=false;
								}
								else
								{
									
									try
									{
										//HAY POSIBLIDAD DE UNA EXCEPCIÓN QUE SIGNIFICA QUE MANDO MAL EL LAYOUT EN EL CAMPO ENTIDAD FEDERATIVA
										filtroEntidadFederativa.put("value", entidadFederativaSelected);
										System.out.println("El filtro lleva:" +filtroEntidadFederativa.get("value"));
										Object resultadosEntidadFederativa = catDao
												.searchRow(new EntidadFederativaShort(), filtroEntidadFederativa);
										tit.setAntecedente_idEntidadFederativa(
											((EntidadFederativaShort) resultadosEntidadFederativa).getKey());
									}
									catch (Exception e)
									{
										response.setStatus(false);
										response.setMessage("El campo de Entidad Federativa (Antecedente) contiene valores incorrectos, favor de verificar el layout!");
										response.setErrorCode("");
										return Response.status(404).entity(response).build();
									}
								}
								
								try {
									tit.setAntecedente_noCedula(String.valueOf(new Double (sheet.getRow(i).getCell(18).getNumericCellValue()).intValue()));
								} catch (Exception e) {
									// TODO: handle exception
									tit.setAntecedente_noCedula("");
								}

								try {
									// COMO ES UNA FECHA OPCIONAL, PONGAMOS EN
									// UN
									// TRY PARA VERIFICAR
									// SI VIENE
									tit.setAntecedente_fechaInicio(sheet.getRow(i).getCell(19).getDateCellValue());
								} catch (Exception e) {
									tit.setAntecedente_fechaInicio(null);
								}
								try {
									tit.setAntecedente_fechaTerminacion(sheet.getRow(i).getCell(20).getDateCellValue());
								} catch (Exception e) {
									// TODO: handle exception
									respuesta.put((i+1), "Falta: Fecha de terminacion de antecedente" );
									numMalos++;
									valido=false;
								}
								if(!valido)
									System.out.println("veamos el titulo que no le gusto:" + tit);
								
								if (valido) {
									tit.setFecha_registro(new Date());
									tit.setFecha_modificacion(new Date());
									tit.setEstatus_titulo(1);
									tit.setDescEstatusEnvio("");
									System.out.println("veamos el titulo a punto de ser registrado:" + tit);
									dao.setTitulo(tit);
									numBuenos++;
									String firmStr = sheet.getRow(i).getCell(8).getStringCellValue();

									String[] firmArr = null;
									firmArr = firmStr.split(",");
									for (int j = 0; j < firmArr.length; j++) {

										Map<String, Object> filtroFirmante = new HashMap<String, Object>();
										filtroFirmante.put("curp", firmArr[j].trim());
										filtroFirmante.put("id_institucion", idInst);
										List<Object> resultadosFirmante = catDao.searchCatalog(new Firmante(),
												filtroFirmante);
										if (resultadosFirmante.size()>0 || firmArr[j].trim().equalsIgnoreCase("")) {
											int post_id = ((Firmante) resultadosFirmante.get(0)).getId_responsable();
											// System.out.println("Veamos las
											// instituciones"+post_id);
											TituloFirmantes titFir = new TituloFirmantes();
											titFir.setId_titulo(tit.getId_titulo());
											titFir.setId_firmante(post_id);
											dao.setTituloFirmante(titFir);
										}else
										{//SIGNIFICA QUE EL FIRMANTE NO EXISTE:
											
											respuesta.put((i+1), "Incorrecta: CURP de firmante "+firmArr[j].trim() );
											numMalos++;
										}
										
									}
								}
								
								
								
							}
							else
							{//SIGNIFICA QUE EL REGISTRO DEL TITULO YA ESTABA EN LA BASE DE DATOS:
								
								respuesta.put((i+1), "Duplicado: Titulo ya existe" );
								numMalos++;
							}

						}
						else
						{//SIGNIFICA QUE LA CARRERA NO EXISTE EN EL CATALOGO							
							respuesta.put((i+1), "Incorrecta: Clave carrera "+ filtroCarrera.get("cve_carrera") );
							numMalos++;
						}

					}
					else
					{ //SIGNIFICA QUE LA MATRICULA NO EXISTE EN EL CATALOGO
						respuesta.put((i+1), "Incorrecta: matrícula "+ matricula );
						numMalos++;						
					}

				}

			}

			workbook.close();
			fis.close();

			// filtro = new HashMap<String, Object>();
			// filtro.put("id_alumno", stu.getId_alumno());
			// resultados = dao.searchCatalog(new Calificacion(), filtro);
			response.setData(respuesta);
			response.setMessage("Se registraron exitosamente " + numBuenos + " titulos, datos erroneos: " + numMalos);
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, idInst));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

	}

	@Override
	@POST
	@Path("/titulos/add")
	public Response setTitulo(@HeaderParam("token") String token, String json) {
		logger.info("Inside setTitulo...");
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		GenericResponse response = new GenericResponse();
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Faltan los parametros del json");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 7);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
	
		org.json.JSONObject jo = new org.json.JSONObject(json);
		TituloDao dao = new TituloDao();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		CatalogsDao catDao = new CatalogsDao();
		Map<String, Object> filtro = new HashMap<String, Object>();
		String tokenDato = "";
		try {
			Titulo tit = new Titulo();
			// DEBEMOS VERIFICAR QUE NO SE DUPLIQUE, usando matricula e id_
			// carrera:
			filtro.put("matricula", jo.get("matricula").toString().trim());
			filtro.put("id_institucion", idInst);

			Student stu = catDao.searchStudent(filtro); 

			
//			filtro.put("id_carrera", jo.get("id_carrera").toString().trim());
			List resTitulos = catDao.searchCatalog(new Titulo(), filtro);

			if (resTitulos.size() <= 0) {
				tit.setId_institucion(idInst);

				tit.setId_carrera(stu.getId_carrera());
				tit.setMatricula(jo.get("matricula").toString());

				tit.setCarrera_fechaInicio(jo.get("carrera_fechaInicio").toString().trim().equals("") ? null
						: df.parse(jo.get("carrera_fechaInicio").toString()));
				tit.setCarrera_fechaTermino(jo.get("carrera_fechaTermino").toString().trim().equals("") ? null
						: df.parse(jo.get("carrera_fechaTermino").toString()));

				tit.setExpedicion_fechaExpedicion(jo.get("expedicion_fechaExpedicion").toString().trim().equals("")
						? null : df.parse(jo.get("expedicion_fechaExpedicion").toString()));
				tit.setExpedicion_idModalidadTitulacion(
						Integer.parseInt(jo.get("expedicion_idModalidadTitulacion").toString()));
				tit.setExpedicion_fechaExamenProfesional(
						jo.get("expedicion_fechaExamenProfesional").toString().trim().equals("") ? null
								: df.parse(jo.get("expedicion_fechaExamenProfesional").toString()));

				try {
					// COMO ES UNA FECHA OPCIONAL, PONGAMOS EN UN TRY PARA
					// VERIFICAR
					// SI VIENE
					tit.setExpedicion_fechaExencionExamenProfesional(
							jo.get("expedicion_fechaExencionExamenProfesional").toString().trim().equals("") ? null
									: df.parse(jo.get("expedicion_fechaExencionExamenProfesional").toString()));
				} catch (Exception e) {
					tit.setExpedicion_fechaExencionExamenProfesional(null);
				}
				try {
					// COMO ES UNA FECHA OPCIONAL, PONGAMOS EN UN TRY PARA
					// VERIFICAR
					// SI VIENE
					tit.setEstudios_fechaTermino(jo.get("estudios_fechaTermino").toString().trim().equals("") ? null
							: df.parse(jo.get("estudios_fechaTermino").toString()));
				} catch (Exception e) {
					tit.setEstudios_fechaTermino(null);
				}
				tit.setExpedicion_cumplioServicioSocial(
						Integer.parseInt(jo.get("expedicion_cumplioServicioSocial").toString()));
				tit.setExpedicion_idFundamentoLegalServicioSocial(
						Integer.parseInt(jo.get("expedicion_idFundamentoLegalServicioSocial").toString()));
				tit.setExpedicion_idEntidadFederativa(
						Integer.parseInt(jo.get("expedicion_idEntidadFederativa").toString()));
				tit.setAntecedente_institucionProcedencia(jo.get("antecedente_institucionProcedencia").toString());
				tit.setAntecedente_idTipoEstudioAntecedente(
						Integer.parseInt(jo.get("antecedente_idTipoEstudioAntecedente").toString()));
				tit.setAntecedente_idEntidadFederativa(
						Integer.parseInt(jo.get("antecedente_idEntidadFederativa").toString()));
				try {
					// COMO ES UNA FECHA OPCIONAL, PONGAMOS EN UN TRY PARA
					// VERIFICAR
					// SI VIENE
					tit.setAntecedente_fechaInicio(jo.get("antecedente_fechaInicio").toString().trim().equals("") ? null
							: df.parse(jo.get("antecedente_fechaInicio").toString()));
				} catch (Exception e) {
					tit.setAntecedente_fechaInicio(null);
				}
				tit.setAntecedente_fechaTerminacion(jo.get("antecedente_fechaTerminacion").toString().trim().equals("")
						? null : df.parse(jo.get("antecedente_fechaTerminacion").toString()));
				tit.setAntecedente_noCedula(jo.get("antecedente_noCedula").toString());
				tit.setFecha_registro(new Date());
				tit.setFecha_modificacion(new Date());
				tit.setEstatus_titulo(1);
				dao.setTitulo(tit);
				JSONArray arrInst = jo.getJSONArray("firmantes");
				for (int i = 0; i < arrInst.length(); i++) {
					int post_id = arrInst.getInt(i);
					// System.out.println("Veamos las instituciones"+post_id);
					TituloFirmantes titFir = new TituloFirmantes();
					titFir.setId_titulo(tit.getId_titulo());
					titFir.setId_firmante(post_id);
					dao.setTituloFirmante(titFir);
				}
				response.setMessage("Titulo registrado exitosamente");
				response.setStatus(true);
				response.setToken(jwtutil.refreshToken(token));
				tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
				return Response.ok(tokenDato).build();
			} else {
				response.setMessage("Ya existe un titulo registrado con esa matricula en dicha carrera");
				response.setStatus(false);
				response.setErrorCode("");
				return Response.status(409).entity(response).build();
			}

		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@PUT
	@Path("/titulos/{id_titulo}")
	public Response updateTitulo(@HeaderParam("token") String token, @PathParam("id_titulo") Integer id_titulo,
			String json) {
		logger.info("Inside updateTitulo..."+ id_titulo );
		
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		GenericResponse response = new GenericResponse();
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Faltan los parametros del json");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 7);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		// if (rod.searchRolOperation(filtroRol).length == 0)
		// return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// JSONObject jo = (JSONObject) obj;
		org.json.JSONObject jo = new org.json.JSONObject(json);
		TituloDao dao = new TituloDao();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		String tokenDato = "";
		try {
			CatalogsDao catDao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("id_titulo", id_titulo);
			List<Object> resultadosTitulos = catDao.searchCatalog(new Titulo(), filtro);
			Titulo tit = ((Titulo) resultadosTitulos.get(0));
			// tit.setId_institucion(idInst);

			//tit.setId_carrera(Integer.parseInt(jo.get("id_carrera").toString()));
			tit.setMatricula(jo.get("matricula").toString());

			tit.setCarrera_fechaInicio(jo.get("carrera_fechaInicio").toString().trim().equals("") ? null
					: df.parse(jo.get("carrera_fechaInicio").toString()));
			tit.setCarrera_fechaTermino(jo.get("carrera_fechaTermino").toString().trim().equals("") ? null
					: df.parse(jo.get("carrera_fechaTermino").toString()));

			tit.setExpedicion_fechaExpedicion(jo.get("expedicion_fechaExpedicion").toString().trim().equals("") ? null
					: df.parse(jo.get("expedicion_fechaExpedicion").toString()));
			tit.setExpedicion_idModalidadTitulacion(
					Integer.parseInt(jo.get("expedicion_idModalidadTitulacion").toString()));
			tit.setExpedicion_fechaExamenProfesional(
					jo.get("expedicion_fechaExamenProfesional").toString().trim().equals("") ? null
							: df.parse(jo.get("expedicion_fechaExamenProfesional").toString()));

			try {
				// COMO ES UNA FECHA OPCIONAL, PONGAMOS EN UN TRY PARA VERIFICAR
				// SI VIENE
				tit.setExpedicion_fechaExencionExamenProfesional(
						jo.get("expedicion_fechaExencionExamenProfesional").toString().trim().equals("") ? null
								: df.parse(jo.get("expedicion_fechaExencionExamenProfesional").toString()));
			} catch (Exception e) {
				tit.setExpedicion_fechaExencionExamenProfesional(null);
			}
			try {
				// COMO ES UNA FECHA OPCIONAL, PONGAMOS EN UN TRY PARA VERIFICAR
				// SI VIENE
				tit.setEstudios_fechaTermino(jo.get("estudios_fechaTermino").toString().trim().equals("") ? null
						: df.parse(jo.get("estudios_fechaTermino").toString()));
			} catch (Exception e) {
				tit.setEstudios_fechaTermino(null);
			}
			tit.setExpedicion_cumplioServicioSocial(
					Integer.parseInt(jo.get("expedicion_cumplioServicioSocial").toString()));
			tit.setExpedicion_idFundamentoLegalServicioSocial(
					Integer.parseInt(jo.get("expedicion_idFundamentoLegalServicioSocial").toString()));
			tit.setExpedicion_idEntidadFederativa(
					Integer.parseInt(jo.get("expedicion_idEntidadFederativa").toString()));
			tit.setAntecedente_institucionProcedencia(jo.get("antecedente_institucionProcedencia").toString());
			tit.setAntecedente_idTipoEstudioAntecedente(
					Integer.parseInt(jo.get("antecedente_idTipoEstudioAntecedente").toString()));
			tit.setAntecedente_idEntidadFederativa(
					Integer.parseInt(jo.get("antecedente_idEntidadFederativa").toString()));
			try {
				// COMO ES UNA FECHA OPCIONAL, PONGAMOS EN UN TRY PARA VERIFICAR
				// SI VIENE
				tit.setAntecedente_fechaInicio(jo.get("antecedente_fechaInicio").toString().trim().equals("") ? null
						: df.parse(jo.get("antecedente_fechaInicio").toString()));
			} catch (Exception e) {
				tit.setAntecedente_fechaInicio(null);
			}
			tit.setAntecedente_fechaTerminacion(jo.get("antecedente_fechaTerminacion").toString().trim().equals("")
					? null : df.parse(jo.get("antecedente_fechaTerminacion").toString()));
			tit.setAntecedente_noCedula(jo.get("antecedente_noCedula").toString());
			tit.setFecha_registro(new Date());
			tit.setFecha_modificacion(new Date());
			tit.setEstatus_titulo(1);

			catDao.updateRecord(tit);

			// HAY QUE ELIMINAR LOS REGISTROS DE FIRMANTES PARA VOLVER A
			// INSERTAR:
			catDao.deleteRecords("TituloFirmantes", "id_titulo", String.valueOf(id_titulo));
			JSONArray arrInst = jo.getJSONArray("firmantes");
			for (int i = 0; i < arrInst.length(); i++) {
				int post_id = arrInst.getInt(i);
				// System.out.println("Veamos las instituciones"+post_id);
				TituloFirmantes titFir = new TituloFirmantes();
				titFir.setId_titulo(tit.getId_titulo());
				titFir.setId_firmante(post_id);
				dao.setTituloFirmante(titFir);
			}

			response.setMessage("Titulo actualizado exitosamente");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (Exception e) {
			logger.error("Excepcion updateTitulo...", e);
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@GET
	@Path("/titulos/{id}/status")
	public Response verifyStatusTitulo(@HeaderParam("token") String token, @PathParam("id") Integer id_titulo) {
		logger.info("Inside verifyStatusTitulo...");
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		GenericResponse response = new GenericResponse();
		String tokenDato;
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL NO NECESARIA

		try {
			CatalogsDao catDao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("id_titulo", id_titulo);
			List<Object> resultadosTitulos = catDao.searchCatalog(new Titulo(), filtro);
			Titulo tit = ((Titulo) resultadosTitulos.get(0));
			TitulosPortTypeSoap11Stub ws = null;

			// OBTENEMOS LAS CREDENCIALES DE LA INSTITUCION:
			filtro = new HashMap<String, Object>();
			filtro.put("id_institucion", tit.getId_institucion());
			List<Object> resultados = catDao.searchCatalog(new Configurations(), filtro);
			Configurations usSep = ((Configurations) resultados.get(0));
			// ACCIONES COMUNES EN TODAS LAS OPERACIONES:
			ws = new TitulosPortTypeSoap11Stub(new URL(usSep.getUrl_tit()), null);
			AutenticacionType auth = new AutenticacionType();
			auth.setUsuario(usSep.getUsuario_tit());
			auth.setPassword(StringUtils.desencriptar(usSep.getPassword_tit(), usSep.getUsuario_tit()));

			ConsultaProcesoTituloElectronicoRequest req = new ConsultaProcesoTituloElectronicoRequest();
			req.setNumeroLote(tit.getNumeroLote());
			req.setAutenticacion(auth);
			ConsultaProcesoTituloElectronicoResponse resp = ws.consultaProcesoTituloElectronico(req);

			response.setMessage("Respuesta de la SEP: " + resp.getMensaje());
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

	}

	@Override
	@PUT
	@Path("/titulos/{id}/cancel")
	public Response cancelTitulo(@HeaderParam("token") String token, @PathParam("id") Integer id_titulo, String json) {
		logger.info("Inside cancelTitulo...");
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		GenericResponse response = new GenericResponse();
		String tokenDato;
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 7);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func7", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Faltan los parametros del json");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		org.json.JSONObject jo = new org.json.JSONObject(json);
		try {
			CatalogsDao catDao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("id_titulo", id_titulo);
			List<Object> resultadosTitulos = catDao.searchCatalog(new Titulo(), filtro);
			Titulo tit = ((Titulo) resultadosTitulos.get(0));
			TitulosPortTypeSoap11Stub ws = null;

			// OBTENEMOS LAS CREDENCIALES DE LA INSTITUCION:
			filtro = new HashMap<String, Object>();
			filtro.put("id_institucion", tit.getId_institucion());
			List<Object> resultados = catDao.searchCatalog(new Configurations(), filtro);
			Configurations usSep = ((Configurations) resultados.get(0));
			// ACCIONES COMUNES EN TODAS LAS OPERACIONES:
			System.out.println("veamos las configuraciones:" + usSep);
			ws = new TitulosPortTypeSoap11Stub(new URL(usSep.getUrl_tit()), null);
			AutenticacionType auth = new AutenticacionType();
			auth.setUsuario(usSep.getUsuario_tit());
			auth.setPassword(StringUtils.desencriptar(usSep.getPassword_tit(), usSep.getUsuario_tit()));

			CancelaTituloElectronicoRequest req = new CancelaTituloElectronicoRequest();
			req.setFolioControl(tit.getFolioControl());
			// RECIBIR DESDE EL JSON EL MOTIVO DE CANCELACION:
			String motCancelacion = jo.get("motivoCancelacion").toString();
			req.setMotCancelacion(motCancelacion);

			tit.setMotivoCancelacion(motCancelacion);
			System.out.println("Se cancelo el titulo:" + tit);

			req.setAutenticacion(auth);

			CancelaTituloElectronicoResponse resp = ws.cancelaTituloElectronico(req);
			tit.setDescEstatusEnvio(resp.getMensaje());
			// ACTUALIZAMOS EL REGISTRO DEL TITULO:
			catDao.updateRecord(tit);
			response.setMessage("Respuesta de la SEP: " + resp.getMensaje());
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

	}

	@Override
	@GET
	@Path("/titulos/{id}")
	public Response getTitulo(@HeaderParam("token") String token, @PathParam("id") Integer id_titulo) {
		logger.info("Inside getTitulo...");
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		GenericResponse response = new GenericResponse();
		String tokenDato;
		int idInst = jwtutil.getInstitucionFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL NO NECESARIA

		try {
			CatalogsDao catDao = new CatalogsDao();
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("id_titulo", id_titulo);
			List<Object> resultadosTitulos = catDao.searchCatalog(new Titulo(), filtro);
			Titulo tit = ((Titulo) resultadosTitulos.get(0));

			filtro = new HashMap<String, Object>();
			filtro.put("id_titulo", id_titulo);
			List<Object> resultadosFirmantes = catDao.searchCatalog(new TituloFirmantes(), filtro);

			Map<String, Object> data = new HashMap<String, Object>();
			data.put("titulo", tit);
			data.put("firmantes", resultadosFirmantes);
			response.setData(data);
			response.setMessage("Información del titulo seleccionado");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			return Response.status(Response.Status.BAD_REQUEST).build();
		}

	}

	@Override
	@GET
	@Path("/titulos/{id}/informe")
	public Response getInformeSepTitulo(@HeaderParam("token") String token, @PathParam("id") Integer id_titulo) {
		logger.info("Inside getInformeSepTitulo...");
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		GenericResponse response = new GenericResponse();
		String tokenDato;
		boolean verifTimbre = true;
		int idInst = jwtutil.getInstitucionFromToken(token);
		int idUsuario = jwtutil.getIdUsuarioFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL NO NECESARIA

		try {
			CatalogsDao catDao = new CatalogsDao();
			UserDao usDao = new UserDao();
			User us = usDao.getUser(idUsuario);
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("id_titulo", id_titulo);
			List<Object> resultadosTitulos = catDao.searchCatalog(new Titulo(), filtro);
			Titulo tit = ((Titulo) resultadosTitulos.get(0));
			TitulosPortTypeSoap11Stub ws = null;

			if (tit.getDescEstatusEnvio() != null && tit.getDescEstatusEnvio().equalsIgnoreCase("Título electrónico registrado exitosamente")) {
				verifTimbre = false;
			}

			// OBTENEMOS LAS CREDENCIALES DE LA INSTITUCION:
			filtro = new HashMap<String, Object>();
			filtro.put("id_institucion", tit.getId_institucion());
			List<Object> resultados = catDao.searchCatalog(new Configurations(), filtro);
			Configurations usSep = ((Configurations) resultados.get(0));
			// ACCIONES COMUNES EN TODAS LAS OPERACIONES:
			ws = new TitulosPortTypeSoap11Stub(new URL(usSep.getUrl_tit()), null);
			AutenticacionType auth = new AutenticacionType();
			auth.setUsuario(usSep.getUsuario_tit());
			auth.setPassword(StringUtils.desencriptar(usSep.getPassword_tit(), usSep.getUsuario_tit()));

			DescargaTituloElectronicoRequest req2 = new DescargaTituloElectronicoRequest();
			req2.setNumeroLote(tit.getNumeroLote());
			req2.setAutenticacion(auth);
			DescargaTituloElectronicoResponse resp2 = ws.descargaTituloElectronico(req2);
			// System.out.println("Veamos la respuesta: " + resp2.getMensaje());
			File zipFile = new File("webapps/tmp/" + tit.getFolioControl() + ".zip");
			byte[] bytes = Base64.decodeBase64(java.util.Base64.getEncoder().encodeToString(resp2.getTitulosBase64()));

			FileUtils.writeByteArrayToFile(zipFile, bytes);

			byte[] buffer = new byte[1024];

			FileInputStream fis = new FileInputStream("webapps/tmp/" + tit.getFolioControl() + ".zip");
			ZipInputStream zis = new ZipInputStream(fis);
			ZipEntry ze = zis.getNextEntry();
			while (ze != null) {
				String fileName = ze.getName();
				File newFile = new File("webapps/tmp/" + File.separator + fileName);
				System.out.println("Unzipping to " + newFile.getAbsolutePath());
				// create directories for sub directories in zip
				new File(newFile.getParent()).mkdirs();
				FileOutputStream fos = new FileOutputStream(newFile);
				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
				fos.close();
				// close this ZipEntry
				zis.closeEntry();
				ze = zis.getNextEntry();
			}
			// close last ZipEntry
			zis.closeEntry();
			zis.close();
			fis.close();

			File excelFile = new File("webapps/tmp/" + "ResultadoCargaTitulos" + tit.getNumeroLote() + ".xls");
			FileInputStream fis2;
			fis2 = new FileInputStream(excelFile);
			HSSFWorkbook workbook = new HSSFWorkbook(fis2);

			HSSFSheet sheet = workbook.getSheetAt(0);
			
			// Read the Row
			for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {
				HSSFRow hssfRow = sheet.getRow(rowNum);
				if (hssfRow != null) {
					HSSFCell no = hssfRow.getCell(0);
					HSSFCell descripcionCell = hssfRow.getCell(2);	
					HSSFCell folioControl = hssfRow.getCell(3);					
					filtro = new HashMap<String, Object>();
					filtro.put("folioControl", folioControl.getStringCellValue());
					List<Object> resultadosTitulosXLS = catDao.searchCatalog(new Titulo(), filtro);
					tit = ((Titulo) resultadosTitulosXLS.get(0));
					//SOLAMENTE DE LOS TITULOS PENDIENTES DE SER REGISTRADOS EXISTOSAMENTE: 
					if (tit.getDescEstatusEnvio() == null 	|| !tit.getDescEstatusEnvio().equalsIgnoreCase("Título electrónico registrado exitosamente")) {
						// ACTUALIZAZION DEL STATUS EN EL REGISTRO DE TITULO ELECTRONICO:
						String descripcion = descripcionCell.getStringCellValue();
						tit.setDescEstatusEnvio(descripcion);
						catDao.updateRecord(tit);
						// VERIFICACION DE QUE YA SE ENCUENTRE EN UN AMBIENTE DE PRODUCCION:
						if (usSep.getId_tipoAmbiente_tit() == 2) {
							//
							if (verifTimbre
									&& tit.getDescEstatusEnvio().equalsIgnoreCase("Título electrónico registrado exitosamente")) {
								LicenseDao licDao = new LicenseDao();
								License lic = licDao.getLicense(us.getId_licencia());
								System.out.println();
								lic.setTimbres_disponibles(lic.getTimbres_disponibles() - 1);
								lic.setTimbres_usados(lic.getTimbres_usados() + 1);
								catDao.updateRecord(lic);
							}

						} // TERMINA VERIFICACION AMBIENTE PRODUCCION
					}
					
				}
			}
			
			
			
			
		

			

			// Double estatus =
			// sheet.getRow(1).getCell(1).getNumericCellValue();
			response.setData(java.util.Base64.getEncoder().encodeToString(resp2.getTitulosBase64()));
			response.setMessage("Respuesta de la SEP: " + resp2.getMensaje());
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

	}

	@Override
	@POST
	@Path("/titulos/send")
	public Response sendTitulos(@HeaderParam("token") String token, String json) {
		logger.info("Inside sendTitulos...");
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			 return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		GenericResponse response = new GenericResponse();

		int idInst = jwtutil.getInstitucionFromToken(token);

		int idUsuario = jwtutil.getIdUsuarioFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 7);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func8", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		CatalogsDao catDao = new CatalogsDao();
		TituloDao dao = new TituloDao();
		UserDao usDao = new UserDao();
		User us = usDao.getUser(idUsuario);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String tokenDato;
		org.json.JSONObject jo = new org.json.JSONObject(json);
		JSONArray arrTitulos = jo.getJSONArray("titulos");
		Map<Integer, String> respuesta = new HashMap<Integer, String>();
		// OBTENEMOS LAS CREDENCIALES DE LA INSTITUCION:
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("id_institucion", idInst);
		List<Object> resultados = catDao.searchCatalog(new Configurations(), filtro);
		Configurations usSep = ((Configurations) resultados.get(0));
		// VERIFICACION DE QUE YA SE ENCUENTRE EN UN AMBIENTE DE PRODUCCION:
		if (usSep.getId_tipoAmbiente_tit() == 2) {
			// VERIFICAR SI AUN TIENE TIMBRES DISPONIBLES
			LicenseDao licDao = new LicenseDao();
			License lic = licDao.getLicense(us.getId_licencia());
			if (lic.getTimbres_disponibles() < arrTitulos.length() && lic.getTimbres_disponibles() !=0 ) {
				response.setStatus(false);
				response.setMessage(
						"Ya no tiene más timbres disponibles en su cuenta, favor de contactar a su asesor de ventas");
				response.setErrorCode("");
				return Response.status(406).entity(response).build();
			}
			// VERIFICAR SI SU LICENCIA ES VIGENTE
			if (lic.getFecha_expiracion().before(new Date())) {
				response.setStatus(false);
				response.setMessage("Su licencia ha expirado, favor de contactar a su asesor de ventas");
				response.setErrorCode("");
				return Response.status(406).entity(response).build();
			}

		} // TERMINA VERIFICACION AMBIENTE PRODUCCION

		String zipFile = token.substring(0, 15) + ".zip";
		byte[] buffer = new byte[1024];
		FileOutputStream fos = null;
		ZipOutputStream zos = null;
		System.out.println("Veamos el length del array: "+arrTitulos.length());
		try {
			fos = new FileOutputStream(zipFile);
			zos = new ZipOutputStream(fos);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			for (int i = 0; i < arrTitulos.length(); i++) {
				int post_id = arrTitulos.getInt(i);
				Titulo tit = dao.getTitulo(post_id);
				System.out.println("Veamos el titulo: " + tit + ", tipo:" + Objects.isNull(tit));

				if (Objects.isNull(tit)) {
					respuesta.put(post_id, "no existe");
							
				}
				if (Objects.isNull(tit)) {				
					continue;
				}

				if (tit.getDescEstatusEnvio() != null && tit.getDescEstatusEnvio().equalsIgnoreCase("Título electrónico registrado exitosamente")) {
				
					respuesta.put(post_id, "ya registrado previamente");
				} else {
					
					respuesta.put(post_id, "enviado a la SEP");

					String xmlFilePath = "/opt/tomcat/certif/tempXML/titulo_" + post_id + ".xml";
					xmlFilePath = "titulo_" + post_id + ".xml";
					StringUtils cadena = new StringUtils();
					String folioControlStr = new String();
					folioControlStr = cadena.getCadenaAleatoria(20);
					// Titulo tit = dao.getTitulo(post_id);
					School sch = usDao.getSchool(tit.getId_institucion());

					Carrera car = catDao.getCarrera(tit.getId_carrera());

					filtro = new HashMap<String, Object>();
					filtro.put("matricula", tit.getMatricula());
					filtro.put("id_institucion", idInst);
					Student stu = catDao.searchStudent(filtro);

					filtro = new HashMap<String, Object>();
					filtro.put("id_titulo", post_id);
					List firmantes = catDao.searchCatalog(new VistaTituloFirmante(), filtro);

					AutorizacionReconocimientoShort aut = catDao.getAutorizacion(car.getId_autorizacion_reconocimiento());

					FundamentoLegalServicioSocialShort fun = catDao
							.getFundamentoLegalServicioSocialShort(tit.getExpedicion_idFundamentoLegalServicioSocial());
					ModalidadTitulacionShort mod = catDao
							.getModalidadTitulacionShort(tit.getExpedicion_idModalidadTitulacion());
					EntidadFederativaShort ent = catDao.getEntidadFederativaShort(tit.getExpedicion_idEntidadFederativa());
					TipoEstudioAntecedenteShort tip = catDao
							.getTipoEstudioAntecedenteShort(tit.getAntecedente_idTipoEstudioAntecedente());

					EntidadFederativaShort entAnt = catDao
							.getEntidadFederativaShort(tit.getAntecedente_idEntidadFederativa());

					DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
					Document document = documentBuilder.newDocument();

					// root element
					Element root = document.createElement("TituloElectronico");
					Attr xmlns = document.createAttribute("xmlns");
					Attr xmlnsxsi = document.createAttribute("xmlns:xsi");
					Attr version = document.createAttribute("version");
					Attr xsischemaLocation = document.createAttribute("xsi:schemaLocation");
					Attr folioControl = document.createAttribute("folioControl");
					xmlns.setValue("https://www.siged.sep.gob.mx/titulos/");
					xmlnsxsi.setValue("http://www.w3.org/2001/XMLSchema-instance");
					version.setValue("1.0");
					xsischemaLocation.setValue("https://www.siged.sep.gob.mx/titulos/schema.xsd");
					folioControl.setValue(folioControlStr);

					root.setAttributeNode(xmlns);
					root.setAttributeNode(xmlnsxsi);
					root.setAttributeNode(version);
					root.setAttributeNode(xsischemaLocation);
					root.setAttributeNode(folioControl);

					document.appendChild(root);
					// NODO DE LA FIRMA DE LOS RESPONSABLES, AQUI PUEDEN SER
					// VARIOS,
					// PERO PODEMOS PROBAR CON 1:
					Element FirmaResponsables = document.createElement("FirmaResponsables");
					String co;

					for (Object object : firmantes) {
						Element FirmaResponsable = document.createElement("FirmaResponsable");
						Attr nombre = document.createAttribute("nombre");
						Attr primerApellido = document.createAttribute("primerApellido");
						Attr segundoApellido = document.createAttribute("segundoApellido");
						Attr curp = document.createAttribute("curp");
						Attr idCargo = document.createAttribute("idCargo");
						Attr cargo = document.createAttribute("cargo");
						Attr abrTitulo = document.createAttribute("abrTitulo");
						Attr sello = document.createAttribute("sello");
						Attr certificadoResponsable = document.createAttribute("certificadoResponsable");
						Attr noCertificadoResponsable = document.createAttribute("noCertificadoResponsable");
						VistaTituloFirmante firm = (VistaTituloFirmante) object;
						System.out.println("Firmante seleccionado:" + firm);
						nombre.setValue(firm.getNombre());
						primerApellido.setValue(firm.getPrimerApellido());
						segundoApellido.setValue(firm.getSegundoApellido());
						curp.setValue(firm.getCurp());
						idCargo.setValue(firm.getIdCargo().toString());
						cargo.setValue(firm.getCargo_firmante());
						abrTitulo.setValue(firm.getAbrTitulo());
						/**
						 * 
						 */
						co = "||";
						co += "1.0" + "|";
						co += folioControlStr + "|";
						co += firm.getCurp() + "|";
						co += firm.getIdCargo() + "|";
						co += firm.getCargo_firmante() + "|";
						co += firm.getAbrTitulo() + "|";
						co += sch.getCveInstitucion() + "|";
						co += sch.getNombreInstitucion() + "|";
						co += car.getCve_carrera() + "|";
						co += car.getNombre_carrera() + "|";
						co += df.format(tit.getCarrera_fechaInicio()) + "|";
						co += df.format(tit.getCarrera_fechaTermino()) + "|";
						co += car.getId_autorizacion_reconocimiento().toString() + "|";
						co += aut.getValue() + "|";
						co += car.getRvoe_dgp() + "|";
						co += stu.getCurp() + "|";
						co += StringUtils.removeSpecialCharacters(stu.getNombre()) + "|";
						co += StringUtils.removeSpecialCharacters(stu.getPrimerApellido()) + "|";
						co += StringUtils.removeSpecialCharacters(stu.getSegundoApellido()) + "|";
						co += stu.getCorreoElectronico() + "|";
						//
						co += df.format(tit.getExpedicion_fechaExpedicion()) + "|";
						co += tit.getExpedicion_idModalidadTitulacion().toString() + "|";
						co += mod.getValue() + "|";
						if(tit.getExpedicion_idModalidadTitulacion()==5)
						{	
							co += "|";
							co += df.format(tit.getExpedicion_fechaExencionExamenProfesional()) + "|";
						}	
						else
						{	
							co += df.format(tit.getExpedicion_fechaExamenProfesional()) + "|";
							co += "|";
						}
						
						
						
						
						co += tit.getExpedicion_cumplioServicioSocial().toString() + "|";
						co += tit.getExpedicion_idFundamentoLegalServicioSocial().toString() + "|";
						co += fun.getValue() + "|";
						co += (tit.getExpedicion_idEntidadFederativa()<10? "0"+tit.getExpedicion_idEntidadFederativa().toString():tit.getExpedicion_idEntidadFederativa().toString()) + "|";
						co += ent.getValue() + "|";
						
						co += tit.getAntecedente_institucionProcedencia() + "|";
						co += tit.getAntecedente_idTipoEstudioAntecedente().toString() + "|";
						co += tip.getValue() + "|";
						co += (tit.getAntecedente_idEntidadFederativa()<10 ? "0"+tit.getAntecedente_idEntidadFederativa().toString():tit.getAntecedente_idEntidadFederativa().toString()) + "|";
						co += entAnt.getValue() + "|";
						if (tit.getAntecedente_fechaInicio() != null)
							co += df.format(tit.getAntecedente_fechaInicio()) + "|";
						else
							co += "|";// La fechaInicio del nodo Antecedente ES
										// OPCIONAL

						co += df.format(tit.getAntecedente_fechaTerminacion()) + "|";
						co += "||";// El campo noCedula es opcional.

						// ATENCION AQUI RUTINA PARA OBTENER EL SELLO:
						System.out.println("Cadena original" + co);
						// String keyPath = firm.getKeyFile();
						String password = StringUtils.desencriptar(firm.getPwdCertificado(),
								firm.getNoCertificadoResponsable());

						//EN EL CASO DE QUE FALLE LA CLAVE QUE TENEMOS PARA ABRIR LA KEY, ENTONCES MANDAMOS 
						//UNA EXCEPCIÓN DE QUE LOS DATOS DEL FIRMANTE SON INCORRECTOS:
						PKCS8Key pkcs8Key=null;
						try {
							pkcs8Key = new PKCS8Key(Base64.decodeBase64(firm.getKeyFile()),
									password.toCharArray());	
						} catch (Exception e) {
							// TODO: handle exception
							response.setStatus(false);
							response.setMessage(
									"Los datos del certificado del firmante son incorrectos");
							response.setErrorCode("PENDIENTE");
							response.setToken(jwtutil.refreshToken(token));
							return Response.status(406).entity(response).build();
						}
						
						
						

						final PrivateKey privateKey = pkcs8Key.getPrivateKey();

						final Signature signature = Signature.getInstance("SHA256withRSA");
						signature.initSign(privateKey);
						signature.update(co.getBytes("UTF-8"));

						sello.setValue(Base64.encodeBase64String(signature.sign()));

						certificadoResponsable.setValue(firm.getCertificadoResponsable());
						noCertificadoResponsable.setValue(firm.getNoCertificadoResponsable());
						FirmaResponsable.setAttributeNode(nombre);
						FirmaResponsable.setAttributeNode(primerApellido);
						FirmaResponsable.setAttributeNode(segundoApellido);
						FirmaResponsable.setAttributeNode(curp);
						FirmaResponsable.setAttributeNode(idCargo);
						FirmaResponsable.setAttributeNode(cargo);
						FirmaResponsable.setAttributeNode(abrTitulo);
						FirmaResponsable.setAttributeNode(sello);
						FirmaResponsable.setAttributeNode(certificadoResponsable);
						FirmaResponsable.setAttributeNode(noCertificadoResponsable);
						FirmaResponsables.appendChild(FirmaResponsable);
					}
					root.appendChild(FirmaResponsables);
					Element Institucion = document.createElement("Institucion");
					Attr cveInstitucion = document.createAttribute("cveInstitucion");
					cveInstitucion.setValue(sch.getCveInstitucion());
					Institucion.setAttributeNode(cveInstitucion);
					Attr nombreInstitucion = document.createAttribute("nombreInstitucion");
					nombreInstitucion.setValue(sch.getNombreInstitucion());
					Institucion.setAttributeNode(nombreInstitucion);
					root.appendChild(Institucion);
					Element Carrera = document.createElement("Carrera");
					Attr cveCarrera = document.createAttribute("cveCarrera");
					cveCarrera.setValue(car.getCve_carrera());
					Carrera.setAttributeNode(cveCarrera);
					Attr nombreCarrera = document.createAttribute("nombreCarrera");
					nombreCarrera.setValue(car.getNombre_carrera());
					Carrera.setAttributeNode(nombreCarrera);
					Attr fechaInicio = document.createAttribute("fechaInicio");
					fechaInicio.setValue(df.format(tit.getCarrera_fechaInicio()));
					Carrera.setAttributeNode(fechaInicio);
					Attr fechaTerminacion = document.createAttribute("fechaTerminacion");
					fechaTerminacion.setValue(df.format(tit.getCarrera_fechaTermino()));
					Carrera.setAttributeNode(fechaTerminacion);
					Attr idAutorizacionReconocimiento = document.createAttribute("idAutorizacionReconocimiento");
					idAutorizacionReconocimiento.setValue(car.getId_autorizacion_reconocimiento().toString());
					Carrera.setAttributeNode(idAutorizacionReconocimiento);
					Attr autorizacionReconocimiento = document.createAttribute("autorizacionReconocimiento");
					autorizacionReconocimiento.setValue(aut.getValue());
					Carrera.setAttributeNode(autorizacionReconocimiento);

					Attr numeroRvoe = document.createAttribute("numeroRvoe");
					numeroRvoe.setValue(car.getRvoe_dgp());
					Carrera.setAttributeNode(numeroRvoe);

					root.appendChild(Carrera);
					Element Profesionista = document.createElement("Profesionista");
					Attr curpProf = document.createAttribute("curp");
					curpProf.setValue(stu.getCurp());
					Profesionista.setAttributeNode(curpProf);
					Attr nombreProf = document.createAttribute("nombre");
					nombreProf.setValue(StringUtils.removeSpecialCharacters(stu.getNombre()));
					Profesionista.setAttributeNode(nombreProf);
					Attr primerApellidoProf = document.createAttribute("primerApellido");
					primerApellidoProf.setValue(StringUtils.removeSpecialCharacters(stu.getPrimerApellido()));
					Profesionista.setAttributeNode(primerApellidoProf);
					Attr segundoApellidoProf = document.createAttribute("segundoApellido");
					segundoApellidoProf.setValue(StringUtils.removeSpecialCharacters(stu.getSegundoApellido()));
					Profesionista.setAttributeNode(segundoApellidoProf);
					Attr correoElectronico = document.createAttribute("correoElectronico");
					correoElectronico.setValue(stu.getCorreoElectronico());
					Profesionista.setAttributeNode(correoElectronico);
					root.appendChild(Profesionista);
					Element Expedicion = document.createElement("Expedicion");
					Attr fechaExpedicion = document.createAttribute("fechaExpedicion");
					fechaExpedicion.setValue(df.format(tit.getExpedicion_fechaExpedicion()));
					Expedicion.setAttributeNode(fechaExpedicion);
					Attr idModalidadTitulacion = document.createAttribute("idModalidadTitulacion");
					idModalidadTitulacion.setValue(tit.getExpedicion_idModalidadTitulacion().toString());
					Expedicion.setAttributeNode(idModalidadTitulacion);
					Attr modalidadTitulacion = document.createAttribute("modalidadTitulacion");
					modalidadTitulacion.setValue(mod.getValue());
					Expedicion.setAttributeNode(modalidadTitulacion);
					
					if(tit.getExpedicion_idModalidadTitulacion()!=5)
					{
						Attr fechaExamenProfesional = document.createAttribute("fechaExamenProfesional");
						fechaExamenProfesional.setValue(df.format(tit.getExpedicion_fechaExamenProfesional()));
						Expedicion.setAttributeNode(fechaExamenProfesional);
					}	
					else						
					{
						Attr fechaExencionExamenProfesional = document.createAttribute("fechaExencionExamenProfesional");
						fechaExencionExamenProfesional.setValue(df.format(tit.getExpedicion_fechaExencionExamenProfesional()));
						Expedicion.setAttributeNode(fechaExencionExamenProfesional);						
					}

					
					Attr cumplioServicioSocial = document.createAttribute("cumplioServicioSocial");
					cumplioServicioSocial.setValue(tit.getExpedicion_cumplioServicioSocial().toString());
					Expedicion.setAttributeNode(cumplioServicioSocial);
					Attr idFundamentoLegalServicioSocial = document.createAttribute("idFundamentoLegalServicioSocial");
					idFundamentoLegalServicioSocial
							.setValue(tit.getExpedicion_idFundamentoLegalServicioSocial().toString());
					Expedicion.setAttributeNode(idFundamentoLegalServicioSocial);
					Attr fundamentoLegalServicioSocial = document.createAttribute("fundamentoLegalServicioSocial");
					fundamentoLegalServicioSocial.setValue(fun.getValue());
					Expedicion.setAttributeNode(fundamentoLegalServicioSocial);
					Attr idEntidadFederativa = document.createAttribute("idEntidadFederativa");
					idEntidadFederativa.setValue(tit.getExpedicion_idEntidadFederativa()<10? "0"+tit.getExpedicion_idEntidadFederativa().toString():tit.getExpedicion_idEntidadFederativa().toString());
					Expedicion.setAttributeNode(idEntidadFederativa);
					Attr entidadFederativa = document.createAttribute("entidadFederativa");
					entidadFederativa.setValue(ent.getValue());
					Expedicion.setAttributeNode(entidadFederativa);

					root.appendChild(Expedicion);
					Element Antecedente = document.createElement("Antecedente");
					Attr institucionProcedencia = document.createAttribute("institucionProcedencia");
					institucionProcedencia.setValue(tit.getAntecedente_institucionProcedencia());
					Antecedente.setAttributeNode(institucionProcedencia);
					Attr idTipoEstudioAntecedente = document.createAttribute("idTipoEstudioAntecedente");
					idTipoEstudioAntecedente.setValue(tit.getAntecedente_idTipoEstudioAntecedente().toString());
					Antecedente.setAttributeNode(idTipoEstudioAntecedente);
					Attr tipoEstudioAntecedente = document.createAttribute("tipoEstudioAntecedente");
					tipoEstudioAntecedente.setValue(tip.getValue());
					Antecedente.setAttributeNode(tipoEstudioAntecedente);
					Attr idEntidadFederativaAntec = document.createAttribute("idEntidadFederativa");
					idEntidadFederativaAntec.setValue(tit.getAntecedente_idEntidadFederativa()<10 ? "0"+tit.getAntecedente_idEntidadFederativa().toString():tit.getAntecedente_idEntidadFederativa().toString());
					Antecedente.setAttributeNode(idEntidadFederativaAntec);
					Attr entidadFederativaAntec = document.createAttribute("entidadFederativa");
					entidadFederativaAntec.setValue(entAnt.getValue());
					Antecedente.setAttributeNode(entidadFederativaAntec);
					if (tit.getAntecedente_fechaInicio() != null) {
						Attr fechaInicioAntec = document.createAttribute("fechaInicio");
						fechaInicioAntec.setValue(df.format(tit.getAntecedente_fechaInicio()));
						Antecedente.setAttributeNode(fechaInicioAntec);
					}
					Attr fechaTerminacionAntec = document.createAttribute("fechaTerminacion");
					fechaTerminacionAntec.setValue(df.format(tit.getAntecedente_fechaTerminacion()));
					Antecedente.setAttributeNode(fechaTerminacionAntec);
					// El campo noCedula es opcional.
					root.appendChild(Antecedente);
					// create the xml file
					// transform the DOM Object to an XML File
					TransformerFactory transformerFactory = TransformerFactory.newInstance();
					Transformer transformer = transformerFactory.newTransformer();
					DOMSource domSource = new DOMSource(document);
					StreamResult streamResult = new StreamResult(new File(xmlFilePath));
					transformer.transform(domSource, streamResult);

					// le agregamos el FOLIO DE CONTROL:

					tit.setFolioControl(folioControlStr);
					catDao.updateRecord(tit);
					File srcFile = new File(xmlFilePath);
					FileInputStream fis = new FileInputStream(srcFile);
					zos.putNextEntry(new ZipEntry(srcFile.getName()));
					int length;
					while ((length = fis.read(buffer)) > 0) {
						zos.write(buffer, 0, length);
					}

					fis.close();

				}

			}
			zos.closeEntry();

			zos.close();

			// EN ESTE MOMENTO YA TENEMOS EL XML COMPLETO, POR LO TANTO HAY QUE
			// CREAR EL CLIENTE WS PARA ENVIAR A LA SEP:
			TitulosPortTypeSoap11Stub ws = null;
			System.out.println("Veamos las configuraciones de la SEP"+ usSep);
			// ACCIONES COMUNES EN TODAS LAS OPERACIONES:
			ws = new TitulosPortTypeSoap11Stub(new URL(usSep.getUrl_tit()), null);
			AutenticacionType auth = new AutenticacionType();
			auth.setUsuario(usSep.getUsuario_tit());
			auth.setPassword(StringUtils.desencriptar(usSep.getPassword_tit(), usSep.getUsuario_tit()));

			CargaTituloElectronicoRequest cargaReq = new CargaTituloElectronicoRequest();
			cargaReq.setAutenticacion(auth);
			cargaReq.setNombreArchivo(token.substring(0, 15) + ".zip");

			byte[] fileContent = FileUtils.readFileToByteArray(new File(zipFile));
			String encodedString = Base64.encodeBase64String(fileContent);

			cargaReq.setArchivoBase64(Base64.decodeBase64(encodedString));
			CargaTituloElectronicoResponse cargaResp = ws.cargaTituloElectronico(cargaReq);
			System.out.println("VEAMOS LA RESPUESTA:"+cargaResp.getMensaje());
			// LOS TITULOS QUE SI SE FUERON, LES ACTUALIZAMOS EL NUMERO DE LOTE:
			for (int i = 0; i < arrTitulos.length(); i++) {
				int post_id = arrTitulos.getInt(i);
				Titulo tit = dao.getTitulo(post_id);
				if (Objects.nonNull(tit)) {

					if (tit.getDescEstatusEnvio() != null && tit.getDescEstatusEnvio().equalsIgnoreCase("Título electrónico registrado exitosamente")) {
				
					} else {
						tit.setNumeroLote(cargaResp.getNumeroLote());
						catDao.updateRecord(tit);
					}
				}
			}

			response.setMessage("Titulos enviados a la SEP exitosamente");
			response.setStatus(true);
			response.setData(respuesta);
			response.setToken(jwtutil.refreshToken(token));
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@GET
	@Path("/titulos/{id}/send")
	public Response sendTitulo(@HeaderParam("token") String token, @PathParam("id") Integer id) {
		logger.info("Inside sendTitulo...");
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		GenericResponse response = new GenericResponse();

		int idInst = jwtutil.getInstitucionFromToken(token);

		int idUsuario = jwtutil.getIdUsuarioFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 7);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func3", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		CatalogsDao catDao = new CatalogsDao();
		TituloDao dao = new TituloDao();
		UserDao usDao = new UserDao();
		User us = usDao.getUser(idUsuario);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String tokenDato;
		String xmlFilePath = "/opt/tomcat/certif/tempXML/titulo_" + id + ".xml";
		xmlFilePath = "titulo_" + id + ".xml";

		// FALTA VALIDAR QUE NO ESTE ENVIADO, ENTONCES TENEMOS QUE HCER UNA
		// BUSQUEDA:

		try {
			StringUtils cadena = new StringUtils();
			String folioControlStr = new String();
			folioControlStr = cadena.getCadenaAleatoria(20);
			Titulo tit = dao.getTitulo(id);
			School sch = usDao.getSchool(tit.getId_institucion());

			// OBTENEMOS LAS CREDENCIALES DE LA INSTITUCION:
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("id_institucion", sch.getId_institucion());
			List<Object> resultados = catDao.searchCatalog(new Configurations(), filtro);
			Configurations usSep = ((Configurations) resultados.get(0));
			// VERIFICACION DE QUE YA SE ENCUENTRE EN UN AMBIENTE DE PRODUCCION:
			if (usSep.getId_tipoAmbiente_tit() == 2) {
				// VERIFICAR SI AUN TIENE TIMBRES DISPONIBLES
				LicenseDao licDao = new LicenseDao();
				License lic = licDao.getLicense(us.getId_licencia());
				if (lic.getTimbres_disponibles() <= 0) {
					response.setStatus(false);
					response.setMessage(
							"Ya no tiene más timbres disponibles en su cuenta, favor de contactar a su asesor de ventas");
					response.setErrorCode("");
					return Response.status(404).entity(response).build();
				}
				// VERIFICAR SI SU LICENCIA ES VIGENTE
				if (lic.getFecha_expiracion().before(new Date())) {
					response.setStatus(false);
					response.setMessage("Su licencia ha expirado, favor de contactar a su asesor de ventas");
					response.setErrorCode("");
					return Response.status(404).entity(response).build();
				}

			} // TERMINA VERIFICACION AMBIENTE PRODUCCION

			Carrera car = catDao.getCarrera(tit.getId_carrera());

			filtro = new HashMap<String, Object>();
			filtro.put("matricula", tit.getMatricula());
			filtro.put("id_institucion", idInst);
			Student stu = catDao.searchStudent(filtro);

			filtro = new HashMap<String, Object>();
			filtro.put("id_titulo", id);
			List firmantes = catDao.searchCatalog(new VistaTituloFirmante(), filtro);

			AutorizacionReconocimientoShort aut = catDao.getAutorizacion(car.getId_autorizacion_reconocimiento());

			FundamentoLegalServicioSocialShort fun = catDao
					.getFundamentoLegalServicioSocialShort(tit.getExpedicion_idFundamentoLegalServicioSocial());
			ModalidadTitulacionShort mod = catDao.getModalidadTitulacionShort(tit.getExpedicion_idModalidadTitulacion());
			EntidadFederativaShort ent = catDao.getEntidadFederativaShort(tit.getExpedicion_idEntidadFederativa());
			TipoEstudioAntecedenteShort tip = catDao
					.getTipoEstudioAntecedenteShort(tit.getAntecedente_idTipoEstudioAntecedente());

			EntidadFederativaShort entAnt = catDao.getEntidadFederativaShort(tit.getAntecedente_idEntidadFederativa());

			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
			Document document = documentBuilder.newDocument();

			// root element
			Element root = document.createElement("TituloElectronico");
			Attr xmlns = document.createAttribute("xmlns");
			Attr xmlnsxsi = document.createAttribute("xmlns:xsi");
			Attr version = document.createAttribute("version");
			Attr xsischemaLocation = document.createAttribute("xsi:schemaLocation");
			Attr folioControl = document.createAttribute("folioControl");
			xmlns.setValue("https://www.siged.sep.gob.mx/titulos/");
			xmlnsxsi.setValue("http://www.w3.org/2001/XMLSchema-instance");
			version.setValue("1.0");
			xsischemaLocation.setValue("https://www.siged.sep.gob.mx/titulos/schema.xsd");
			folioControl.setValue(folioControlStr);

			root.setAttributeNode(xmlns);
			root.setAttributeNode(xmlnsxsi);
			root.setAttributeNode(version);
			root.setAttributeNode(xsischemaLocation);
			root.setAttributeNode(folioControl);

			document.appendChild(root);
			// NODO DE LA FIRMA DE LOS RESPONSABLES, AQUI PUEDEN SER VARIOS,
			// PERO PODEMOS PROBAR CON 1:
			Element FirmaResponsables = document.createElement("FirmaResponsables");
			String co;

			for (Object object : firmantes) {
				Element FirmaResponsable = document.createElement("FirmaResponsable");
				Attr nombre = document.createAttribute("nombre");
				Attr primerApellido = document.createAttribute("primerApellido");
				Attr segundoApellido = document.createAttribute("segundoApellido");
				Attr curp = document.createAttribute("curp");
				Attr idCargo = document.createAttribute("idCargo");
				Attr cargo = document.createAttribute("cargo");
				Attr abrTitulo = document.createAttribute("abrTitulo");
				Attr sello = document.createAttribute("sello");
				Attr certificadoResponsable = document.createAttribute("certificadoResponsable");
				Attr noCertificadoResponsable = document.createAttribute("noCertificadoResponsable");
				VistaTituloFirmante firm = (VistaTituloFirmante) object;
				System.out.println("Firmante seleccionado:" + firm);
				nombre.setValue(firm.getNombre());
				primerApellido.setValue(firm.getPrimerApellido());
				segundoApellido.setValue(firm.getSegundoApellido());
				curp.setValue(firm.getCurp());
				idCargo.setValue(firm.getIdCargo().toString());
				cargo.setValue(firm.getCargo_firmante());
				abrTitulo.setValue(firm.getAbrTitulo());
				/**
				 * 
				 */
				co = "||";
				co += "1.0" + "|";
				co += folioControlStr + "|";
				co += firm.getCurp() + "|";
				co += firm.getIdCargo() + "|";
				co += firm.getCargo_firmante() + "|";
				co += firm.getAbrTitulo() + "|";
				co += sch.getCveInstitucion() + "|";
				co += sch.getNombreInstitucion() + "|";
				co += car.getCve_carrera() + "|";
				co += car.getNombre_carrera() + "|";
				co += df.format(tit.getCarrera_fechaInicio()) + "|";
				co += df.format(tit.getCarrera_fechaTermino()) + "|";
				co += car.getId_autorizacion_reconocimiento().toString() + "|";
				co += aut.getValue() + "|";
				co += car.getRvoe_dgp() + "|";
				co += stu.getCurp() + "|";
				co += StringUtils.removeSpecialCharacters(stu.getNombre()) + "|";
				co += StringUtils.removeSpecialCharacters(stu.getPrimerApellido()) + "|";
				co += StringUtils.removeSpecialCharacters(stu.getSegundoApellido()) + "|";
				co += stu.getCorreoElectronico() + "|";
				//
				co += df.format(tit.getExpedicion_fechaExpedicion()) + "|";
				co += tit.getExpedicion_idModalidadTitulacion().toString() + "|";
				co += mod.getValue() + "|";
				if(tit.getExpedicion_idModalidadTitulacion()==5)
				{	
					co += "|";
					co += df.format(tit.getExpedicion_fechaExencionExamenProfesional()) + "|";
				}	
				else
				{	
					co += df.format(tit.getExpedicion_fechaExamenProfesional()) + "|";
					co += "|";
				}
				co += tit.getExpedicion_cumplioServicioSocial().toString() + "|";
				co += tit.getExpedicion_idFundamentoLegalServicioSocial().toString() + "|";
				co += fun.getValue() + "|";
				co += (tit.getExpedicion_idEntidadFederativa()<10 ? "0"+tit.getExpedicion_idEntidadFederativa().toString(): tit.getExpedicion_idEntidadFederativa().toString()) + "|";
				co += ent.getValue() + "|";

				co += tit.getAntecedente_institucionProcedencia() + "|";
				co += tit.getAntecedente_idTipoEstudioAntecedente().toString() + "|";
				co += tip.getValue() + "|";
				co += (tit.getAntecedente_idEntidadFederativa()<10 ? "0"+tit.getAntecedente_idEntidadFederativa().toString():tit.getAntecedente_idEntidadFederativa().toString()) + "|";
				co += entAnt.getValue() + "|";
				if (tit.getAntecedente_fechaInicio() != null)
					co += df.format(tit.getAntecedente_fechaInicio()) + "|";
				else
					co += "|";// La fechaInicio del nodo Antecedente ES OPCIONAL

				co += df.format(tit.getAntecedente_fechaTerminacion()) + "|";
				co += "||";// El campo noCedula es opcional.

				// ATENCION AQUI RUTINA PARA OBTENER EL SELLO:
				System.out.println("Cadena original" + co);
				// String keyPath = firm.getKeyFile();
				String password = StringUtils.desencriptar(firm.getPwdCertificado(),
						firm.getNoCertificadoResponsable());

				// final PKCS8Key pkcs8Key = new PKCS8Key(toByteArray(keyPath),
				// password.toCharArray());
				final PKCS8Key pkcs8Key = new PKCS8Key(Base64.decodeBase64(firm.getKeyFile()), password.toCharArray());

				final PrivateKey privateKey = pkcs8Key.getPrivateKey();

				final Signature signature = Signature.getInstance("SHA256withRSA");
				signature.initSign(privateKey);
				signature.update(co.getBytes("UTF-8"));

				sello.setValue(Base64.encodeBase64String(signature.sign()));

				certificadoResponsable.setValue(firm.getCertificadoResponsable());
				noCertificadoResponsable.setValue(firm.getNoCertificadoResponsable());
				FirmaResponsable.setAttributeNode(nombre);
				FirmaResponsable.setAttributeNode(primerApellido);
				FirmaResponsable.setAttributeNode(segundoApellido);
				FirmaResponsable.setAttributeNode(curp);
				FirmaResponsable.setAttributeNode(idCargo);
				FirmaResponsable.setAttributeNode(cargo);
				FirmaResponsable.setAttributeNode(abrTitulo);
				FirmaResponsable.setAttributeNode(sello);
				FirmaResponsable.setAttributeNode(certificadoResponsable);
				FirmaResponsable.setAttributeNode(noCertificadoResponsable);
				FirmaResponsables.appendChild(FirmaResponsable);
			}
			root.appendChild(FirmaResponsables);
			Element Institucion = document.createElement("Institucion");
			Attr cveInstitucion = document.createAttribute("cveInstitucion");
			cveInstitucion.setValue(sch.getCveInstitucion());
			Institucion.setAttributeNode(cveInstitucion);
			Attr nombreInstitucion = document.createAttribute("nombreInstitucion");
			nombreInstitucion.setValue(sch.getNombreInstitucion());
			Institucion.setAttributeNode(nombreInstitucion);
			root.appendChild(Institucion);
			Element Carrera = document.createElement("Carrera");
			Attr cveCarrera = document.createAttribute("cveCarrera");
			cveCarrera.setValue(car.getCve_carrera());
			Carrera.setAttributeNode(cveCarrera);
			Attr nombreCarrera = document.createAttribute("nombreCarrera");
			nombreCarrera.setValue(car.getNombre_carrera());
			Carrera.setAttributeNode(nombreCarrera);
			Attr fechaInicio = document.createAttribute("fechaInicio");
			fechaInicio.setValue(df.format(tit.getCarrera_fechaInicio()));
			Carrera.setAttributeNode(fechaInicio);
			Attr fechaTerminacion = document.createAttribute("fechaTerminacion");
			fechaTerminacion.setValue(df.format(tit.getCarrera_fechaTermino()));
			Carrera.setAttributeNode(fechaTerminacion);
			Attr idAutorizacionReconocimiento = document.createAttribute("idAutorizacionReconocimiento");
			idAutorizacionReconocimiento.setValue(car.getId_autorizacion_reconocimiento().toString());
			Carrera.setAttributeNode(idAutorizacionReconocimiento);
			Attr autorizacionReconocimiento = document.createAttribute("autorizacionReconocimiento");
			autorizacionReconocimiento.setValue(aut.getValue());
			Carrera.setAttributeNode(autorizacionReconocimiento);

			Attr numeroRvoe = document.createAttribute("numeroRvoe");
			numeroRvoe.setValue(car.getRvoe_dgp());
			Carrera.setAttributeNode(numeroRvoe);

			root.appendChild(Carrera);
			Element Profesionista = document.createElement("Profesionista");
			Attr curpProf = document.createAttribute("curp");
			curpProf.setValue(stu.getCurp());
			Profesionista.setAttributeNode(curpProf);
			Attr nombreProf = document.createAttribute("nombre");
			nombreProf.setValue(StringUtils.removeSpecialCharacters(stu.getNombre()));
			Profesionista.setAttributeNode(nombreProf);
			Attr primerApellidoProf = document.createAttribute("primerApellido");
			primerApellidoProf.setValue(StringUtils.removeSpecialCharacters(stu.getPrimerApellido()));
			Profesionista.setAttributeNode(primerApellidoProf);
			Attr segundoApellidoProf = document.createAttribute("segundoApellido");
			segundoApellidoProf.setValue(StringUtils.removeSpecialCharacters(stu.getSegundoApellido()));
			Profesionista.setAttributeNode(segundoApellidoProf);
			Attr correoElectronico = document.createAttribute("correoElectronico");
			correoElectronico.setValue(stu.getCorreoElectronico());
			Profesionista.setAttributeNode(correoElectronico);
			root.appendChild(Profesionista);
			Element Expedicion = document.createElement("Expedicion");
			Attr fechaExpedicion = document.createAttribute("fechaExpedicion");
			fechaExpedicion.setValue(df.format(tit.getExpedicion_fechaExpedicion()));
			Expedicion.setAttributeNode(fechaExpedicion);
			Attr idModalidadTitulacion = document.createAttribute("idModalidadTitulacion");
			idModalidadTitulacion.setValue(tit.getExpedicion_idModalidadTitulacion().toString());
			Expedicion.setAttributeNode(idModalidadTitulacion);
			Attr modalidadTitulacion = document.createAttribute("modalidadTitulacion");
			modalidadTitulacion.setValue(mod.getValue());
			Expedicion.setAttributeNode(modalidadTitulacion);
			
			if(tit.getExpedicion_idModalidadTitulacion()!=5)
			{
				Attr fechaExamenProfesional = document.createAttribute("fechaExamenProfesional");
				fechaExamenProfesional.setValue(df.format(tit.getExpedicion_fechaExamenProfesional()));
				Expedicion.setAttributeNode(fechaExamenProfesional);
			}
			else						
			{
				Attr fechaExencionExamenProfesional = document.createAttribute("fechaExencionExamenProfesional");
				fechaExencionExamenProfesional.setValue(df.format(tit.getExpedicion_fechaExencionExamenProfesional()));
				Expedicion.setAttributeNode(fechaExencionExamenProfesional);						
			}
			
			Attr cumplioServicioSocial = document.createAttribute("cumplioServicioSocial");
			cumplioServicioSocial.setValue(tit.getExpedicion_cumplioServicioSocial().toString());
			Expedicion.setAttributeNode(cumplioServicioSocial);
			Attr idFundamentoLegalServicioSocial = document.createAttribute("idFundamentoLegalServicioSocial");
			idFundamentoLegalServicioSocial.setValue(tit.getExpedicion_idFundamentoLegalServicioSocial().toString());
			Expedicion.setAttributeNode(idFundamentoLegalServicioSocial);
			Attr fundamentoLegalServicioSocial = document.createAttribute("fundamentoLegalServicioSocial");
			fundamentoLegalServicioSocial.setValue(fun.getValue());
			Expedicion.setAttributeNode(fundamentoLegalServicioSocial);
			Attr idEntidadFederativa = document.createAttribute("idEntidadFederativa");
			idEntidadFederativa.setValue(tit.getExpedicion_idEntidadFederativa()<10? "0"+tit.getExpedicion_idEntidadFederativa().toString(): tit.getExpedicion_idEntidadFederativa().toString());
			Expedicion.setAttributeNode(idEntidadFederativa);
			Attr entidadFederativa = document.createAttribute("entidadFederativa");
			entidadFederativa.setValue(ent.getValue());
			Expedicion.setAttributeNode(entidadFederativa);

			root.appendChild(Expedicion);
			Element Antecedente = document.createElement("Antecedente");
			Attr institucionProcedencia = document.createAttribute("institucionProcedencia");
			institucionProcedencia.setValue(tit.getAntecedente_institucionProcedencia());
			Antecedente.setAttributeNode(institucionProcedencia);
			Attr idTipoEstudioAntecedente = document.createAttribute("idTipoEstudioAntecedente");
			idTipoEstudioAntecedente.setValue(tit.getAntecedente_idTipoEstudioAntecedente().toString());
			Antecedente.setAttributeNode(idTipoEstudioAntecedente);
			Attr tipoEstudioAntecedente = document.createAttribute("tipoEstudioAntecedente");
			tipoEstudioAntecedente.setValue(tip.getValue());
			Antecedente.setAttributeNode(tipoEstudioAntecedente);
			Attr idEntidadFederativaAntec = document.createAttribute("idEntidadFederativa");
			idEntidadFederativaAntec.setValue(tit.getAntecedente_idEntidadFederativa()<10 ? "0"+tit.getAntecedente_idEntidadFederativa().toString():tit.getAntecedente_idEntidadFederativa().toString());
			Antecedente.setAttributeNode(idEntidadFederativaAntec);
			Attr entidadFederativaAntec = document.createAttribute("entidadFederativa");
			entidadFederativaAntec.setValue(entAnt.getValue());
			Antecedente.setAttributeNode(entidadFederativaAntec);
			if (tit.getAntecedente_fechaInicio() != null) {
				Attr fechaInicioAntec = document.createAttribute("fechaInicio");
				fechaInicioAntec.setValue(df.format(tit.getAntecedente_fechaInicio()));
				Antecedente.setAttributeNode(fechaInicioAntec);
			}
			Attr fechaTerminacionAntec = document.createAttribute("fechaTerminacion");
			fechaTerminacionAntec.setValue(df.format(tit.getAntecedente_fechaTerminacion()));
			Antecedente.setAttributeNode(fechaTerminacionAntec);
			// El campo noCedula es opcional.
			root.appendChild(Antecedente);
			// create the xml file
			// transform the DOM Object to an XML File
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource domSource = new DOMSource(document);
			StreamResult streamResult = new StreamResult(new File(xmlFilePath));
			transformer.transform(domSource, streamResult);
			// EN ESTE MOMENTO YA TENEMOS EL XML COMPLETO, POR LO TANTO HAY QUE
			// CREAR EL CLIENTE WS PARA ENVIAR A LA SEP:
			TitulosPortTypeSoap11Stub ws = null;

			// ACCIONES COMUNES EN TODAS LAS OPERACIONES:
			ws = new TitulosPortTypeSoap11Stub(new URL(usSep.getUrl_tit()), null);
			AutenticacionType auth = new AutenticacionType();
			auth.setUsuario(usSep.getUsuario_tit());
			auth.setPassword(StringUtils.desencriptar(usSep.getPassword_tit(), usSep.getUsuario_tit()));

			CargaTituloElectronicoRequest cargaReq = new CargaTituloElectronicoRequest();
			cargaReq.setAutenticacion(auth);
			cargaReq.setNombreArchivo(tit.getMatricula() + ".xml");

			byte[] fileContent = FileUtils.readFileToByteArray(new File(xmlFilePath));
			String encodedString = Base64.encodeBase64String(fileContent);

			cargaReq.setArchivoBase64(Base64.decodeBase64(encodedString));
			CargaTituloElectronicoResponse cargaResp = ws.cargaTituloElectronico(cargaReq);
			// System.out.println("Veamos la respuesta (numero
			// Lote):"+cargaResp.getNumeroLote());
			// le agregamos el numero de Lote y cambiamos el status:
			tit.setNumeroLote(cargaResp.getNumeroLote());
			tit.setFolioControl(folioControlStr);
			catDao.updateRecord(tit);
			response.setMessage("Titulo enviado a la SEP exitosamente");
			response.setStatus(true);
			response.setData(cargaResp.getNumeroLote());
			response.setToken(jwtutil.refreshToken(token));
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@GET
	@Path("/titulos/{id}/xml")
	public Response getXML(@HeaderParam("token") String token, @PathParam("id") Integer id) {
		logger.info("Inside getXML...");
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		GenericResponse response = new GenericResponse();

		int idInst = jwtutil.getInstitucionFromToken(token);

		int idUsuario = jwtutil.getIdUsuarioFromToken(token);
		if (idInst == 0) {
			response.setStatus(false);
			response.setMessage("El token no contiene el id de institución");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();

		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 7);// Conforme al catalogo de permisos y
										// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func4", 1); // CONFORME AL ID DE COLUMNA QUE
										// CORRESPONDA
										// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		CatalogsDao catDao = new CatalogsDao();
		TituloDao dao = new TituloDao();
		UserDao usDao = new UserDao();
		User us = usDao.getUser(idUsuario);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String tokenDato;
		String xmlFilePath = "/opt/tomcat/certif/tempXML/titulo_" + id + ".xml";
		xmlFilePath = "titulo_" + id + ".xml";

		// FALTA VALIDAR QUE NO ESTE ENVIADO, ENTONCES TENEMOS QUE HCER UNA
		// BUSQUEDA:

		try {
			StringUtils cadena = new StringUtils();

			Titulo tit = dao.getTitulo(id);
			School sch = usDao.getSchool(tit.getId_institucion());

			Carrera car = catDao.getCarrera(tit.getId_carrera());

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("matricula", tit.getMatricula());
			filtro.put("id_institucion", idInst);
			Student stu = catDao.searchStudent(filtro);

			filtro = new HashMap<String, Object>();
			filtro.put("id_titulo", id);
			List firmantes = catDao.searchCatalog(new VistaTituloFirmante(), filtro);

			AutorizacionReconocimientoShort aut = catDao.getAutorizacion(car.getId_autorizacion_reconocimiento());

			FundamentoLegalServicioSocialShort fun = catDao
					.getFundamentoLegalServicioSocialShort(tit.getExpedicion_idFundamentoLegalServicioSocial());
			ModalidadTitulacionShort mod = catDao.getModalidadTitulacionShort(tit.getExpedicion_idModalidadTitulacion());
			EntidadFederativaShort ent = catDao.getEntidadFederativaShort(tit.getExpedicion_idEntidadFederativa());
			TipoEstudioAntecedenteShort tip = catDao
					.getTipoEstudioAntecedenteShort(tit.getAntecedente_idTipoEstudioAntecedente()); 

			EntidadFederativaShort entAnt = catDao.getEntidadFederativaShort(tit.getAntecedente_idEntidadFederativa());

			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
			Document document = documentBuilder.newDocument();

			// root element
			Element root = document.createElement("TituloElectronico");
			Attr xmlns = document.createAttribute("xmlns");
			Attr xmlnsxsi = document.createAttribute("xmlns:xsi");
			Attr version = document.createAttribute("version");
			Attr xsischemaLocation = document.createAttribute("xsi:schemaLocation");
			Attr folioControl = document.createAttribute("folioControl");
			xmlns.setValue("https://www.siged.sep.gob.mx/titulos/");
			xmlnsxsi.setValue("http://www.w3.org/2001/XMLSchema-instance");
			version.setValue("1.0");
			xsischemaLocation.setValue("https://www.siged.sep.gob.mx/titulos/schema.xsd");
			folioControl.setValue(tit.getFolioControl());

			root.setAttributeNode(xmlns);
			root.setAttributeNode(xmlnsxsi);
			root.setAttributeNode(version);
			root.setAttributeNode(xsischemaLocation);
			root.setAttributeNode(folioControl);

			document.appendChild(root);
			// NODO DE LA FIRMA DE LOS RESPONSABLES, AQUI PUEDEN SER VARIOS,
			// PERO PODEMOS PROBAR CON 1:
			Element FirmaResponsables = document.createElement("FirmaResponsables");
			String co;

			for (Object object : firmantes) {
				Element FirmaResponsable = document.createElement("FirmaResponsable");
				Attr nombre = document.createAttribute("nombre");
				Attr primerApellido = document.createAttribute("primerApellido");
				Attr segundoApellido = document.createAttribute("segundoApellido");
				Attr curp = document.createAttribute("curp");
				Attr idCargo = document.createAttribute("idCargo");
				Attr cargo = document.createAttribute("cargo");
				Attr abrTitulo = document.createAttribute("abrTitulo");
				Attr sello = document.createAttribute("sello");
				Attr certificadoResponsable = document.createAttribute("certificadoResponsable");
				Attr noCertificadoResponsable = document.createAttribute("noCertificadoResponsable");
				VistaTituloFirmante firm = (VistaTituloFirmante) object;
				System.out.println("Firmante seleccionado:" + firm);
				nombre.setValue(firm.getNombre());
				primerApellido.setValue(firm.getPrimerApellido());
				segundoApellido.setValue(firm.getSegundoApellido());
				curp.setValue(firm.getCurp());
				idCargo.setValue(firm.getIdCargo().toString());
				cargo.setValue(firm.getCargo_firmante());
				abrTitulo.setValue(firm.getAbrTitulo());
				/**
				 * 
				 */
				co = "||";
				co += "1.0" + "|";
				co += tit.getFolioControl() + "|";
				co += firm.getCurp() + "|";
				co += firm.getIdCargo() + "|";
				co += firm.getCargo_firmante() + "|";
				co += firm.getAbrTitulo() + "|";
				co += sch.getCveInstitucion() + "|";
				co += sch.getNombreInstitucion() + "|";
				co += car.getCve_carrera() + "|";
				co += car.getNombre_carrera() + "|";
				co += df.format(tit.getCarrera_fechaInicio()) + "|";
				co += df.format(tit.getCarrera_fechaTermino()) + "|";
				co += car.getId_autorizacion_reconocimiento().toString() + "|";
				co += aut.getValue() + "|";
				co += car.getRvoe_dgp() + "|";
				co += stu.getCurp() + "|";
				co += StringUtils.removeSpecialCharacters(stu.getNombre()) + "|";
				co += StringUtils.removeSpecialCharacters(stu.getPrimerApellido()) + "|";
				co += StringUtils.removeSpecialCharacters(stu.getSegundoApellido()) + "|";
				co += stu.getCorreoElectronico() + "|";
				//
				co += df.format(tit.getExpedicion_fechaExpedicion()) + "|";
				co += tit.getExpedicion_idModalidadTitulacion().toString() + "|";
				co += mod.getValue() + "|";
				if(tit.getExpedicion_idModalidadTitulacion()==5)
				{	
					co += "|";
					co += df.format(tit.getExpedicion_fechaExencionExamenProfesional()) + "|";
				}	
				else
				{	
					co += df.format(tit.getExpedicion_fechaExamenProfesional()) + "|";
					co += "|";
				}
				co += tit.getExpedicion_cumplioServicioSocial().toString() + "|";
				co += tit.getExpedicion_idFundamentoLegalServicioSocial().toString() + "|";
				co += fun.getValue() + "|";
				co += (tit.getExpedicion_idEntidadFederativa() <10 ? "0"+tit.getExpedicion_idEntidadFederativa().toString(): tit.getExpedicion_idEntidadFederativa().toString()) + "|";
				co += ent.getValue() + "|";

				co += tit.getAntecedente_institucionProcedencia() + "|";
				co += tit.getAntecedente_idTipoEstudioAntecedente().toString() + "|";
				co += tip.getValue() + "|";
				co += (tit.getAntecedente_idEntidadFederativa()<10?"0"+tit.getAntecedente_idEntidadFederativa().toString():tit.getAntecedente_idEntidadFederativa().toString()) + "|";
				co += entAnt.getValue() + "|";
				if (tit.getAntecedente_fechaInicio() != null)
					co += df.format(tit.getAntecedente_fechaInicio()) + "|";
				else
					co += "|";// La fechaInicio del nodo Antecedente ES OPCIONAL

				co += df.format(tit.getAntecedente_fechaTerminacion()) + "|";
				co += "||";// El campo noCedula es opcional.

				// ATENCION AQUI RUTINA PARA OBTENER EL SELLO:
				System.out.println("Cadena original" + co);
				// String keyPath = firm.getKeyFile();
				String password = StringUtils.desencriptar(firm.getPwdCertificado(),
						firm.getNoCertificadoResponsable());

				// final PKCS8Key pkcs8Key = new PKCS8Key(toByteArray(keyPath),
				// password.toCharArray());
				final PKCS8Key pkcs8Key = new PKCS8Key(Base64.decodeBase64(firm.getKeyFile()), password.toCharArray());

				final PrivateKey privateKey = pkcs8Key.getPrivateKey();

				final Signature signature = Signature.getInstance("SHA256withRSA");
				signature.initSign(privateKey);
				signature.update(co.getBytes("UTF-8"));

				sello.setValue(Base64.encodeBase64String(signature.sign()));

				certificadoResponsable.setValue(firm.getCertificadoResponsable());
				noCertificadoResponsable.setValue(firm.getNoCertificadoResponsable());
				FirmaResponsable.setAttributeNode(nombre);
				FirmaResponsable.setAttributeNode(primerApellido);
				FirmaResponsable.setAttributeNode(segundoApellido);
				FirmaResponsable.setAttributeNode(curp);
				FirmaResponsable.setAttributeNode(idCargo);
				FirmaResponsable.setAttributeNode(cargo);
				FirmaResponsable.setAttributeNode(abrTitulo);
				FirmaResponsable.setAttributeNode(sello);
				FirmaResponsable.setAttributeNode(certificadoResponsable);
				FirmaResponsable.setAttributeNode(noCertificadoResponsable);
				FirmaResponsables.appendChild(FirmaResponsable);
			}
			root.appendChild(FirmaResponsables);
			Element Institucion = document.createElement("Institucion");
			Attr cveInstitucion = document.createAttribute("cveInstitucion");
			cveInstitucion.setValue(sch.getCveInstitucion());
			Institucion.setAttributeNode(cveInstitucion);
			Attr nombreInstitucion = document.createAttribute("nombreInstitucion");
			nombreInstitucion.setValue(sch.getNombreInstitucion());
			Institucion.setAttributeNode(nombreInstitucion);
			root.appendChild(Institucion);
			Element Carrera = document.createElement("Carrera");
			Attr cveCarrera = document.createAttribute("cveCarrera");
			cveCarrera.setValue(car.getCve_carrera());
			Carrera.setAttributeNode(cveCarrera);
			Attr nombreCarrera = document.createAttribute("nombreCarrera");
			nombreCarrera.setValue(car.getNombre_carrera());
			Carrera.setAttributeNode(nombreCarrera);
			Attr fechaInicio = document.createAttribute("fechaInicio");
			fechaInicio.setValue(df.format(tit.getCarrera_fechaInicio()));
			Carrera.setAttributeNode(fechaInicio);
			Attr fechaTerminacion = document.createAttribute("fechaTerminacion");
			fechaTerminacion.setValue(df.format(tit.getCarrera_fechaTermino()));
			Carrera.setAttributeNode(fechaTerminacion);
			Attr idAutorizacionReconocimiento = document.createAttribute("idAutorizacionReconocimiento");
			idAutorizacionReconocimiento.setValue(car.getId_autorizacion_reconocimiento().toString());
			Carrera.setAttributeNode(idAutorizacionReconocimiento);
			Attr autorizacionReconocimiento = document.createAttribute("autorizacionReconocimiento");
			autorizacionReconocimiento.setValue(aut.getValue());
			Carrera.setAttributeNode(autorizacionReconocimiento);

			Attr numeroRvoe = document.createAttribute("numeroRvoe");
			numeroRvoe.setValue(car.getRvoe_dgp());
			Carrera.setAttributeNode(numeroRvoe);

			root.appendChild(Carrera);
			Element Profesionista = document.createElement("Profesionista");
			Attr curpProf = document.createAttribute("curp");
			curpProf.setValue(stu.getCurp());
			Profesionista.setAttributeNode(curpProf);
			Attr nombreProf = document.createAttribute("nombre");
			nombreProf.setValue(StringUtils.removeSpecialCharacters(stu.getNombre()));
			Profesionista.setAttributeNode(nombreProf);
			Attr primerApellidoProf = document.createAttribute("primerApellido");
			primerApellidoProf.setValue(StringUtils.removeSpecialCharacters(stu.getPrimerApellido()));
			Profesionista.setAttributeNode(primerApellidoProf);
			Attr segundoApellidoProf = document.createAttribute("segundoApellido");
			segundoApellidoProf.setValue(StringUtils.removeSpecialCharacters(stu.getSegundoApellido()));
			Profesionista.setAttributeNode(segundoApellidoProf);
			Attr correoElectronico = document.createAttribute("correoElectronico");
			correoElectronico.setValue(stu.getCorreoElectronico());
			Profesionista.setAttributeNode(correoElectronico);
			root.appendChild(Profesionista);
			Element Expedicion = document.createElement("Expedicion");
			Attr fechaExpedicion = document.createAttribute("fechaExpedicion");
			fechaExpedicion.setValue(df.format(tit.getExpedicion_fechaExpedicion()));
			Expedicion.setAttributeNode(fechaExpedicion);
			Attr idModalidadTitulacion = document.createAttribute("idModalidadTitulacion");
			idModalidadTitulacion.setValue(tit.getExpedicion_idModalidadTitulacion().toString());
			Expedicion.setAttributeNode(idModalidadTitulacion);
			Attr modalidadTitulacion = document.createAttribute("modalidadTitulacion");
			modalidadTitulacion.setValue(mod.getValue());
			Expedicion.setAttributeNode(modalidadTitulacion);
			
			if(tit.getExpedicion_idModalidadTitulacion()!=5)
			{
				Attr fechaExamenProfesional = document.createAttribute("fechaExamenProfesional");
				fechaExamenProfesional.setValue(df.format(tit.getExpedicion_fechaExamenProfesional()));
				Expedicion.setAttributeNode(fechaExamenProfesional);
			}
			else						
			{
				Attr fechaExencionExamenProfesional = document.createAttribute("fechaExencionExamenProfesional");
				fechaExencionExamenProfesional.setValue(df.format(tit.getExpedicion_fechaExencionExamenProfesional()));
				Expedicion.setAttributeNode(fechaExencionExamenProfesional);						
			}
			
			Attr cumplioServicioSocial = document.createAttribute("cumplioServicioSocial");
			cumplioServicioSocial.setValue(tit.getExpedicion_cumplioServicioSocial().toString());
			Expedicion.setAttributeNode(cumplioServicioSocial);
			Attr idFundamentoLegalServicioSocial = document.createAttribute("idFundamentoLegalServicioSocial");
			idFundamentoLegalServicioSocial.setValue(tit.getExpedicion_idFundamentoLegalServicioSocial().toString());
			Expedicion.setAttributeNode(idFundamentoLegalServicioSocial);
			Attr fundamentoLegalServicioSocial = document.createAttribute("fundamentoLegalServicioSocial");
			fundamentoLegalServicioSocial.setValue(fun.getValue());
			Expedicion.setAttributeNode(fundamentoLegalServicioSocial);
			Attr idEntidadFederativa = document.createAttribute("idEntidadFederativa");
			idEntidadFederativa.setValue(tit.getExpedicion_idEntidadFederativa()<10 ?"0"+tit.getExpedicion_idEntidadFederativa().toString():tit.getExpedicion_idEntidadFederativa().toString());
			Expedicion.setAttributeNode(idEntidadFederativa);
			Attr entidadFederativa = document.createAttribute("entidadFederativa");
			entidadFederativa.setValue(ent.getValue());
			Expedicion.setAttributeNode(entidadFederativa);

			root.appendChild(Expedicion);
			Element Antecedente = document.createElement("Antecedente");
			Attr institucionProcedencia = document.createAttribute("institucionProcedencia");
			institucionProcedencia.setValue(tit.getAntecedente_institucionProcedencia());
			Antecedente.setAttributeNode(institucionProcedencia);
			Attr idTipoEstudioAntecedente = document.createAttribute("idTipoEstudioAntecedente");
			idTipoEstudioAntecedente.setValue(tit.getAntecedente_idTipoEstudioAntecedente().toString());
			Antecedente.setAttributeNode(idTipoEstudioAntecedente);
			Attr tipoEstudioAntecedente = document.createAttribute("tipoEstudioAntecedente");
			tipoEstudioAntecedente.setValue(tip.getValue());
			Antecedente.setAttributeNode(tipoEstudioAntecedente);
			Attr idEntidadFederativaAntec = document.createAttribute("idEntidadFederativa");
			idEntidadFederativaAntec.setValue(tit.getAntecedente_idEntidadFederativa()<10?tit.getAntecedente_idEntidadFederativa().toString():tit.getAntecedente_idEntidadFederativa().toString());
			Antecedente.setAttributeNode(idEntidadFederativaAntec);
			Attr entidadFederativaAntec = document.createAttribute("entidadFederativa");
			entidadFederativaAntec.setValue(entAnt.getValue());
			Antecedente.setAttributeNode(entidadFederativaAntec);
			if (tit.getAntecedente_fechaInicio() != null) {
				Attr fechaInicioAntec = document.createAttribute("fechaInicio");
				fechaInicioAntec.setValue(df.format(tit.getAntecedente_fechaInicio()));
				Antecedente.setAttributeNode(fechaInicioAntec);
			}
			Attr fechaTerminacionAntec = document.createAttribute("fechaTerminacion");
			fechaTerminacionAntec.setValue(df.format(tit.getAntecedente_fechaTerminacion()));
			Antecedente.setAttributeNode(fechaTerminacionAntec);
			// El campo noCedula es opcional.
			root.appendChild(Antecedente);
			// create the xml file
			// transform the DOM Object to an XML File
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource domSource = new DOMSource(document);
			StreamResult streamResult = new StreamResult(new File(xmlFilePath));
			transformer.transform(domSource, streamResult);
			// EN ESTE MOMENTO YA TENEMOS EL XML COMPLETO, POR LO TANTO HAY QUE
			byte[] fileContent = FileUtils.readFileToByteArray(new File(xmlFilePath));
			String encodedString = Base64.encodeBase64String(fileContent);
			response.setMessage("XML generado");
			response.setStatus(true);
			response.setData(encodedString);
			response.setToken(jwtutil.refreshToken(token));
			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	private static byte[] toByteArray(String filePath) throws Exception {
		File f = new File(filePath);

		FileInputStream fis = new FileInputStream(f);

		byte[] fbytes = new byte[(int) f.length()];

		fis.read(fbytes);
		fis.close();

		return fbytes;
	}
}
