package com.ibm.services;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

public interface UsersService {
	public Response getAllUsers(@HeaderParam("token") String token);
	
	public Response getRoles(@HeaderParam("token") String token);
	
	public Response createRol(@HeaderParam("token") String token, String json);
	
	public Response getSchools(@HeaderParam("token") String token);

	public Response getUser(@HeaderParam("token") String token,int id);	
	
	public Response setSchool(@HeaderParam("token") String token,int id);	

	public Response deleteUser(@HeaderParam("token") String token,int id);

	public Response addUser(@HeaderParam("token") String token, String json);
	
	public Response updateUser(@HeaderParam("token") String token,@PathParam("id") int id, String json);

	public Response updateRol(@HeaderParam("token") String token,@PathParam("id") int id, String json);

	public Response getPermisos(@HeaderParam("token") String token);

	public Response getPermisosRol(@HeaderParam("token") String token,@PathParam("id") int id);

	public Response createPermisosRol(@HeaderParam("token") String token,@PathParam("id") int id, String json);
}
