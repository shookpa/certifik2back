package com.ibm.services;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.beanutils.PropertyUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

//import org.jose4j.json.internal.json_simple.JSONObject;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.ibm.dao.CatalogsDao;
import com.ibm.dao.LoginDao;
import com.ibm.dao.RolDao;
import com.ibm.dao.RolOperationDao;
import com.ibm.dao.UserDao;

import com.ibm.models.GenericResponse;
import com.ibm.models.PermisosRol;
import com.ibm.models.Rol;
import com.ibm.models.School;
import com.ibm.models.User;
import com.ibm.models.UserSchool;
import com.ibm.util.JWTUtil;
import com.lambdaworks.crypto.SCryptUtil;

@Path("/rest/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UsersServiceImpl implements UsersService {

	@Override
	@GET
	@Path("")
	public Response getAllUsers(@HeaderParam("token") String token) {
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			 return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		// validamos si el rol del usuario permite la ejecucion de esta
		// operacion:
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("id_rol", rol);
		filtro.put("id_permiso", 2);// Conforme al catalogo de permisos y
									// operaciones
		filtro.put("per_modulo", 1);
		// filtro.put("per_func1", 1); //CONFORME AL ID DE COLUMNA QUE
		// CORRESPONDA EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtro).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// aqui termina la validacion de rol y operacion
		UserDao userDao = new UserDao();
		String strJson = "";
		try {
			GenericResponse response = new GenericResponse();
			response.setData(userDao.getAllUsers());
			response.setMessage("Lista de datos de los usuarios");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@GET
	@Path("/roles")
	public Response getRoles(@HeaderParam("token") String token) {
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			 return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		// validamos si el rol del usuario permite la ejecucion de esta
		// operacion:
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("id_rol", rol);
		filtro.put("id_permiso", 9);// Conforme al catalogo de permisos y
									// operaciones
		filtro.put("per_modulo", 1);
		// filtro.put("per_func1", 1); //CONFORME AL ID DE COLUMNA QUE
		// CORRESPONDA EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtro).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// aqui termina la validacion de rol y operacion
		RolDao rolDao = new RolDao();
		String strJson = "";
		try {
			GenericResponse response = new GenericResponse();
			response.setData(rolDao.getAllRoles());
			response.setMessage("Lista de datos de los roles");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@GET
	@Path("/roles/{id}/permisos")
	public Response getPermisosRol(@HeaderParam("token") String token, @PathParam("id") int id) {
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			 return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		// validamos si el rol del usuario permite la ejecucion de esta
		// operacion:
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("id_rol", rol);
		filtro.put("id_permiso", 9);// Conforme al catalogo de permisos y
									// operaciones
		filtro.put("per_modulo", 1);
		// filtro.put("per_func1", 1); //CONFORME AL ID DE COLUMNA QUE
		// CORRESPONDA EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtro).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// aqui termina la validacion de rol y operacion
		CatalogsDao dao = new CatalogsDao();
		String strJson = "";
		try {
			GenericResponse response = new GenericResponse();
			Map<String, Object> fil = new HashMap<String, Object>();
			fil.put("id_rol", id);
			response.setData(dao.searchCatalog(new PermisosRol(), fil));
			response.setMessage("Lista de permisos del rol: " + id);
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@POST
	@Path("/roles/{id}/permisos")
	public Response createPermisosRol(@HeaderParam("token") String token, @PathParam("id") int id, String json) {
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			 return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		// validamos si el rol del usuario permite la ejecucion de esta
		// operacion:
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("id_rol", rol);
		filtro.put("id_permiso", 9);// Conforme al catalogo de permisos y
									// operaciones
		filtro.put("per_modulo", 1);
		filtro.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE CORRESPONDA
									// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtro).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// aqui termina la validacion de rol y operacion
		RolDao rolDao = new RolDao();
		CatalogsDao dao = new CatalogsDao();
		String strJson = "";
		JSONParser parser = new JSONParser();

		org.json.JSONObject jo = new org.json.JSONObject(json);
		try {
			GenericResponse response = new GenericResponse();

			// TENDRÁ QUE ACTUALIZAR LOS REGISTROS DE PERMISOS DE ACUERDO AL ID
			// DE ROL Y DE PERMISO:

			JSONArray arr = jo.getJSONArray("permisos");
			for (int i = 0; i < arr.length(); i++) {
				org.json.JSONObject perObj = arr.getJSONObject(i);
				Map<String, Object> fil = new HashMap<String, Object>();
				fil.put("id_permiso", perObj.getInt("id_permiso"));
				fil.put("id_rol", id);
				List resultados = dao.searchCatalog(new PermisosRol(), fil);
				PermisosRol permRol = (PermisosRol) resultados.get(0);
				permRol.setId_permiso(perObj.getInt("id_permiso"));
				permRol.setId_rol(id);
				permRol.setPer_modulo(perObj.getInt("per_modulo"));
				permRol.setPer_func1(
						perObj.get("per_func1").toString().equals("null") ? null : perObj.getInt("per_func1"));
				permRol.setPer_func2(
						perObj.get("per_func2").toString().equals("null") ? null : perObj.getInt("per_func2"));
				permRol.setPer_func3(
						perObj.get("per_func3").toString().equals("null") ? null : perObj.getInt("per_func3"));
				permRol.setPer_func4(
						perObj.get("per_func4").toString().equals("null") ? null : perObj.getInt("per_func4"));
				permRol.setPer_func5(
						perObj.get("per_func5").toString().equals("null") ? null : perObj.getInt("per_func5"));
				permRol.setPer_func6(
						perObj.get("per_func6").toString().equals("null") ? null : perObj.getInt("per_func6"));
				permRol.setPer_func7(
						perObj.get("per_func7").toString().equals("null") ? null : perObj.getInt("per_func7"));
				permRol.setPer_func8(
						perObj.get("per_func8").toString().equals("null") ? null : perObj.getInt("per_func8"));
				permRol.setPer_func9(
						perObj.get("per_func9").toString().equals("null") ? null : perObj.getInt("per_func9"));
				permRol.setPer_func10(
						perObj.get("per_func10").toString().equals("null") ? null : perObj.getInt("per_func10"));
				permRol.setPer_func11(
						perObj.get("per_func11").toString().equals("null") ? null : perObj.getInt("per_func11"));
				permRol.setPer_func12(
						perObj.get("per_func12").toString().equals("null") ? null : perObj.getInt("per_func12"));
				permRol.setPer_func13(
						perObj.get("per_func13").toString().equals("null") ? null : perObj.getInt("per_func13"));
				permRol.setPer_func14(
						perObj.get("per_func14").toString().equals("null") ? null : perObj.getInt("per_func14"));
				permRol.setPer_func15(
						perObj.get("per_func15").toString().equals("null") ? null : perObj.getInt("per_func15"));
				permRol.setPer_func16(
						perObj.get("per_func16").toString().equals("null") ? null : perObj.getInt("per_func16"));
				permRol.setPer_func17(
						perObj.get("per_func17").toString().equals("null") ? null : perObj.getInt("per_func17"));
				permRol.setPer_func18(
						perObj.get("per_func18").toString().equals("null") ? null : perObj.getInt("per_func18"));

				dao.updateRecord(permRol);

			}

			response.setData(null);
			response.setMessage("Permisos Actualizados");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@GET
	@Path("/permisos")
	public Response getPermisos(@HeaderParam("token") String token) {
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		// validamos si el rol del usuario permite la ejecucion de esta
		// operacion:
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("id_rol", rol);
		filtro.put("id_permiso", 9);// Conforme al catalogo de permisos y
									// operaciones
		filtro.put("per_modulo", 1);
		// filtro.put("per_func1", 1); //CONFORME AL ID DE COLUMNA QUE
		// CORRESPONDA EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtro).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// aqui termina la validacion de rol y operacion
		RolDao rolDao = new RolDao();
		String strJson = "";
		try {
			GenericResponse response = new GenericResponse();
			response.setData(rolDao.getAllPermisos());
			response.setMessage("Lista de datos de los permisos");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@POST
	@Path("/roles")
	public Response createRol(@HeaderParam("token") String token, String json) {
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		// validamos si el rol del usuario permite la ejecucion de esta
		// operacion:
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("id_rol", rol);
		filtro.put("id_permiso", 9);// Conforme al catalogo de permisos y
									// operaciones
		filtro.put("per_modulo", 1);
		filtro.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE CORRESPONDA
									// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtro).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// aqui termina la validacion de rol y operacion
		RolDao rolDao = new RolDao();
		CatalogsDao dao = new CatalogsDao();
		String strJson = "";
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;
		try {
			GenericResponse response = new GenericResponse();
			String desc_rol = jo.get("desc_rol").toString();
			Map<String, Object> fil = new HashMap<String, Object>();
			fil.put("desc_rol", desc_rol);
			List resultados = dao.searchCatalog(new Rol(), fil);
			Rol rolEntity = new Rol();
			if (resultados.size() == 0) {
				rolEntity.setStatus(1);
				rolEntity.setDesc_rol(desc_rol);
				rolDao.setRol(rolEntity);
			} else {
				rolEntity = (Rol) resultados.get(0);
			}

			response.setData(null);
			response.setMessage("Rol agregado: " + rolEntity.getDesc_rol());
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@PUT
	@Path("/roles/{id}")
	public Response updateRol(@HeaderParam("token") String token, @PathParam("id") int id, String json) {
		GenericResponse response = new GenericResponse();

		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		// validamos si el rol del usuario permite la ejecucion de esta
		// operacion:
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("id_rol", rol);
		filtro.put("id_permiso", 9);// Conforme al catalogo de permisos y
									// operaciones
		filtro.put("per_modulo", 1);
		filtro.put("per_func2", 1); // CONFORME AL ID DE COLUMNA QUE CORRESPONDA
									// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtro).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// aqui termina la validacion de rol y operacion
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Bad request");
			response.setErrorCode("");
			return Response.status(404).entity(response).build();
		}

		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		String tokenDato = "";
		try {
			CatalogsDao dao = new CatalogsDao();

			String desc_rol = jo.get("desc_rol").toString();

			Map<String, Object> filtro2 = new HashMap<String, Object>();

			Rol rolEntity = new Rol();

			filtro2 = new HashMap<String, Object>();

			filtro2.put("id_rol", id);

			List resultados = dao.searchCatalog(new Rol(), filtro2);
			if (resultados.size() == 0) {
				response.setStatus(false);
				response.setMessage("No existe el id de rol: " + id);
				response.setErrorCode("");
				response.setToken(jwtutil.refreshToken(token));
				return Response.status(404).entity(response).build();
			}

			rolEntity.setDesc_rol(desc_rol);
			rolEntity.setId_rol(id);
			dao.updateRecord(rolEntity);

			response.setData(null);
			response.setMessage("Rol actualizado");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));

			tokenDato = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(tokenDato).build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@GET
	@Path("/{id}")
	public Response getUser(@HeaderParam("token") String token, @PathParam("id") int id) {

		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		// validamos si el rol del usuario permite la ejecucion de esta
		// operacion:
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("id_rol", rol);
		filtro.put("id_permiso", 2);// Conforme al catalogo de permisos y
									// operaciones
		filtro.put("per_modulo", 1);
//		filtro.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE CORRESPONDA
									// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtro).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// aqui termina la validacion de rol y operacion
	

		UserDao userDao = new UserDao();
		CatalogsDao catalogDao = new CatalogsDao();
		String strJson = "";
		try {
			GenericResponse response = new GenericResponse();
			Map<String, Object> resp = new HashMap<String, Object>();
			resp.put("userData", userDao.getUser(id));
			filtro = new HashMap<String, Object>();
			filtro.put("id_usuario", id);
			resp.put("escuelas", catalogDao.searchCatalog(new UserSchool(), filtro));
			response.setData(resp);
			response.setMessage("Datos del usuario");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));

			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@GET
	@Path("/schools")
	public Response getSchools(@HeaderParam("token") String token) {

		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		// IDENTIFICACION DEL ROL
//		int rol = jwtutil.getRolFromToken(token);
		int idUsuario = jwtutil.getIdUsuarioFromToken(token);
//		RolOperationDao rod = new RolOperationDao();

//		filtro.put("id_permiso", 2);// Conforme al catalogo de permisos y
//									// operaciones
//		filtro.put("per_modulo", 1);
//		filtro.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE CORRESPONDA
									// EN LA FUNCIONALIDAD
//		if (rod.searchRolOperation(filtro).length == 0)
//			return Response.status(Response.Status.UNAUTHORIZED).build();
		//ESTA OPERACIÓN CUALQUIER USUARIO DEBE PODER CONSUMIRLA
		
		UserDao userDao = new UserDao();
		String strJson = "";
		try {
			GenericResponse response = new GenericResponse();
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("id_usuario", idUsuario);
			response.setData(userDao.getSchools(filtro));
			response.setMessage("Lista de instituciones del Usuario");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));

			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@GET
	@Path("/schools/set/{id}")
	public Response setSchool(@HeaderParam("token") String token, @PathParam("id") int id) {

		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		String strJson = "";
		try {
			GenericResponse response = new GenericResponse();
			response.setData(null);
			response.setMessage("La sesion del usuario ha pasado a la escuela:" + id);
			response.setStatus(true);
			response.setToken(jwtutil.refreshTokenWithSchool(token, id));

			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@POST
	@Path("")
	public Response addUser(@HeaderParam("token") String token, String json) {
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			 return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtroRol = new HashMap<String, Object>();
		filtroRol.put("id_rol", rol);
		filtroRol.put("id_permiso", 2);// Conforme al catalogo de permisos y
									// operaciones
		filtroRol.put("per_modulo", 1);
		filtroRol.put("per_func1", 1); // CONFORME AL ID DE COLUMNA QUE CORRESPONDA
									// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtroRol).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL

		GenericResponse response = new GenericResponse();
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("Faltan criterios de busqueda");
			response.setErrorCode("Codigo pendiente");
			return Response.status(404).entity(response).build();
		}
		UserDao userDao = new UserDao();

		String strJson = "";
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// JSONObject jo = (JSONObject) obj;
		org.json.JSONObject jo = new org.json.JSONObject(json);

		/**
		 * Comienzo de la seccion de busqueda de duplicados del campo user:
		 */
		System.out.println("el json enviado es " + json);
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("user", jo.get("user")); // MISMO CASO SOLAMENTE BUSCAMOS EN
											// BASE AL USER
		User[] us = userDao.searchUser(filtro); // EL RESULTADO DE LA BUSQUEDA
												// ESTA EN ESE ARRAY

		if (us.length > 0) {
			response.setToken(jwtutil.refreshToken(token));
			response.setStatus(false);
			response.setMessage("User Already Exists because de duplicated user");
			response.setErrorCode("EC-01");
			return Response.status(422).entity(response).build();
		}
		/**
		 * Fin de la seccion de busqueda de duplicados del campo user
		 */
		/**
		 * Comienzo de la seccion de busqueda de duplicados del campo email:
		 */

		Map<String, Object> filtro2 = new HashMap<String, Object>();
		filtro2.put("email", jo.get("email"));
		User[] us2 = userDao.searchUser(filtro2);

		if (us2.length > 0) {
			response.setToken(jwtutil.refreshToken(token));
			response.setStatus(false);
			response.setMessage("User Already Exists because the duplicated email");
			response.setErrorCode("EC-01");
			return Response.status(422).entity(response).build();
		}

		/**
		 * Fin de la seccion de busqueda de duplicados del campo email
		 */
		try {
			User user = new User();
			user.setUser(jo.get("user").toString());
			user.setNombre(jo.get("nombre").toString());
			user.setA_materno(jo.get("a_materno").toString());
			user.setA_paterno(jo.get("a_paterno").toString());
			user.setPassword(SCryptUtil.scrypt(jo.get("password").toString(), 16, 16, 16));
			user.setEmail(jo.get("email").toString());
			user.setId_licencia(Integer.parseInt(jo.get("id_licencia").toString()));
			user.setRol(Integer.parseInt(jo.get("rol").toString()));
			user.setStatus(1);
			userDao.setUser(user);
			//INSTITUCIONES SERÁN LOS PROYECTOS, ALUMNOS SERAN LAS APPS.
			JSONArray arrInst = jo.getJSONArray("instituciones");
			for (int i = 0; i < arrInst.length(); i++) {
				int post_id = arrInst.getInt(i);
				// System.out.println("Veamos las instituciones"+post_id);
				UserSchool userSch = new UserSchool();
				userSch.setId_usuario(user.getId_usuario());
				userSch.setId_institucion(post_id);
				userDao.setUserSchool(userSch);
			}
			response.setMessage("Usuario creado exitosamente");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));
			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);
			return Response.ok(strJson).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@Override
	@PUT
	@Path("/{id}")
	public Response updateUser(@HeaderParam("token") String token, @PathParam("id") int id, String json) {
		// igual que siempre, primero hay que verificar que este entrando al
		// servicio, quitamos la validacion del token para que no tengamos que
		// mandar un token valido:
		System.out.println("Estamos en la operacion update con el json:" + json);// EL
																					// JSON
																					// COMPLETO
																					// SE
																					// IMPRIME
																					// AQUI
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			 return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("id_rol", rol);
		filtro.put("id_permiso", 2);// Conforme al catalogo de permisos y
									// operaciones
		filtro.put("per_modulo", 1);
		filtro.put("per_func2", 1); // CONFORME AL ID DE COLUMNA QUE CORRESPONDA
									// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtro).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		GenericResponse response = new GenericResponse();
		if (json.trim() == "") {
			response.setStatus(false);
			response.setMessage("No se ingresaron todos los campos");
			response.setErrorCode("Codigo pendiente");
			return Response.status(404).entity(response).build();
		}
		UserDao userDao = new UserDao();

		String strJson = "";
		JSONParser parser = new JSONParser();
		Object obj = null;

		try {
			obj = parser.parse(json);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;
		org.json.JSONObject jo2 = new org.json.JSONObject(json);

		if (userDao.getUser(id) == null) {
			response.setStatus(false);
			response.setMessage("User Doesn't Exist");
			response.setErrorCode("EC-02");
			return Response.status(404).entity(response).build();
		}
		String opcion = "";
		boolean op = true;

		try { // nombre, a_paterno, a_materno, password, user, email,rol ,
				// status e id_usario
			User user = userDao.getUser(id);
			Iterator entries = jo.entrySet().iterator();
			while (entries.hasNext()) {
				Map.Entry entry = (Map.Entry) entries.next();
				System.out.println("Veamos lo que llega: " + entry.getKey().toString() + " -- " + entry.getValue());
				try {
					if (entry.getKey().toString().equalsIgnoreCase("password"))
						PropertyUtils.setSimpleProperty(user, entry.getKey().toString(),
								SCryptUtil.scrypt(jo.get("password").toString(), 16, 16, 16));
					else if (entry.getKey().toString().equalsIgnoreCase("rol")
							|| entry.getKey().toString().equalsIgnoreCase("status")
							|| entry.getKey().toString().equalsIgnoreCase("id_licencia"))
						PropertyUtils.setSimpleProperty(user, entry.getKey().toString(),
								Integer.valueOf(entry.getValue().toString()));
					else
						PropertyUtils.setSimpleProperty(user, entry.getKey().toString(), entry.getValue());
				} catch (Exception e) {
					System.out.println("CAYO EN EXCEPCION: " + entry.getKey().toString() + " -- " + entry.getValue());
					e.printStackTrace();
				}
			}
			System.out.println("Veamos el usuario a punto de actualizar:" + user);
			userDao.updateUser(user);
			// DEBEMOS ELIMINAR LAS INSTITUCIONES QUE TENGAN ASOCIADAS, PARA
			// DESPUES VOLVER A INSERTARLAS:
			userDao.deleteUserSchools("UserSchool", "id_usuario", String.valueOf(user.getId_usuario()));
			JSONArray arrInst = jo2.getJSONArray("instituciones");
			for (int i = 0; i < arrInst.length(); i++) {
				int post_id = arrInst.getInt(i);
				// System.out.println("Veamos las instituciones"+post_id);
				UserSchool userSch = new UserSchool();
				userSch.setId_usuario(user.getId_usuario());
				userSch.setId_institucion(post_id);
				userDao.setUserSchool(userSch);
			}

			response.setMessage("Usuario actualizado exitosamente");
			response.setStatus(true);
			response.setToken(jwtutil.refreshToken(token));

			strJson = (new org.codehaus.jackson.map.ObjectMapper()).writeValueAsString(response);

		} catch (Exception e) {
			System.out.println(e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok(strJson).build();

	}

	@Override
	@DELETE
	@Path("/{id}/delete")
	public Response deleteUser(@HeaderParam("token") String token, @PathParam("id") int id) {
		if (token == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		JWTUtil jwtutil = new JWTUtil();
		if (!jwtutil.validateToken(token)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		// IDENTIFICACION DEL ROL
		int rol = jwtutil.getRolFromToken(token);
		RolOperationDao rod = new RolOperationDao();
		Map<String, Object> filtro = new HashMap<String, Object>();
		
		filtro.put("id_rol", rol);
		filtro.put("id_permiso", 2);// Conforme al catalogo de permisos y
									// operaciones
		filtro.put("per_modulo", 1);
		filtro.put("per_func3", 1); // CONFORME AL ID DE COLUMNA QUE CORRESPONDA
									// EN LA FUNCIONALIDAD
		if (rod.searchRolOperation(filtro).length == 0)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		// FIN DE IDENTIFICACION DE ROL
		GenericResponse response = new GenericResponse();
		User user = new User();

		UserDao userDao = new UserDao();
		user = userDao.getUser(id);

		if (user == null) {
			response.setData(null);
			response.setToken(jwtutil.refreshToken(token));
			response.setStatus(false);
			response.setMessage("el usuario no existe");
			response.setErrorCode("EC-02");
			return Response.status(404).entity(response).build();
		}
		userDao.deleteUser(user);
		response.setStatus(true);
		response.setMessage("Usuario eliminado exitosamente");
		response.setToken(jwtutil.refreshToken(token));
		return Response.status(200).entity(response).build();
	}
}
