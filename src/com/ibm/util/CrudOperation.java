package com.ibm.util;

import java.io.File;
import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.internal.util.config.ConfigurationException;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;





//import com.ibm.selenium.pojo.Incidenciartc;

public  class CrudOperation {
	private Session session;
	static Logger logger = LoggerFactory.getLogger(CrudOperation.class);
	public CrudOperation() {
		   if (session == null || !session.isOpen() || !session.isConnected()) {
			   logger.info("Veamos el session {}", session);	
			   session = HibernateUtil.getSessionFactory().openSession();
		   }
		

	}
	public Session getSession()
	{
//		logger.info("Veamos el session {}", session);
		
		if (session == null || !session.isOpen() || !session.isConnected() ) {			
		   session = HibernateUtil.getSessionFactory().openSession();
	   }
		return session;
		
	}

	public List<Object> getList(Object pojito, String filtro) {
		Transaction tx = getSession().beginTransaction();
		List<Object> listaResultados = getSession().createQuery("from " + pojito.toString().split("\\.")[pojito.toString().split("\\.").length - 1] + " " + filtro).list();
		cierra();
//		cierra();HibernateUtil.shutdown();
		getSession().getTransaction().commit();
		return listaResultados;
	}
	public List<Object[]> query(String query) {
		Transaction tx = getSession().beginTransaction();
		List<Object[]> listaResultados = getSession().createSQLQuery(query).list();
		cierra();
//		cierra();HibernateUtil.shutdown();
		getSession().getTransaction().commit();
		//Iterator it = listaResultados.iterator();		
		return listaResultados;
	}

	public List<Object> search(Object pojito, Map<String, Object> filtro) {
		Transaction tx = getSession().beginTransaction();
		List listaResultados = null;
		CriteriaBuilder builder = getSession().getCriteriaBuilder();
		CriteriaQuery criteria = builder.createQuery(pojito.getClass());
		Root root = criteria.from(pojito.getClass());
		criteria.select(root);
		List<Predicate> predList = new LinkedList<Predicate>();
		if (filtro != null) {
			for (Map.Entry<String, Object> entry : filtro.entrySet()) {
				if ((root.get(entry.getKey()).getJavaType()).toString().equalsIgnoreCase("class java.lang.String"))
					predList.add(builder.like(root.get(entry.getKey()),"%"+ entry.getValue().toString()+"%"));
				else
					predList.add(builder.equal(root.get(entry.getKey()), entry.getValue()));
				
			}
		}
		Predicate[] predArray = new Predicate[predList.size()];
		predList.toArray(predArray);
		criteria.where(predArray);
		
		System.out.println("Query ejecutado:    "+getSession().createQuery(criteria).getQueryString());
		listaResultados = getSession().createQuery(criteria).getResultList();
		Iterator it = listaResultados.iterator();
//		System.out.println("A punto de imprimir los resultados con criteria:" + listaResultados.size());
//
//		while (it.hasNext()) {
//			System.out.println(it.next());
//
//		}
		//tx.commit();
		getSession().getTransaction().commit();
		cierra();
//		cierra();HibernateUtil.shutdown();
		return listaResultados;
	}
	public List<Object> searchWithDate(Object pojito, Map<String, Object> filtro, Date ini, Date fin) {
		Transaction tx = getSession().beginTransaction();
		List listaResultados = null;
		CriteriaBuilder builder = getSession().getCriteriaBuilder();
		CriteriaQuery criteria = builder.createQuery(pojito.getClass());
		Root root = criteria.from(pojito.getClass());
		criteria.select(root);
		List<Predicate> predList = new LinkedList<Predicate>();
		if (filtro != null) {
			for (Map.Entry<String, Object> entry : filtro.entrySet()) {
				if ((root.get(entry.getKey()).getJavaType()).toString().equalsIgnoreCase("class java.lang.String"))
					predList.add(builder.like(root.get(entry.getKey()),"%"+ entry.getValue().toString()+"%"));
				else
					if ((root.get(entry.getKey()).getJavaType()).toString().equalsIgnoreCase("class java.util.Date"))
					{
						predList.add(builder.between(root.get(entry.getKey()), ini, fin));
					}else
					{
						predList.add(builder.equal(root.get(entry.getKey()), entry.getValue()));
					}
						
				
			}
		}
		Predicate[] predArray = new Predicate[predList.size()];
		predList.toArray(predArray);
		criteria.where(predArray);
		
		System.out.println("Query ejecutado:    "+getSession().createQuery(criteria).getQueryString());
		listaResultados = getSession().createQuery(criteria).getResultList();
		Iterator it = listaResultados.iterator();
//		System.out.println("A punto de imprimir los resultados con criteria:" + listaResultados.size());
//
//		while (it.hasNext()) {
//			System.out.println(it.next());
//
//		}
		//tx.commit();
		getSession().getTransaction().commit();
		cierra();
//		cierra();HibernateUtil.shutdown();
		return listaResultados;
	}
	public List<Object> search(Object pojito, Map<String, Object> filtro, String orderBy) {
		Transaction tx = getSession().beginTransaction();
		List listaResultados = null;
		CriteriaBuilder builder = getSession().getCriteriaBuilder();
		CriteriaQuery criteria = builder.createQuery(pojito.getClass());
		Root root = criteria.from(pojito.getClass());
		criteria.select(root);
		List<Predicate> predList = new LinkedList<Predicate>();
		if (filtro != null) {
			for (Map.Entry<String, Object> entry : filtro.entrySet()) {
				if ((root.get(entry.getKey()).getJavaType()).toString().equalsIgnoreCase("class java.lang.String"))
					predList.add(builder.like(root.get(entry.getKey()),"%"+ entry.getValue().toString()+"%"));
				else
					predList.add(builder.equal(root.get(entry.getKey()), entry.getValue()));
				
			}
		}
		Predicate[] predArray = new Predicate[predList.size()];
		predList.toArray(predArray);
		criteria.where(predArray);
//		criteria.addOrder(Order.asc(orderBy));
		criteria.orderBy(builder.asc(root.get(orderBy)));
		System.out.println("Query ejecutado:    "+getSession().createQuery(criteria).getQueryString());
		listaResultados = getSession().createQuery(criteria).getResultList();
		Iterator it = listaResultados.iterator();
//		System.out.println("A punto de imprimir los resultados con criteria:" + listaResultados.size());
//
//		while (it.hasNext()) {
//			System.out.println(it.next());
//
//		}
		//tx.commit();
		getSession().getTransaction().commit();
		cierra();
//		cierra();HibernateUtil.shutdown();
		return listaResultados;
	}
	public List<Object> searchExact(Object pojito, Map<String, Object> filtro) {
		Transaction tx = getSession().beginTransaction();
		List listaResultados = null;
		CriteriaBuilder builder = getSession().getCriteriaBuilder();
		CriteriaQuery criteria = builder.createQuery(pojito.getClass());
		Root root = criteria.from(pojito.getClass());
		criteria.select(root);
		List<Predicate> predList = new LinkedList<Predicate>();
		if (filtro != null) {
			for (Map.Entry<String, Object> entry : filtro.entrySet()) {
				if ((root.get(entry.getKey()).getJavaType()).toString().equalsIgnoreCase("class java.lang.String"))
					predList.add(builder.like(root.get(entry.getKey()), entry.getValue().toString()));
				else
					predList.add(builder.equal(root.get(entry.getKey()), entry.getValue()));
				
			}
		}
		Predicate[] predArray = new Predicate[predList.size()];
		predList.toArray(predArray);
		criteria.where(predArray);
		
		System.out.println("Query ejecutado:    "+getSession().createQuery(criteria).getQueryString());
		listaResultados = getSession().createQuery(criteria).getResultList();
		Iterator it = listaResultados.iterator();
//		System.out.println("A punto de imprimir los resultados con criteria:" + listaResultados.size());
//
//		while (it.hasNext()) {
//			System.out.println(it.next());
//
//		}
		getSession().getTransaction().commit();
		cierra();
//		cierra();HibernateUtil.shutdown();
		return listaResultados;
	}
	public List<Object> searchPaginated(Object pojito, Map<String, Object> filtro, int limit, int offset) {
		Transaction tx = getSession().beginTransaction();
		List listaResultados = null;
		CriteriaBuilder builder = getSession().getCriteriaBuilder();
		CriteriaQuery criteria = builder.createQuery(pojito.getClass());
		Root root = criteria.from(pojito.getClass());
		criteria.select(root);
		List<Predicate> predList = new LinkedList<Predicate>();
		if (filtro != null) {
			for (Map.Entry<String, Object> entry : filtro.entrySet()) {
				if ((root.get(entry.getKey()).getJavaType()).toString().equalsIgnoreCase("class java.lang.String"))
					predList.add(builder.like(root.get(entry.getKey()),"%"+ entry.getValue().toString()+"%"));
				else
					predList.add(builder.equal(root.get(entry.getKey()), entry.getValue()));
				
			}
		}
		Predicate[] predArray = new Predicate[predList.size()];
		predList.toArray(predArray);
		criteria.where(predArray);
		
		System.out.println("Query ejecutado:    "+getSession().createQuery(criteria).setFirstResult(offset).setMaxResults(limit).getQueryString());
		listaResultados = getSession().createQuery(criteria).setFirstResult(offset).setMaxResults(limit).getResultList();
		Iterator it = listaResultados.iterator();
//		System.out.println("A punto de imprimir los resultados con criteria:" + listaResultados.size());
//
//		while (it.hasNext()) {
//			System.out.println(it.next());
//
//		}
		getSession().getTransaction().commit();
		cierra();
//		cierra();HibernateUtil.shutdown();
		return listaResultados;
	}
	public Object searchRow(Object pojito, Map<String, Object> filtro) {
		Transaction tx = getSession().beginTransaction();
		List listaResultados = null;
		CriteriaBuilder builder = getSession().getCriteriaBuilder();
		CriteriaQuery criteria = builder.createQuery(pojito.getClass());
		Root root = criteria.from(pojito.getClass());
		criteria.select(root);
		List<Predicate> predList = new LinkedList<Predicate>();
		if (filtro != null) {
			for (Map.Entry<String, Object> entry : filtro.entrySet()) {
				if ((root.get(entry.getKey()).getJavaType()).toString().equalsIgnoreCase("class java.lang.String"))
					predList.add(builder.like(root.get(entry.getKey()), entry.getValue().toString()));
				else
					predList.add(builder.equal(root.get(entry.getKey()), entry.getValue()));
				
			}
		}
		Predicate[] predArray = new Predicate[predList.size()];
		predList.toArray(predArray);
		criteria.where(predArray);
		System.out.println("Query ejecutadp:    "+getSession().createQuery(criteria).getQueryString());
		listaResultados = getSession().createQuery(criteria).getResultList();
		Iterator it = listaResultados.iterator();
//		System.out.println("A punto de imprimir los resultados con criteria:" + listaResultados.size());
//
//		while (it.hasNext()) {
//			System.out.println(it.next());
//
//		}
		getSession().getTransaction().commit();
		cierra();
//		cierra();HibernateUtil.shutdown();
		return listaResultados.get(0);
	}

	public void inserta(Object pojito) {
		try {

			getSession().beginTransaction();
			// persists two new Contact objects
			getSession().persist(pojito);
//			getSession().flush();
			getSession().getTransaction().commit();
			

		} catch (ConfigurationException ce) {

			System.out.println(ce.getMessage());

		} finally
		{
			cierra();
//			cierra() ;
		}

	}

	public void CambiaEstatus(Object pojito) {
		try {

//			Session session = HibernateUtil.getSessionFactory().openSession();
			getSession().beginTransaction();
			// persists two new Contact objects
			getSession().persist(pojito);
			getSession().getTransaction().commit();

		} catch (ConfigurationException ce) {

			System.out.println(ce.getMessage());

		}
		finally
		{
			cierra();
			
//			cierra();HibernateUtil.shutdown();
		}

	}

	public void cierra() {
//		session.close();
//		HibernateUtil.shutdown();
//		HibernateUtil.getSessionFactory().close();
	}
	static void fullClose()
	{
//		HibernateUtil.getSessionFactory().close();
//		HibernateUtil.shutdown();		
	}
	public void closeSession()
	{
//		this.session.close();
	}
	public void actualizaEstatusRegistro(String tabla, int status, String idRTC) {
		
		getSession().beginTransaction();
		Query query = getSession().createQuery("update " + tabla + " set estatusRegistro = :status" + " where id = :Id");
		query.setParameter("status", status);
		query.setParameter("Id", idRTC);
		int result = query.executeUpdate();
		getSession().getTransaction().commit();
		cierra();
//		cierra();HibernateUtil.shutdown();
	}

	public void actualiza(Object pojito) {
		try {
			Transaction tx = getSession().beginTransaction();

			getSession().update(pojito);
			getSession().getTransaction().commit();

		} catch (ConfigurationException ce) {

			System.out.println(ce.getMessage());
		}finally
		{
			cierra();
//			cierra();HibernateUtil.shutdown();
		}

	}

	public void borra(Object pojito) {

		try {
			// Employee emp=(Employee)pojito;
			Transaction tx = getSession().beginTransaction();
			getSession().delete(pojito);
			getSession().getTransaction().commit();
		} catch (ConfigurationException ce) {
			System.out.println(ce.getMessage());
		}finally
		{
			cierra();
//			cierra();HibernateUtil.shutdown();
		}

	}
	public void borra(String tabla, String campo, String valor) {

		try {
			// Employee emp=(Employee)pojito;
			Transaction tx = getSession().beginTransaction();
			
			
//			Query query = session.createQuery("Delete " + pojito.getClass()+ " WHERE "+campo+"= :valorCampo");			
//			query.setParameter("valorCampo", valor);
			System.out.println("delete FROM  " + tabla+ " where "+campo+"= "+valor);
			Query q = getSession().createQuery("delete FROM  " + tabla + " where "+campo+"= "+valor);
			q.executeUpdate();
//			int result = query.executeUpdate();
			getSession().getTransaction().commit();
		} catch (ConfigurationException ce) {
			System.out.println(ce.getMessage());
		}finally
		{
			cierra();
//			cierra();HibernateUtil.shutdown();
		}

	}

	public boolean deleteById(Class<?> type, Serializable id) {
		Object persistentInstance = getSession().load(type, id);
		if (persistentInstance != null) {
			getSession().delete(persistentInstance);
			
			cierra();
//			cierra();HibernateUtil.shutdown();
			return true;
		}
		return false;
	}

	public Object obtieneDatosPojo(Object pojito, String llave, String tipo) {
		Transaction tx = getSession().beginTransaction();
		Object respuesta=getSession().get(pojito.getClass(), tipo.equals("String") ? llave : Integer.valueOf(llave));
		cierra();
		getSession().getTransaction().commit();
//		cierra();HibernateUtil.shutdown();
		return respuesta;

	}

}
