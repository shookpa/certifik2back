package com.ibm.util;

import java.security.Key;
import java.util.LinkedList;
import java.util.List;

import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwa.AlgorithmConstraints.ConstraintType;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwk.JsonWebKeySet;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.jwt.consumer.JwtContext;
import org.jose4j.lang.JoseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JWTUtil {
	static Logger logger = LoggerFactory.getLogger(JWTUtil.class);
	static List<JsonWebKey> jwkList = null;

	static {

		logger.info("Inside static initializer...");
		jwkList = new LinkedList<>();
		for (int kid = 1; kid <= 3; kid++) {
			JsonWebKey jwk = null;
			try {
				jwk = RsaJwkGenerator.generateJwk(2048);
				logger.info("PUBLIC KEY (" + kid + "): " + jwk.toJson(JsonWebKey.OutputControlLevel.PUBLIC_ONLY));

			} catch (JoseException e) {
				e.printStackTrace();
			}
			jwk.setKeyId(String.valueOf(kid));
			jwkList.add(jwk);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String user = "adiazkim"; // simulando que el usuario logueado es
									// abraham
		Integer rol = 1; // simulando que es el rol 1
		String jwt = "";
		JWTUtil test = new JWTUtil();
		jwt = test.generateToken(user, rol, 1, "test");
		System.out.println("el token generado es:  ");
		System.out.println(jwt);

		System.out.println("Verificando el token recibido:  ");
		Boolean tokenValido = test.validateToken(jwt);

		System.out.println("Obteniendo datos del token:");
		Integer rolObtenido = test.getRolFromToken(jwt);
		String userObtenido = test.getUserFromToken(jwt);
		System.out.println("Datos obtenidos del jwt, user: " + userObtenido + ", y su rol es:" + rolObtenido);
	}

	public Integer getRolFromToken(String jwt) {
		Integer rol = 0;
		try {			
			JwtConsumer firstPassJwtConsumer = new JwtConsumerBuilder().setSkipAllValidators().setDisableRequireSignature().setSkipSignatureVerification().build();
			JwtContext jwtContext;
			jwtContext = firstPassJwtConsumer.process(jwt);		
			rol = Integer.parseInt(jwtContext.getJwtClaims().getClaimValue("rol").toString());
			logger.info("El rol obtenido del jwt: {}", jwtContext.getJwtClaims().getClaimValue("rol"));
		} catch (InvalidJwtException e) {
			e.printStackTrace();
		}
		return rol;

	}
	public Integer getInstitucionFromToken(String jwt) {
		Integer id_ins = 0;
		try {			
			JwtConsumer firstPassJwtConsumer = new JwtConsumerBuilder().setSkipAllValidators().setDisableRequireSignature().setSkipSignatureVerification().build();
			JwtContext jwtContext;
			jwtContext = firstPassJwtConsumer.process(jwt);		
			id_ins = Integer.parseInt(jwtContext.getJwtClaims().getClaimValue("idInstitucion").toString());
			logger.info("La institución obtenida del jwt: {}", jwtContext.getJwtClaims().getClaimValue("idInstitucion"));
		} catch (InvalidJwtException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return id_ins;

	}
	public Integer getIdUsuarioFromToken(String jwt) {
		Integer id_usuario = 0;
		try {			
			JwtConsumer firstPassJwtConsumer = new JwtConsumerBuilder().setSkipAllValidators().setDisableRequireSignature().setSkipSignatureVerification().build();
			JwtContext jwtContext;
			jwtContext = firstPassJwtConsumer.process(jwt);		
			id_usuario = Integer.parseInt(jwtContext.getJwtClaims().getClaimValue("id_usuario").toString());
			logger.info("El id_usuario obtenido del jwt: {}", jwtContext.getJwtClaims().getClaimValue("id_usuario"));
		} catch (InvalidJwtException e) {
			e.printStackTrace();
		}
		return id_usuario;

	}
	public String getFullUserNameFromToken(String jwt) {
		String fullUserName = null;
		try {			
			JwtConsumer firstPassJwtConsumer = new JwtConsumerBuilder().setSkipAllValidators().setDisableRequireSignature().setSkipSignatureVerification().build();
			JwtContext jwtContext;
			jwtContext = firstPassJwtConsumer.process(jwt);		
			fullUserName = jwtContext.getJwtClaims().getClaimValue("fullUserName").toString();
			logger.info("El fullUserName obtenido del jwt: {}", jwtContext.getJwtClaims().getClaimValue("fullUserName"));
		} catch (InvalidJwtException e) {
			e.printStackTrace();
		}
		return fullUserName;

	}

	public String getUserFromToken(String jwt) {
		String user = null;
		try {
			JwtConsumer firstPassJwtConsumer = new JwtConsumerBuilder().setSkipAllValidators().setDisableRequireSignature().setSkipSignatureVerification().build();

			JwtContext jwtContext;
			jwtContext = firstPassJwtConsumer.process(jwt);
		
			user = jwtContext.getJwtClaims().getSubject();
			logger.info("El user obtenido del jwt: {}", user);
		} catch (InvalidJwtException e) {
			
			e.printStackTrace();
		} catch (MalformedClaimException e) {
			
			e.printStackTrace();
		}
		return user;

	}

	public Boolean validateToken(String token) {
		JsonWebKeySet jwks = new JsonWebKeySet(jwkList);
		JsonWebKey jwk = jwks.findJsonWebKey("1", null, null, null);
		logger.info("validateToken JWK (1) ===> " + jwk.toJson());
	
		JwtConsumer jwtConsumer = new JwtConsumerBuilder()
				.setRequireExpirationTime()
				.setAllowedClockSkewInSeconds(30)
				.setExpectedIssuer("Mapfre")
				.setVerificationKey(jwk.getKey()).build();
		try {
		
			JwtClaims jwtClaims = jwtConsumer.processToClaims(token);
			logger.info("JWT validation succeeded! " + jwtClaims);
			return true;
		} catch (InvalidJwtException e) {
			logger.error("JWT is Invalid: {}", e);
			return false;
		}

	}
	public String refreshToken(String token)
	{
		String user = getUserFromToken(token);
		Integer rol = getRolFromToken(token);		
		Integer id_usuario = getIdUsuarioFromToken(token);	
		String fullUserName= getFullUserNameFromToken(token);
		return generateToken(user, rol, id_usuario, fullUserName);
	}
	public String refreshTokenWithSchool(String token, int idIns)
	{
		String user = getUserFromToken(token);
		Integer rol = getRolFromToken(token);		
		Integer id_usuario = getIdUsuarioFromToken(token);	
		String fullUserName= getFullUserNameFromToken(token);
		return generateToken(user, rol, id_usuario, fullUserName,idIns);
	}
	public String generateToken(String username, Integer rol, Integer id_usuario, String fullUserName) {

		JwtClaims claims = null;
		String jwt = null;
		try {

			RsaJsonWebKey senderJwk = (RsaJsonWebKey) jwkList.get(0);
			senderJwk.setKeyId("1");
			logger.info("generateToken JWK (1) ===> " + senderJwk.toJson());
			
			claims = new JwtClaims();
			claims.setIssuer("Mapfre");
			claims.setExpirationTimeMinutesInTheFuture(500); 
			claims.setGeneratedJwtId(); 
			claims.setIssuedAtToNow(); 										
			claims.setNotBeforeMinutesInThePast(2);
			claims.setSubject(username);
			claims.setStringClaim("rol", rol.toString());
			claims.setStringClaim("id_usuario", id_usuario.toString());
			claims.setStringClaim("fullUserName", fullUserName);
			
			JsonWebSignature jws = new JsonWebSignature();
			jws.setPayload(claims.toJson());
			jws.setKeyIdHeaderValue(senderJwk.getKeyId());
			jws.setKey(senderJwk.getPrivateKey());
			jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

			jwt = jws.getCompactSerialization();
			logger.info("El jwt generado: {}", jwt);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jwt;
	}
	public String generateToken(String username, Integer rol, Integer id_usuario, String fullUserName, Integer idInstitucion) {

		JwtClaims claims = null;
		String jwt = null;
		try {

			RsaJsonWebKey senderJwk = (RsaJsonWebKey) jwkList.get(0);
			senderJwk.setKeyId("1");
			logger.info("generateToken JWK (1) ===> " + senderJwk.toJson());
			
			claims = new JwtClaims();
			claims.setIssuer("Mapfre");
			claims.setExpirationTimeMinutesInTheFuture(500); 
			claims.setGeneratedJwtId(); 
			claims.setIssuedAtToNow(); 										
			claims.setNotBeforeMinutesInThePast(2);
			claims.setSubject(username);
			claims.setStringClaim("rol", rol.toString());
			claims.setStringClaim("id_usuario", id_usuario.toString());
			claims.setStringClaim("fullUserName", fullUserName);
			claims.setStringClaim("idInstitucion", idInstitucion.toString());
			
			JsonWebSignature jws = new JsonWebSignature();
			jws.setPayload(claims.toJson());
			jws.setKeyIdHeaderValue(senderJwk.getKeyId());
			jws.setKey(senderJwk.getPrivateKey());
			jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

			jwt = jws.getCompactSerialization();
			logger.info("El jwt generado: {}", jwt);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jwt;
	}
}
