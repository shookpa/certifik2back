package com.ibm.util;

import java.util.Arrays;

public class MiException extends Exception {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MiException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MiException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	public MiException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public MiException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public MiException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "MiException [getMessage()=" + getMessage() + ", getLocalizedMessage()=" + getLocalizedMessage() + ", getCause()=" + getCause() + ", toString()=" + super.toString() + ", fillInStackTrace()=" + fillInStackTrace() + ", getStackTrace()="
				+ Arrays.toString(getStackTrace())  + ", getSuppressed()=" + Arrays.toString(getSuppressed()) + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
	
}
