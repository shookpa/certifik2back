package com.ibm.util;

import java.security.MessageDigest;
import java.text.Normalizer;
import java.util.Arrays;
import java.util.Random;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.ssl.Base64;

import com.lambdaworks.crypto.SCryptUtil;

public class StringUtils {
	 public String getCadenaAleatoria(int longitud) {
		String cadenaAleatoria = "";
		long ms = new java.util.GregorianCalendar().getTimeInMillis();
		Random r = new Random(ms);
		int i = 0;
		while (i < longitud) {
			char c = (char) r.nextInt(255);
			if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z')) {
				cadenaAleatoria += c;
				i++;
			}
		}
		return cadenaAleatoria;
	}
	
	 /**
	  * Función que elimina acentos y caracteres especiales de
	  * una cadena de texto.
	  * @param input
	  * @return cadena de texto limpia de acentos y caracteres especiales.
	  */
	 public static String removeSpecialCharacters(String str) {
		 //SOLAMENTE HAY QUE REEMPLAZAR ACENTOS, LAS EÑES SE CONSERVAN:
		 final String ORIGINAL = "ÁáÉéÍíÓóÚúÜü";
         final String REEMPLAZO = "AaEeIiOoUuUu";         
         if (str == null) {
             return null;
         }
         char[] array = str.toCharArray();
         for (int indice = 0; indice < array.length; indice++) {
             int pos = ORIGINAL.indexOf(array[indice]);
             if (pos > -1) {
                 array[indice] = REEMPLAZO.charAt(pos);
             }
         }
         return new String(array);
	  
	     
	 }//rem
	 public static void main (String args[]) throws Exception
	 {
		 	String algo ="El niño está dando muchísima latita";
		 	System.out.println("La nueva cadena queda como: "+removeSpecialCharacters(algo));
		 
		 String password = desencriptar("tIrTOXzeyTaHTpgoDMd3EA==", "00001000000507369770");
		 System.out.println("El pwd decifrado del firmante IVONNE: "+password);
//		 
//		 password = desencriptar("pITSZGZ97t0=", "00001000000503483776");
//		 System.out.println("El pwd decifrado del firmante JACKELINE: "+password);
//		 
//		 String pwd="jacke2909";
//		 String user="00001000000503483776";
//		 System.out.println("El pwd de Jackeline nuevo antes de cifrar: "+pwd);
//		 pwd=encriptar(pwd, user );
//		 System.out.println("El pwd de Jackeline ya cifrado: "+pwd);		 
//		 System.out.println("El pwd de Jackeline decifrado: "+desencriptar(pwd, user));		 
	 }
	 public static String encriptar(String texto, String secretKey) {

	       // String secretKey = "qualityinfosolutions"; //llave para encriptar datos
	        String base64EncryptedString = "";

	        try {

	            MessageDigest md = MessageDigest.getInstance("MD5");
	            byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
	            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);

	            SecretKey key = new SecretKeySpec(keyBytes, "DESede");
	            Cipher cipher = Cipher.getInstance("DESede");
	            cipher.init(Cipher.ENCRYPT_MODE, key);

	            byte[] plainTextBytes = texto.getBytes("utf-8");
	            byte[] buf = cipher.doFinal(plainTextBytes);
	            byte[] base64Bytes = Base64.encodeBase64(buf);
	            base64EncryptedString = new String(base64Bytes);

	        } catch (Exception ex) {
	        }
	        return base64EncryptedString;
	}
	 public static String desencriptar(String textoEncriptado , String secretKey) throws Exception {

	        //String secretKey = "qualityinfosolutions"; //llave para desenciptar datos
	        String base64EncryptedString = "";

	        try {
	            byte[] message = Base64.decodeBase64(textoEncriptado.getBytes("utf-8"));
	            MessageDigest md = MessageDigest.getInstance("MD5");
	            byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
	            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
	            SecretKey key = new SecretKeySpec(keyBytes, "DESede");

	            Cipher decipher = Cipher.getInstance("DESede");
	            decipher.init(Cipher.DECRYPT_MODE, key);

	            byte[] plainText = decipher.doFinal(message);

	            base64EncryptedString = new String(plainText, "UTF-8");

	        } catch (Exception ex) {
	        }
	        return base64EncryptedString;
	}
	 
	 
}
