package com.ibm.util;

import java.io.Serializable;

public class Throw extends Throwable implements Serializable{
	
	private String mensaje;
	private Throwable causa;
	
	public Throw() {}
	
	public Throw(String mensaje){
		super(mensaje);
		this.setMensaje(mensaje);	
	}
	public Throw(Throwable causa){
		super(causa);
		this.setCausa(causa);
		
	}
	public Throw(String mensaje,Throwable causa){
		super(mensaje,causa);
		this.setMensaje(mensaje);
		this.setCausa(causa);
		
	}
	

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Throwable getCausa() {
		return causa;
	}

	public void setCausa(Throwable causa) {
		this.causa = causa;
	}
	
	public String listaEscepcion(Exception e){
		Throw t = new Throw(e);
		
		
		
		
		return "";
	}
	
	
	
	
	
	
}